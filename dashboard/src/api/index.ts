import { gql } from '@apollo/client';
import * as Apollo from '@apollo/client';
import * as ApolloReactHooks from '@apollo/client';
export type Maybe<T> = T | null;
export type Exact<T extends { [key: string]: unknown }> = { [K in keyof T]: T[K] };
export type MakeOptional<T, K extends keyof T> = Omit<T, K> & { [SubKey in K]?: Maybe<T[SubKey]> };
export type MakeMaybe<T, K extends keyof T> = Omit<T, K> & { [SubKey in K]: Maybe<T[SubKey]> };
const defaultOptions =  {}
/** All built-in and custom scalars, mapped to their actual values */
export type Scalars = {
  ID: string;
  String: string;
  Boolean: boolean;
  Int: number;
  Float: number;
  Upload: any;
};


export enum CacheControlScope {
  Private = 'PRIVATE',
  Public = 'PUBLIC'
}

export type Episode = {
  __typename?: 'Episode';
  endTime?: Maybe<Scalars['String']>;
  startTime: Scalars['String'];
  title: Scalars['String'];
};

export type F1Data = {
  __typename?: 'F1Data';
  date: Scalars['String'];
  location: Scalars['String'];
};

export type Mutation = {
  __typename?: 'Mutation';
  _empty?: Maybe<Scalars['String']>;
  addReminder?: Maybe<Scalars['Boolean']>;
  authenticatePlex?: Maybe<Scalars['String']>;
  removeReminder?: Maybe<Scalars['Boolean']>;
  updateReminder?: Maybe<Scalars['Boolean']>;
};


export type MutationAddReminderArgs = {
  contents: Scalars['String'];
  userId: Scalars['Int'];
};


export type MutationAuthenticatePlexArgs = {
  password: Scalars['String'];
};


export type MutationRemoveReminderArgs = {
  id: Scalars['ID'];
};


export type MutationUpdateReminderArgs = {
  contents: Scalars['String'];
  id: Scalars['ID'];
  userId: Scalars['Int'];
};

export type Query = {
  __typename?: 'Query';
  SportsCentre: SportsCentre;
  TV: Tv;
  _empty?: Maybe<Scalars['String']>;
  reminders: Array<Reminder>;
};


export type QueryRemindersArgs = {
  userId: Scalars['Int'];
};

export type Reminder = {
  __typename?: 'Reminder';
  contents: Scalars['String'];
  id: Scalars['ID'];
};

export type Schedule = {
  __typename?: 'Schedule';
  channel: Scalars['String'];
  competition: Scalars['String'];
  eventText: Scalars['String'];
  fixture: Scalars['String'];
  liveLengthHours: Scalars['Int'];
  pageName: Scalars['String'];
  startTime?: Maybe<Scalars['String']>;
};

export type Shows = {
  __typename?: 'Shows';
  shows: Array<Episode>;
};

export type SportsCentre = {
  __typename?: 'SportsCentre';
  nextF1: F1Data;
  schedule: Array<Schedule>;
};

export type Tv = {
  __typename?: 'TV';
  bbc1: Shows;
  bbc2: Shows;
  c4: Shows;
  itv: Shows;
};


export type EpisodeFragment = (
  { __typename?: 'Episode' }
  & Pick<Episode, 'startTime' | 'title' | 'endTime'>
);

export type ShowFragment = (
  { __typename?: 'Shows' }
  & { shows: Array<(
    { __typename?: 'Episode' }
    & EpisodeFragment
  )> }
);

export type SportsCentreQueryVariables = Exact<{ [key: string]: never; }>;


export type SportsCentreQuery = (
  { __typename?: 'Query' }
  & { SportsCentre: (
    { __typename?: 'SportsCentre' }
    & { nextF1: (
      { __typename?: 'F1Data' }
      & Pick<F1Data, 'location' | 'date'>
    ), schedule: Array<(
      { __typename?: 'Schedule' }
      & Pick<Schedule, 'fixture' | 'startTime' | 'liveLengthHours' | 'eventText' | 'channel' | 'pageName' | 'competition'>
    )> }
  ) }
);

export type TvFragment = (
  { __typename?: 'TV' }
  & { bbc1: (
    { __typename?: 'Shows' }
    & ShowFragment
  ), bbc2: (
    { __typename?: 'Shows' }
    & ShowFragment
  ), itv: (
    { __typename?: 'Shows' }
    & ShowFragment
  ), c4: (
    { __typename?: 'Shows' }
    & ShowFragment
  ) }
);

export type TvQueryVariables = Exact<{ [key: string]: never; }>;


export type TvQuery = (
  { __typename?: 'Query' }
  & { TV: (
    { __typename?: 'TV' }
    & TvFragment
  ) }
);

export const EpisodeFragmentDoc = gql`
    fragment episode on Episode {
  startTime
  title
  endTime
}
    `;
export const ShowFragmentDoc = gql`
    fragment show on Shows {
  shows {
    ...episode
  }
}
    ${EpisodeFragmentDoc}`;
export const TvFragmentDoc = gql`
    fragment tv on TV {
  bbc1 {
    ...show
  }
  bbc2 {
    ...show
  }
  itv {
    ...show
  }
  c4 {
    ...show
  }
}
    ${ShowFragmentDoc}`;
export const SportsCentreDocument = gql`
    query SportsCentre {
  SportsCentre {
    nextF1 {
      location
      date
    }
    schedule {
      fixture
      startTime
      liveLengthHours
      eventText
      channel
      pageName
      channel
      competition
    }
  }
}
    `;

/**
 * __useSportsCentreQuery__
 *
 * To run a query within a React component, call `useSportsCentreQuery` and pass it any options that fit your needs.
 * When your component renders, `useSportsCentreQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useSportsCentreQuery({
 *   variables: {
 *   },
 * });
 */
export function useSportsCentreQuery(baseOptions?: ApolloReactHooks.QueryHookOptions<SportsCentreQuery, SportsCentreQueryVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return ApolloReactHooks.useQuery<SportsCentreQuery, SportsCentreQueryVariables>(SportsCentreDocument, options);
      }
export function useSportsCentreLazyQuery(baseOptions?: ApolloReactHooks.LazyQueryHookOptions<SportsCentreQuery, SportsCentreQueryVariables>) {
          const options = {...defaultOptions, ...baseOptions}
          return ApolloReactHooks.useLazyQuery<SportsCentreQuery, SportsCentreQueryVariables>(SportsCentreDocument, options);
        }
export type SportsCentreQueryHookResult = ReturnType<typeof useSportsCentreQuery>;
export type SportsCentreLazyQueryHookResult = ReturnType<typeof useSportsCentreLazyQuery>;
export type SportsCentreQueryResult = Apollo.QueryResult<SportsCentreQuery, SportsCentreQueryVariables>;
export const TvDocument = gql`
    query TV {
  TV {
    ...tv
  }
}
    ${TvFragmentDoc}`;

/**
 * __useTvQuery__
 *
 * To run a query within a React component, call `useTvQuery` and pass it any options that fit your needs.
 * When your component renders, `useTvQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useTvQuery({
 *   variables: {
 *   },
 * });
 */
export function useTvQuery(baseOptions?: ApolloReactHooks.QueryHookOptions<TvQuery, TvQueryVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return ApolloReactHooks.useQuery<TvQuery, TvQueryVariables>(TvDocument, options);
      }
export function useTvLazyQuery(baseOptions?: ApolloReactHooks.LazyQueryHookOptions<TvQuery, TvQueryVariables>) {
          const options = {...defaultOptions, ...baseOptions}
          return ApolloReactHooks.useLazyQuery<TvQuery, TvQueryVariables>(TvDocument, options);
        }
export type TvQueryHookResult = ReturnType<typeof useTvQuery>;
export type TvLazyQueryHookResult = ReturnType<typeof useTvLazyQuery>;
export type TvQueryResult = Apollo.QueryResult<TvQuery, TvQueryVariables>;