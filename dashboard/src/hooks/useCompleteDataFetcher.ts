import { useEffect, useState } from "react"
import { useDispatch } from "react-redux"
import { AnyAction, Dispatch } from "redux"
import { useSportsCentreQuery, useTvQuery } from "../api"
import { useGetWatchlistQuery } from "../reducers/mediaCentre"
import { updateSportsSchedule } from "../reducers/sportsCentre"
import { setStrava } from "../reducers/strava"
import { useGetTubeDataQuery } from "../reducers/tube"
import { setUser } from "../reducers/userConfig"
import { hasAllEventsFinished } from "../services/sportsCentre"
import TubeService from "../services/tube"
import { Account, ScheduleType, ViewServiceType } from "../types"

export const useCompleteDataFetcher = (
  schedule: ScheduleType | undefined,
  viewService: ViewServiceType,
  dispatch: Dispatch<AnyAction>,
  userAccount: Account | null,
  setValue: React.Dispatch<React.SetStateAction<"light" | "dark">>
) => {
  // To fetch all the data for all showing widgets
  const isDarkMode = viewService.getIsDarkMode()
  setValue(isDarkMode ? 'dark' : 'light')
  const currentSchedule = viewService.getCurrentSchedule(schedule)
  // const currentSchedule = [
  //   {
  //       "tag": "default",
  //       "widget": "morningSummary",
  //       "displayName": "Morning Summary",
  //       "positioned": "window"
  //   },
  // ]
    // {
    //     "tag": "default",
    //     "widget": "sportsCentre",
    //     "displayName": "Sports Centre",
    //     "positioned": "window"
    // },
    //     {
    //     "tag": "default",
    //     "widget": "tube",
    //     "displayName": "Tube",
    //     "positioned": "window"
    // },
    // {
    //   "tag": "tv",
    //   "widget": "tv",
    //   "displayName": "TV",
    //   "extra": {
    //       "type": "evening"
    //   },
    //   "positioned": "window"
    // },
  //   {
  //       "tag": "weather",
  //       "widget": "weather",
  //       "displayName": "Weather",
  //       "extra": {
  //           "location": "london",
  //           "type": "standard"
  //       },
  //       "positioned": "stack"
  //   },
  //   {
  //     "tag": "strava",
  //     "widget": "strava",
  //     "displayName": "Strava",
  //     "positioned": "stack"
  // },
    // {
    //     "tag": "default",
    //     "widget": "news",
    //     "displayName": "News",
    //     "positioned": "window"
    // },
  // ]
//     {
//       "tag": "mediaCentre",
//       "widget": "mediaCentre",
//       "displayName": "Media Centre",
//       "positioned": "window"
//     },
//     {
//       "tag": "default",
//       "widget": "birthdays",
//       "displayName": "Upcoming birthdays",
//       "positioned": "window"
//     }
// ]

  const {
    data: tvSchedule
  } = useTvQuery({pollInterval: 3600000})

  const {
    data: scData
  } = useSportsCentreQuery({pollInterval: 3600000})

  const {
    data: tubeStatus
  } = useGetTubeDataQuery(undefined, { pollingInterval: 400000 })

  const userId = userAccount ? userAccount.userId : -1

  const {
    data: watchlist,
  } = useGetWatchlistQuery(userId, { pollingInterval: 3600000 })

  const [windowWidgets, setWindowWidgets] = useState<any[]>([])
  const [stackWidgets, setStackWidgets] = useState<any[]>([])
  const stringedVersion = JSON.stringify(currentSchedule)
  useEffect(() => {
    const parsed = currentSchedule.filter(scheduleItem => {
      if(scheduleItem.widget === 'tube') {
        if(!tubeStatus) {
          return false
        }
        const parsedData = TubeService.getDataToParse(tubeStatus.tubeData, true)
        if(parsedData.length === 0) {
          return false
        }
      } else if(scheduleItem.widget === 'sportsCentre') {
        // if(!scData?.SportsCentre?.schedule || scData.SportsCentre.schedule.length === 0) {
        //   return false
        // }

        // if(hasAllEventsFinished(scData.SportsCentre.schedule).length === 0) {
        //   return false
        // }
      } else if(scheduleItem.widget === 'tv') {
        if(!tvSchedule) {
          return false
        }
      } else if (scheduleItem.widget === 'mediaCentre') {
        // NEED TO CHECK CORS ON API FOR LOCALHOST
        if(!watchlist) {
          return false
        }
      }

      // NEED TO ADD ALL REMAINING WIDGETS

      return scheduleItem.positioned === "window"
    })

    setWindowWidgets(parsed)
  }, [stringedVersion, tvSchedule, scData, tubeStatus]);

  useEffect(() => {
    const parsed = currentSchedule.filter(scheduleItem => {
      if(scheduleItem.positioned === "window") {
        return false
      }

      if(scheduleItem.widget === 'tube') {
        if(!tubeStatus) {
          return false
        }
        const parsedData = TubeService.getDataToParse(tubeStatus.tubeData, true)
        if(parsedData.length === 0) {
          return false
        }
      } else if(scheduleItem.widget === 'sportsCentre') {
        if(!scData?.SportsCentre?.schedule || scData.SportsCentre.schedule.length === 0) {
          return false
        }

        if(hasAllEventsFinished(scData.SportsCentre.schedule).length === 0) {
          return false
        }
      } else if(scheduleItem.widget === 'tv') {
        if(!tvSchedule) {
          return false
        }
      } else if (scheduleItem.widget === 'mediaCentre') {
        // NEED TO CHECK CORS ON API FOR LOCALHOST
        if(!watchlist) {
          return false
        }
      }

      // NEED TO ADD ALL REMAINING WIDGETS

      return true
    })

    setStackWidgets(parsed)
  }, [stringedVersion, tvSchedule, scData, tubeStatus]);

  useEffect(() => {
    if(userAccount && userAccount.strava && userAccount.strava.access) {
      fetch('https://www.strava.com/api/v3/athletes/37477356/stats', {
        headers: {
          Authorization: `Bearer ${userAccount?.strava.access}`
        }
      })
      .then(res => res.json())
      .then(res => {
        dispatch(setStrava(res.ytd_run_totals.distance / 1000))
      })
      .catch(_ => {
        fetch('https://www.strava.com/api/v3/oauth/token', {
          method: 'POST',
          body: new URLSearchParams({
            'client_id': process.env.REACT_APP_STRAVA_CLIENT as string,
            'client_secret': process.env.REACT_APP_STRAVA_SECRET as string,
            'refresh_token': userAccount.strava.refresh,
            'grant_type': 'refresh_token'
          })
        })
        .then(res => res.json())
        .then(res => {
          dispatch(setUser({
            ...userAccount,
            strava: {
              ...(userAccount as Account).strava,
              access: res['access_token']
            }
          }))

          fetch(process.env.REACT_APP_NETLIFY_ENDPOINT + ".netlify/functions/updateStravaConfig", {
            method: 'POST',
            body: JSON.stringify({ref: userAccount?.ref, strava: {
              access: res['access_token'],
              refresh: userAccount?.strava.refresh
            }})
          })
        })
      })
    }
  }, [userAccount])

  return [windowWidgets, stackWidgets]
}
