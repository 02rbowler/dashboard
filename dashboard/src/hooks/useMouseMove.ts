import { useEffect, useState } from "react"

export const useMouseMove = () => {
  const [mouseMove, setMouseMove] = useState(false)

  let timeout!: NodeJS.Timeout

  const mouseMoveHandler = () => {
    setMouseMove(true)

    clearTimeout(timeout)
    timeout = setTimeout(() => setMouseMove(false), 2000)
  }

  useEffect(() => {
    timeout = setTimeout(() => setMouseMove(false), 3000)
    document.addEventListener("mousemove", mouseMoveHandler)

    return function cleanup() {
      document.removeEventListener("mousemove", mouseMoveHandler)
      clearTimeout(timeout)
    }
  }, [])

  return mouseMove
}