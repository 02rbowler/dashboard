import { useState, useEffect } from "react"

function useAnimate(allowAnimate: boolean, window: boolean): number {
  const [animate, setAnimate] = useState(0)

  useEffect(() => {
    let animateTimeout: NodeJS.Timeout

    if (allowAnimate) {
      if (!animate) {
        animateTimeout = setTimeout(() => {
          setAnimate(1)
        }, 100)
      } else if (animate === 1) {
        animateTimeout = setTimeout(
          () => {
            setAnimate(2)
          },
          window ? 27000 : 57000
        )
      }
    } else {
      setAnimate(1)
    }

    return function cleanup() {
      clearTimeout(animateTimeout)
    }
  }, [animate, allowAnimate, window])

  return animate
}

export default useAnimate
