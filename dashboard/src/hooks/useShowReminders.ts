import { useEffect, useState } from "react"

export const useShowReminders = () => {
  const [showReminders, setShowReminders] = useState(false)

  useEffect(() => {
    const interval = setInterval(() => {
      setShowReminders(!showReminders)
    }, 15000)

    return () => {
      clearInterval(interval)
    }
  }, [showReminders])

  return showReminders
}
