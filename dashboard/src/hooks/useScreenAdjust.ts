import { useEffect, useState } from "react"

export const useScreenAdjust = () => {
  const [screenAdjust, setScreenAdjust] = useState(false)

  useEffect(() => {
    let adjustInterval = setInterval(() => {
      setScreenAdjust(!screenAdjust)
    }, 600000)

    return function cleanup() {
      adjustInterval && clearInterval(adjustInterval)
      // windowTimeout && clearTimeout(windowTimeout);
    }
  }, [screenAdjust])

  return screenAdjust
}
