import { useEffect, useState } from "react"
import { Config, ScheduleType, StackLayout, ViewServiceType } from "../types"

const fetchLayout = (
  viewService: ViewServiceType,
  schedule: ScheduleType | undefined,
  email: string | undefined,
  cctvMode: boolean | undefined,
  config: Config,
  setLayout: (layout: StackLayout) => void
) => {
  if (schedule && email) {
    const layout = viewService.getLayout(schedule, email, cctvMode, config)
    setLayout(layout)
  }
}

export const useGetLayout = (
  viewService: ViewServiceType,
  schedule: ScheduleType | undefined,
  email: string | undefined,
  cctvMode: boolean | undefined,
  config: Config
) => {
  const [layout, setLayout] = useState<StackLayout | null>(null)

  useEffect(() => {
    fetchLayout(viewService, schedule, email, cctvMode, config, setLayout)

    let viewInterval = setInterval(() => {
      fetchLayout(viewService, schedule, email, cctvMode, config, setLayout)
    }, 60000)

    return function cleanup() {
      viewInterval && clearInterval(viewInterval)
    }
  }, [schedule, email, cctvMode, viewService, config])

  return layout
}
