import * as Sentry from "@sentry/browser"
const { version } = require("./../../package.json")

const ApiService = {
  sendHealthCheck() {
    let location = localStorage.getItem("userAccount")
    const requestOptions = {
      method: "POST",
      body: JSON.stringify({
        location,
        version,
      }),
    }

    fetch(
      process.env.REACT_APP_NETLIFY_ENDPOINT + ".netlify/functions/healthCheck",
      requestOptions
    ).catch(error => {
      var timeInHours = new Date().getHours()
      if (timeInHours >= 6)
        Sentry.captureException("error sending health check: " + error)
    })
  },
}

export default ApiService
