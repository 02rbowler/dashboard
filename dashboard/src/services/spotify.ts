import Axios from "axios"
import store from "../reducers/store"
import { PodcastType } from "../types"

const SpotifyService = {
  requestRefreshToken() {
    const tokens = SpotifyService.getTokens()
    if (tokens[1]) {
      const params = new URLSearchParams()
      params.append("grant_type", "refresh_token")
      params.append("refresh_token", tokens[1])

      const state = store.getState()
      const config = state.keyStorageReducer.value

      const requestConfig = {
        headers: {
          "Content-Type": "application/x-www-form-urlencoded",
          Authorization:
            "Basic " +
            btoa(config.SPOTIFY.CLIENT_ID + ":" + config.SPOTIFY.CLIENT_SECRET),
        },
      }
      Axios.post(
        "https://accounts.spotify.com/api/token",
        params,
        requestConfig
      )
        .then(res => {
          if (res.data) {
            SpotifyService.parseTokens([res.data.access_token, tokens[1]])
            return true
          }
        })
        .catch(err => {
          console.log("ERROR")
          console.log(err)
          console.log(err.response)
        })
    }

    return false
  },

  parseTokens(tokenArray: string[]) {
    localStorage.setItem("spotify_access", tokenArray[0])
    localStorage.setItem("spotify_refresh", tokenArray[1])
  },

  getTokens() {
    return [
      localStorage.getItem("spotify_access"),
      localStorage.getItem("spotify_refresh"),
    ]
  },

  orderShows(inputArr: PodcastType[]) {
    const amendedArr = inputArr
      .sort((a, b) => {
        return new Date(a.episodeDate) < new Date(b.episodeDate) ? 1 : -1
      })
      .slice(0, 4)

    return amendedArr
  },
}

export default SpotifyService
