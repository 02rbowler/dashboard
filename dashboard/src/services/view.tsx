import React from "react"
import Tube from "../components/widgets/Tube"
import Calendar from "../components/widgets/Calendar"
import News from "../components/widgets/News"
import TV from "../components/widgets/TV"
import Weather from "../components/widgets/Weather"
import Reminders from "../components/widgets/Reminders"
import CCTV from "../components/widgets/CCTV"
import Podcast from "../components/widgets/Podcast"
import {
  Config,
  PusherMessage,
  ScheduleItem,
  ScheduleType,
  StackLayout,
} from "../types"
import Birthdays from "../components/widgets/Birthdays"
import SportsCentre from "../components/widgets/SportsCentre"
import * as Sentry from "@sentry/browser"
import { CalendarServiceType } from "./calendar"
import MediaCentre from "../components/widgets/MediaCentre"
import MorningSummary from "../components/widgets/MorningSummary"
import Strava from "../components/widgets/Strava"

const ViewService = (calendarService: CalendarServiceType) => {
  let showExtras = true
  let emailId = ""

  function parseValues(
    value: ScheduleItem[],
    config: Config,
    findForStack?: boolean
  ): JSX.Element[] {
    var elems: JSX.Element[] = []
    var timeKey

    for (var i = 0; i < value.length; i++) {
      if (findForStack) {
        if (value[i].positioned !== "stack") {
          continue
        }
      } else if (value[i].positioned === "stack") {
        continue
      }

      var timeKeyNext = new Date().getTime()
      if (timeKey === timeKeyNext) {
        timeKey++
      } else {
        timeKey = timeKeyNext
      }
      if (value[i].widget === "calendar") {
        Sentry.captureException("Calendar widget has been called")
        // if(value[i].widget === 'calendar')
        //     elems.push(<Calendar key={"cal" + timeKey} emailId={emailId} calendarService={calendarService} config={config} />);
        // else {
        //     const split = value[i].split('=');
        //
        //     } else {
        //         elems.push(<Calendar key={"cal2" + timeKey} emailId={emailId} type={split[1]} calendarService={calendarService} config={config} />);
        //     }
        // }
      // } else if (value[i].widget === "birthdays") {
      //   elems.push(
      //     <Birthdays
      //       key={"birthdays" + timeKey}
      //       emailId={emailId}
      //       calendarService={calendarService}
      //       config={config}
      //     />
      //   )
      } else if (value[i].widget === "weather") {
        elems.push(
          <Weather
            key={"weather" + timeKey}
            location={value[i].extra.location}
            asWindow={value[i].positioned !== "stack"}
          />
        )
      } else if (value[i].widget === "strava") {
        elems.push(
          <Strava
            key={"strava" + timeKey}
            asWindow={value[i].positioned !== "stack"}
          />
        )
      } else if (value[i].widget === "news") {
        // elems.push(<News key={"news" + timeKey} />)
      } else if (value[i].widget === "tube") {
        // elems.push(<Tube key={"tube" + timeKey} />)
      } else if (value[i].widget === "tv") {
        // elems.push(<TV key={"tv" + timeKey} />)
      } else if (value[i].widget === "cctv") {
        elems.push(<CCTV key={"cctv" + timeKey} />)
      } else if (value[i].widget === "podcast") {
        elems.push(<Podcast key={"podcast" + timeKey} />)
      } else if (value[i].widget === "sportsCentre") {
        // elems.push(<SportsCentre key={"sportsCentre" + timeKey} />)
      } else if (value[i].widget === "mediaCentre") {
        elems.push(<MediaCentre key={"mediaCentre" + timeKey} />)
      } else if (value[i].widget === "morningSummary") {
        elems.push(<MorningSummary key={"morningSummary" + timeKey} />)
      } else {
        Sentry.captureException(
          "There is a widget not accounted for: " + value[i].widget
        )
      }
    }
  
    return elems
  }

  function getIsDarkMode() {
    const dateTime = new Date()
    const hour = dateTime.getHours()
    const location = localStorage.getItem('userAccount');
    if(location === 'rob_bedroom') {
      return hour < 9 || hour > 20
    }

    return false
  }

  function getCurrentSchedule(
    schedule: ScheduleType | undefined
  ): ScheduleItem[] {
    const dateTime = new Date()
    const dayOfWeek = dateTime.getDay()
    const hour = dateTime.getHours()
    const minute = dateTime.getMinutes()

    if (!schedule) {
      return []
    }

    if (hour > 22 || hour < 6) {
      const todayDate = new Date().toLocaleDateString()
      const lastRestart = localStorage.getItem("DashboardRestart")
      if (hour === 23 && minute >= 20 && minute <= 55 && todayDate !== lastRestart) {
        localStorage.setItem("DashboardRestart", todayDate)
        window.location.reload()
      }
      // Night mode
      return []
    }

    if (dayOfWeek === 0 || dayOfWeek === 6) {
      // Weekend mode
      if (hour < 11) {
        return schedule.weekend.morning
      } else if (hour < 16) {
        return schedule.weekend.afternoon
      }

      return schedule.weekend.evening
    } else {
      // Weekday mode
      if (hour < 11) {
        return schedule.weekday.morning
      } else if (hour < 16) {
        return schedule.weekday.afternoon
      }

      return schedule.weekday.evening
    }
  }

  function getLayout(
    schedule: ScheduleType,
    email: string,
    cctvMode: boolean = false,
    config: Config
  ): StackLayout {
    showExtras = !showExtras
    emailId = email

    const currentSchedule = getCurrentSchedule(schedule)
    const defaultComps = <Reminders key="reminder" show={true} />
    const calendarElem = (
      <Calendar
        key={"mainCal"}
        emailId={emailId}
        calendarService={calendarService}
        config={config}
      />
    )

    // return {
    //     widgets: <div>
    //         {/* <CCTV /> */}
    //     </div>,
    //     default: <Calendar key={"mainCal"} emailId={emailId} calendarService={calendarService} config={config} />,
    //     showExtras: true
    // }

    if (currentSchedule.length === 0) {
      // Night mode
      return {
        widgets: [],
        stackWidgets: [],
      }
    }

    return {
      widgets: [...parseValues(currentSchedule, config)],
      stackWidgets: [...parseValues(currentSchedule, config, true)],
      default: defaultComps,
      showExtras,
      calendarElem,
    }
  }

  function parsePusher(
    pusherMessage: PusherMessage,
    refetchReminders: () => void,
    refetchExercise: () => void
  ) {
    if (pusherMessage.command) {
      switch (pusherMessage.command) {
        case "refresh":
          window.location.reload()
          return null
        case "fetchReminders":
          refetchReminders()
          return null
        case "fetchExercise":
          refetchExercise()
          return null
      }
    }

    switch (pusherMessage.widget) {
      // case "weather": return <Weather asWindow={true} />;
      case "tube":
        // return <Tube />
      case "tv":
        // return <TV />
      case "news":
        // return <News />
      case "cctv":
        return <CCTV />
    }

    return null
  }

  /*********************************
   *
   * Routine:
   *      Night - ?????
   *
   *      Weekend -
   *          Before 11am:
   *              Left - Calendar for today & tomorrow, Birthdays within next 2 weeks
   *              Right - Weather, Tube
   *          11am - 4pm:
   *              Left - Calendar for today, TV
   *              Right - Tube
   *          After 4pm:
   *              Left - Calendar full
   *              Right - TV
   *
   *      Weekday -
   *          Before 11am:
   *              Left - Calendar for today & tomorrow, TV
   *              Right - Weather, News
   *          11am - 4pm:
   *              Left - Calendar for today
   *              Right - Tube
   *          After 4pm:
   *              Left - Calendar full
   *              Right - TV
   *
   *********************************/

  return {
    getIsDarkMode,
    getCurrentSchedule,
    getLayout,
    parsePusher,
    parseValues,
  }
}

export default ViewService
