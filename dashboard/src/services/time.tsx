import React, { ReactNode } from "react"

export type TimeServiceType = {
  getTime: () => string
  getTimeSymbol: () => string
  getDayOfWeek: () => string
  getDate: () => string
  parseDateValue: (
    date: Date,
    withDayOfWeek?: boolean,
    inlineString?: boolean | undefined
  ) => string | ReactNode
}

const TimeService = (): TimeServiceType => {
  let timeSymbol: string
  let dayOfWeekNumber: number
  let dateNumber: number
  let monthNumber: number

  const weekArray = [
    "Sunday",
    "Monday",
    "Tuesday",
    "Wednesday",
    "Thursday",
    "Friday",
    "Saturday",
  ]
  const miniWeekArray = ["Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"]
  const monthArray = [
    "January",
    "February",
    "March",
    "April",
    "May",
    "June",
    "July",
    "August",
    "September",
    "October",
    "November",
    "December",
  ]

  function getTime(): string {
    const current = new Date()
    let hours: number = current.getHours()
    const minutes: number = current.getMinutes()

    dayOfWeekNumber = current.getDay()
    monthNumber = current.getMonth()
    dateNumber = current.getDate()

    let time = hours + ":"
    timeSymbol = "am"
    if (hours > 12) {
      hours -= 12
      time = hours + ":"
      timeSymbol = "pm"
    } else if (hours === 12) {
      timeSymbol = "pm"
    }
    if (hours < 10) {
      time = "0" + hours + ":"
    }

    if (minutes < 10) {
      time += "0"
    }

    return time + minutes
  }

  function getTimeSymbol(): string {
    return timeSymbol
  }

  function getDayOfWeek(): string {
    return miniWeekArray[dayOfWeekNumber]
  }

  function getDate() {
    return dateNumber + " " + monthArray[monthNumber]
  }

  function parseDateValue(
    date: Date,
    withDayOfWeek: boolean = false,
    inlineString?: boolean
  ): string | ReactNode {
    var todayDate = new Date()
    if (todayDate.toDateString() === date.toDateString()) {
      return "Today"
    }

    var testDate = new Date(
      todayDate.getFullYear(),
      todayDate.getMonth(),
      todayDate.getDate() + 1
    )
    // if(testDate.toDateString() === date.toDateString()){
    //     return 'Tomorrow';
    // }

    testDate = new Date(
      todayDate.getFullYear(),
      todayDate.getMonth(),
      todayDate.getDate() + 7
    )
    if (testDate.getTime() > date.getTime()) {
      // return weekArray[date.getDay()];
      return miniWeekArray[date.getDay()]
    }

    if (withDayOfWeek) {
      var day = miniWeekArray[date.getDay()]

      if (inlineString) {
        return `${day} ${date.getDate()}`
      }

      return (
        <>
          {day}
          <br />
          <b>{date.getDate()}</b>
        </>
      )
    }

    return date.getDate() + " " + monthArray[date.getMonth()]
  }

  return {
    getTime,
    getTimeSymbol,
    getDayOfWeek,
    getDate,
    parseDateValue,
  }
}

export default TimeService
