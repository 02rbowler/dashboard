import BTSport from "../assets/channels/BTSport.svg"
import PrimeVideo from "../assets/channels/PrimeVideo.svg"
import BBCOne from "../assets/channels/BBC_One.svg"
import BBCTwo from "../assets/channels/BBC_Two.svg"
import ITV from "../assets/channels/ITV.svg"
import ITV4 from "../assets/channels/ITV4.svg"
import C4 from "../assets/channels/C4.svg"
import SkySports from "../assets/channels/SkySports.svg"
import SkySportsWhite from "../assets/channels/SkySports_white.svg"
import BBCSport from "../assets/channels/BBC_Sport.svg"

import CricketEngland from "../assets/logos/cricket/england.png"
import CricketWhiteEngland from "../assets/logos/cricket/england-white.png"
import CricketNewZealand from "../assets/logos/cricket/new-zealand.png"
import CricketAustralia from "../assets/logos/cricket/australia.png"
import CricketIndia from "../assets/logos/cricket/india.png"
import CricketPakistan from "../assets/logos/cricket/pakistan.png"
import CricketSouthAfrica from "../assets/logos/cricket/south-africa.png"
import CricketSriLanka from "../assets/logos/cricket/sri-lanka.png"
import CricketWestIndies from "../assets/logos/cricket/west-indies.png"

import CricketLondonSpirit from "../assets/logos/cricket/london-spirit.jpg"
import CricketSouthernBrave from "../assets/logos/cricket/southern-brave.jpg"
import CricketWelshFire from "../assets/logos/cricket/welsh-fire.jpg"
import CricketTrentRockets from "../assets/logos/cricket/trent-rockets.jpg"
import CricketManchesterOriginals from "../assets/logos/cricket/manchester-originals.jpg"
import CricketNorthernSuperchargers from "../assets/logos/cricket/northern-superchargers.jpg"
import CricketOvalInvincibles from "../assets/logos/cricket/oval-invincibles.jpg"
import CricketBirminghamPhoenix from "../assets/logos/cricket/birmingham-phoenix.jpg"

import RugbyEngland from "../assets/logos/rugby/england.png"
import RugbyWales from "../assets/logos/rugby/wales.png"
import RugbyNewZealand from "../assets/logos/rugby/new-zealand.png"
import RugbyIreland from "../assets/logos/rugby/ireland.png"
import RugbySouthAfrica from "../assets/logos/rugby/south-africa.png"
import RugbyAustralia from "../assets/logos/rugby/australia.png"
import RugbyScotland from "../assets/logos/rugby/scotland.png"
import RugbyArgentina from "../assets/logos/rugby/argentina.png"
import RugbyFrance from "../assets/logos/rugby/france.png"
import RugbyItaly from "../assets/logos/rugby/italy.png"

import LordsCricket from "../assets/eventImg/lords-cricket.jpg"
import F1Img from "../assets/eventImg/f1.jpg"
import { Schedule } from "../api"
import { SportsGuideApiReturn } from "../components/widgets/SportsCentre/SkySportsGuide"
import moment from "moment"
import { SportsFixture } from "../types"

export const parseCompetition = (competition: string, mainText: string) => {
  const trimmed = competition.trim()
  switch (trimmed) {
    case "F1 season":
      return ""
  }

  if (mainText.indexOf("UCI Tour de France") === 0) {
    return "Tour de France"
  }

  return mainText
}

export const parseMainText = (data: Schedule) => {
  const trimmed = data.competition.trim()

  if (trimmed.indexOf("Cricket Test Match") === 0) {
    return "Test Match"
  }

  switch (trimmed) {
    case "F1 season":
      const event = data.eventText.split("-")[0].trim()
      return event
    case "Twenty20 International":
    case "Womens T20":
    case "One Day International":
    case "Guinness Six Nations":
    case "U.S. Masters":
    case "The Ashes":
    case "British & Irish Lions Tour":
    case "European Swimming Championships":
    case "ATP World Tour":
    case "IAAF Diamond League":
    case "Grand Slam Tennis":
    case "UCI World Tour":
    case "Vitality Blast":
    case "Ryder Cup":
      return ""
  }

  return trimmed
}

export const isChannelPadded = (channel: string): boolean => {
  if (
    channel.indexOf("Sky Sports") !== -1 ||
    channel.indexOf("BBC Sport") !== -1 ||
    channel.indexOf("BBC2") !== -1
  ) {
    return true
  }

  return false
}

export const getTestMatchDay = (competition: string): string => {
  const trimmed = competition.trim()
  const dayIndex = trimmed.indexOf("Day")

  if (dayIndex === -1) {
    return trimmed
  }

  return trimmed.slice(dayIndex, trimmed.length)
}

type TeamDataItem = {
  img: string
  whiteBallImg?: string
  name: string
  primaryColour: string
  secondaryColour?: string
  whiteBallColour?: string
}

type TeamData = {
  home: TeamDataItem
  away: TeamDataItem
}
export const getCricketTeamData = (fixture: string): TeamData => {
  const split = fixture.split(" v ")

  return {
    home: parseCricketTeamForData(split[0].toLowerCase()),
    away: parseCricketTeamForData(split[1].toLowerCase()),
  }
}

export const getCricketHundredTeamData = (fixture: string) => {
  if(!fixture) {
    return {
      home: '',
      away: ''
    }
  }

  const split = fixture.split(" v ")

  return {
    home: parseCricketHundredTeam(split[0].toLowerCase()),
    away: parseCricketHundredTeam(split[1].toLowerCase()),
  }
}

export const getRugbyTeamData = (fixture: string): TeamData => {
  const split = fixture.split(" v ")

  return {
    home: parseRugbyTeamForData(split[0].toLowerCase(), split[0].trim()),
    away: parseRugbyTeamForData(split[1].toLowerCase(), split[1].trim()),
  }
}

const parseRugbyTeamForData = (
  input: string,
  teamName: string
): TeamDataItem => {
  if (input.indexOf("england") !== -1) {
    return {
      img: RugbyEngland,
      name: teamName,
      primaryColour: "white",
    }
  }
  if (input.indexOf("new zealand") !== -1) {
    return {
      img: RugbyNewZealand,
      name: teamName,
      primaryColour: "black",
    }
  }
  if (input.indexOf("scotland") !== -1) {
    return {
      img: RugbyScotland,
      name: teamName,
      primaryColour: "#0071ff",
    }
  }
  if (input.indexOf("wales") !== -1) {
    return {
      img: RugbyWales,
      name: teamName,
      primaryColour: "#D30000",
    }
  }
  if (input.indexOf("ireland") !== -1) {
    return {
      img: RugbyIreland,
      name: teamName,
      primaryColour: "#00a700",
    }
  }
  if (input.indexOf("france") !== -1) {
    return {
      img: RugbyFrance,
      name: teamName,
      primaryColour: "#403dff",
    }
  }
  if (input.indexOf("italy") !== -1) {
    return {
      img: RugbyItaly,
      name: teamName,
      primaryColour: "#3da8ff",
    }
  }
  if (input.indexOf("argentina") !== -1) {
    return {
      img: RugbyArgentina,
      name: teamName,
      primaryColour: "#00deff",
    }
  }
  if (input.indexOf("australia") !== -1) {
    return {
      img: RugbyAustralia,
      name: teamName,
      primaryColour: "#ffbf00",
    }
  }
  if (input.indexOf("south africa") !== -1) {
    return {
      img: RugbySouthAfrica,
      name: teamName,
      primaryColour: "#00881d",
    }
  }
  return {
    img: "",
    name: teamName,
    primaryColour: "#ffffffbd",
  }
}

const parseCricketHundredTeam = (input: string): string => {
  if (input.indexOf("london spirit") !== -1) {
    return CricketLondonSpirit
  }
  if (input.indexOf("oval invincibles") !== -1) {
    return CricketOvalInvincibles
  }
  if (input.indexOf("trent rockets") !== -1) {
    return CricketTrentRockets
  }
  if (input.indexOf("birmingham phoenix") !== -1) {
    return CricketBirminghamPhoenix
  }
  if (input.indexOf("welsh fire") !== -1) {
    return CricketWelshFire
  }
  if (input.indexOf("northern superchargers") !== -1) {
    return CricketNorthernSuperchargers
  }
  if (input.indexOf("manchester originals") !== -1) {
    return CricketManchesterOriginals
  }
  if (input.indexOf("southern brave") !== -1) {
    return CricketSouthernBrave
  }

  return ""
}

const parseCricketTeamForData = (input: string): TeamDataItem => {
  if (input.indexOf("england") !== -1) {
    return {
      img: CricketEngland,
      whiteBallImg: CricketWhiteEngland,
      name: "England",
      primaryColour: "#1170ffbd",
      secondaryColour: "#2DB3FF",
      whiteBallColour: "#4b9effc2",
    }
  }
  if (input.indexOf("new zealand") !== -1) {
    return {
      img: CricketNewZealand,
      name: "New Zealand",
      primaryColour: "#000000bd",
      secondaryColour: "#a5a5a5",
      whiteBallColour: "#777777c2",
    }
  }
  if (input.indexOf("australia") !== -1) {
    return {
      img: CricketAustralia,
      name: "Australia",
      primaryColour: "#006916bd",
      secondaryColour: "#ffbf12",
      whiteBallColour: "#0c9919c2",
    }
  }
  if (input.indexOf("india") !== -1) {
    return {
      img: CricketIndia,
      name: "India",
      primaryColour: "#009af7bd",
      secondaryColour: "#dcb983",
      whiteBallColour: "#40bcffc2",
    }
  }
  if (input.indexOf("pakistan") !== -1) {
    return {
      img: CricketPakistan,
      name: "Pakistan",
      primaryColour: "#00480fbd",
      secondaryColour: "#bfa628",
      whiteBallColour: "#075900c2",
    }
  }
  if (input.indexOf("south africa") !== -1) {
    return {
      img: CricketSouthAfrica,
      name: "South Africa",
      primaryColour: "#009506bd",
      secondaryColour: "#fccc1e",
      whiteBallColour: "#02bd19c2",
    }
  }
  if (input.indexOf("sri lanka") !== -1) {
    return {
      img: CricketSriLanka,
      name: "Sri Lanka",
      primaryColour: "#1e3d86bd",
      secondaryColour: "#f5ed5c",
      whiteBallColour: "#0a2ba1c2",
    }
  }
  if (input.indexOf("west indies") !== -1) {
    return {
      img: CricketWestIndies,
      name: "West Indies",
      primaryColour: "#791b23bd",
      secondaryColour: "#f6c01b",
      whiteBallColour: "#5b0f0fc2",
    }
  }

  return {
    img: "",
    name: input,
    primaryColour: "#1170ffbd",
    secondaryColour: "#fe4617",
    whiteBallColour: "#dcdcdcc2",
  }
}

export const parseChannel = (
  channel: string,
  whiteVersion?: boolean
): string => {
  if (channel.indexOf("Sky Sports") !== -1) {
    return whiteVersion ? SkySportsWhite : SkySports
  }
  if (channel.indexOf("BT Sport") !== -1) {
    return BTSport
  }
  if (channel.indexOf("BBC1") !== -1) {
    return BBCOne
  }
  if (channel.indexOf("BBC2") !== -1) {
    return BBCTwo
  }
  if (channel.indexOf("ITV4") !== -1) {
    return ITV4
  }
  if (channel.indexOf("ITV") !== -1) {
    return ITV
  }
  if (channel.indexOf("Channel 4") !== -1) {
    return C4
  }
  if (channel.indexOf("Prime Video") !== -1) {
    return PrimeVideo
  }
  if (channel.indexOf("BBC Sport") !== -1) {
    return BBCSport
  }

  return channel
}

export const getGradientColours = (comp: string): string[] => {
  if (comp.indexOf("The Ashes") === 0) {
    return ["#09689D", "#212657", "#212657"]
  }
  if (comp.indexOf("F1 season") === 0) {
    return ["#000000", "#101010", "#282828"]
  }

  return ["white", "white", "white"]
}

export const getEventImage = (comp: string): string => {
  if (comp.indexOf("The Ashes") === 0) {
    return LordsCricket
  }
  if (comp.indexOf("F1 season") === 0) {
    return F1Img
  }

  return ""
}

export const parseCurrentAndFutureSportsGuide = (
  guide: SportsGuideApiReturn | undefined
): SportsGuideApiReturn | undefined => {
  if (!guide) {
    return guide
  }

  const transformedGuide: SportsGuideApiReturn = {
    mainEvent: [],
    cricket: [],
    action: [],
    f1: [],
    arena: [],
    football: [],
    golf: [],
    premierLeague: [],
    racing: [],
  }
  const currentTime = moment()
  const channels = Object.keys(guide)

  channels.forEach(channel => {
    const newArray = guide[channel as keyof typeof guide].filter(item => {
      const hasFinished =
        currentTime >
        moment(item.startTime, ["h:mm a"]).add(item.minutesLength, "minutes")
      return !hasFinished
    })

    transformedGuide[channel as keyof typeof guide] = newArray
  })

  return transformedGuide
}

export const hasEventFinished = (eventStart: string | null, eventLength: number): boolean => {
  const currentTime = moment();
  if(!eventStart) {
    return false
  }
  return currentTime > moment(eventStart.trim(), ["h:mm a"]).add(eventLength, "hours")
}

export const hasAllEventsFinished = (data: any[]) => {
  return data.filter(item => !hasEventFinished(item.startTime, item.liveLengthHours))
}

export const getAllFixturesByType = (fixtures: SportsFixture[], pageName: string) => {
  return fixtures.filter(
    item => item.pageName === pageName && (!item.startTime || !hasEventFinished(item.startTime, item.liveLengthHours))
  )
}
