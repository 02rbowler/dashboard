import * as Sentry from "@sentry/browser"
import React from "react"
import Cloudy from "../assets/weather/cloudy.svg"
import Drizzle from "../assets/weather/drizzle.svg"
import Overcast from "../assets/weather/overcast.svg"
import Rain from "../assets/weather/rain.svg"
import Snow from "../assets/weather/snow.svg"
import Sunny from "../assets/weather/sunny.svg"
import Thunderstorm from "../assets/weather/thunderstorm.svg"

export type WeatherType = {
  dt: number
  dt_txt: string
  main: {
    feels_like: number
    temp: number
  }
  weather: {
    description: string
    icon: string
    id: number
    main: string
  }[]
  wind: {
    deg: number
    speed: number
  }
}

export type MetOfficeWeatherPeriod = {
  type: 'Day',
  value: string,
  Rep: MetOfficeWeather[]
}

export type MetOfficeWeather = {
  $: string;
  F: string;
  T: string;
  W: string;
}

export type ParsedMetOfficeWeather = {
  dateTime: Date;
  feelsLikeTemp: string;
  temperature: string;
  icon: string;
}

const WeatherService = {
  getIcon(weather: string, bigIcon?: boolean): JSX.Element {
    const iconSize = bigIcon ? 150 : 105
    let weatherIcon = (
      <img src={Sunny} width={iconSize} height={iconSize} alt={weather} />
    )

    switch (parseInt(weather)) {
      case 2:
      case 3:
      case 7:
        weatherIcon = (
          <img src={Cloudy} width={iconSize} height={iconSize} alt={weather} />
        )
        break
      case 0:
      case 1:
        weatherIcon = (
          <img src={Sunny} width={iconSize} height={iconSize} alt={weather} />
        )
        break
      case 5:
      case 6:
      case 8:
        weatherIcon = (
          <img
            src={Overcast}
            width={iconSize}
            height={iconSize}
            alt={weather}
          />
        )
        break
      case 16:
      case 17:
      case 18:
      case 22:
      case 23:
      case 24:
      case 25:
      case 26:
      case 27:
        weatherIcon = (
          <img src={Snow} width={iconSize} height={iconSize} alt={weather} />
        )
        break
      case 13:
      case 14:
      case 15:
      case 19:
      case 20:
      case 21:
        weatherIcon = (
          <img src={Rain} width={iconSize} height={iconSize} alt={weather} />
        )
        break
      case -1:
      case 9:
      case 10:
      case 11:
      case 12:
        weatherIcon = (
          <img src={Drizzle} width={iconSize} height={iconSize} alt={weather} />
        )
        break
      case 28:
      case 29:
      case 30:
        weatherIcon = (
          <img
            src={Thunderstorm}
            width={iconSize}
            height={iconSize}
            alt={weather}
          />
        )
        break
    }

    return weatherIcon
  },

  getBackgroundColours(weather: string): string[] {
    switch (weather.toLowerCase()) {
      case "clouds":
        return ["#746161b3", "#d9d9d9b3"]
      case "clear":
        return ["#0095e9b3", "#96dfffb3"]
      case "ash":
      case "dust":
      case "sand":
      case "fog":
      case "haze":
      case "smoke":
      case "mist":
        return ["#415a87b3", "#cfd9ddb3"]
      case "tornado":
      case "squall":
      case "snow":
        return ["#746161b3", "#d9d9d9b3"]
      case "rain":
      case "drizzle":
      case "thunderstorm":
        return ["#161616b3", "#d5d5d5b3"]
    }

    return ["#0095e9b3", "#96dfffb3"]
  },

  parseForecast(fullList: MetOfficeWeatherPeriod[]): ParsedMetOfficeWeather[] {
    const now = Date.now()
    const arrayToPush: ParsedMetOfficeWeather[] = []
    fullList.slice(0, 2).map((listItem: MetOfficeWeatherPeriod, index) => {
      listItem.Rep.forEach((element) => {
        const date = new Date()
        date.setHours(0,0,0,0)
        date.setDate(date.getDate() + index)
        date.setHours(date.getHours() + (parseInt(element.$) / 60))

        if(now < date.getTime()) {
          arrayToPush.push({
            dateTime: date,
            feelsLikeTemp: element.F,
            temperature: element.T,
            icon: element.W
          })
        }
      });
    })

    return arrayToPush;
  },

  callApi(url: string, callback: Function) {
    fetch(url)
      .then(res => {
        return res.json()
      })
      .then(res => {
        callback(res)
      })
      .catch(err => {
        Sentry.captureException("Weather fetch failed (services): " + err)
      })
  },

  getTime(date: Date): string {
    let hours = date.getHours()
    let timeSymbol = "am"
    let time = hours + ""

    if (hours === 0) {
      return "12am"
    } else if (hours > 12) {
      hours -= 12
      return hours + "pm"
    } else if (hours === 12) {
      timeSymbol = "pm"
    }

    return time + timeSymbol
  },
}

export default WeatherService
