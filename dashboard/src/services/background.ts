// import * as Sentry from '@sentry/browser';
import background1 from "./../assets/backgroundImages/8ywSvj.jpg"
import background2 from "./../assets/backgroundImages/africa.jpg"

type BackgroundServiceType = {
  getBackgroundImage: (
    backgroundImages: string[],
    noInternetBackgroundToggle: boolean,
    setNightMode: (val: boolean) => void,
    setImageNumber: (val: string) => void
  ) => void
}

const BackgroundService: BackgroundServiceType = {
  getBackgroundImage(
    backgroundImages,
    noInternetBackgroundToggle,
    setNightMode,
    setImageNumber
  ) {
    const date = new Date()
    if (date.getHours() > 22 || date.getHours() < 6) {
      setNightMode(true)
      return
    }

    if (!backgroundImages) {
      return
    }

    if (backgroundImages.length === 0) {
      if (!(date.getHours() > 22 || date.getHours() < 6)) {
        // Sentry.captureException("No background images available");
      }
      noInternetBackgroundToggle = !noInternetBackgroundToggle
      if (noInternetBackgroundToggle) {
        setImageNumber(background1)
        setNightMode(false)
      } else {
        setImageNumber(background2)
        setNightMode(false)
      }
      return
    }

    const rand = Math.floor(Math.random() * backgroundImages.length)
    const image = backgroundImages[rand]

    setImageNumber(image)
    setNightMode(false)
  },
}

export default BackgroundService
