interface matchesData {
  matches: {
    "team-1": string
    "team-2": string
    date: string
    matchStarted: boolean
    unique_id: number
  }[]
}

const SportsScoresService = () => {
  function parseCricketData(rawData: matchesData) {
    if (!rawData || Object.keys(rawData).length === 0) return null

    const filteredMatches = rawData.matches.filter(
      match => match["team-1"] === "England" || match["team-2"] === "England"
    )

    if (filteredMatches.length === 0) {
      return null
    }

    const sliced = filteredMatches.slice(0, 1)
    if (new Date(sliced[0].date).getTime() > new Date().getTime()) {
      return null
    }

    return filteredMatches[0]
  }

  return {
    parseCricketData,
  }
}

export default SportsScoresService
