import { ColouredBackground } from "../components/Rows"
import { TubeObj } from "../types"

const TubeService = {
  getDataToParse(lineStatus: TubeObj[], allowEmpty?: boolean) {
    const filtered = lineStatus.filter(
      (tube: TubeObj) =>
        tube.lineStatuses[0].statusSeverityDescription !== "Good Service" &&
        tube.lineStatuses[0].statusSeverityDescription !== "Minor Delays"
    )

    if (filtered.length === 0 && !allowEmpty) {
      filtered.push({
        id: "victoria",
        name: "victoria",
        lineStatuses: [
          {
            statusSeverityDescription: "Good Service",
            reason: "",
          },
        ],
      })
    }

    return filtered
  },

  getBackgroundByLineType(id: string): string {
    switch (id) {
      case "london-overground":
        return "#ff7702"
      case "northern":
        return "#424242"
      case "piccadilly":
        return "#0523fe"
      case "bakerloo":
        return "#af6100"
      case "central":
        return "#ff0e19"
      case "circle":
        return "#ffaa0e"
      case "district":
        return "#2fbd01"
      case "hammersmith-city":
        return "#d410ce"
      case "jubilee":
        return "#868686"
      case "victoria":
        return "#0086b9"
      case "metropolitan":
        return "#a900ff"
      case "dlr":
        return "#00c4ff"
      case "elizabeth":
        return "#741bff"
    }

    return 'white';
  },

  isWhiteText(tubeName: string): boolean {
    switch (tubeName) {
      case "piccadilly":
      case "bakerloo":
      case "elizabeth":
      case "jubilee":
      case "metropolitan":
      case "northern":
      case "hammersmith-city":
        return true
    }

    return false
  },

  parseReason(reason: string) {
    return reason.substring(reason.indexOf(":") + 1)
  },
}

export default TubeService
