import * as Sentry from "@sentry/browser"
import React from "react"
import { CalendarEvent, Config } from "../types"
import { TimeServiceType } from "./time"
import { gapi } from "gapi-script"

type EventTime = { dateTime: string } | { date: string }

export type CalendarServiceType = {
  getServerEvents: () => Promise<any>
  getEvents: (callback: Function, emailId: string | string[], config: Config) => void
  handleSignIn: () => void
  getTime: (eventStart: EventTime) => JSX.Element
  displayDate: (
    eventStart: EventTime,
    timeService: TimeServiceType,
    inlineString: boolean,
    allowRepeatDates?: boolean
  ) => React.ReactNode
  getWhoClass: (eventWho: string) => string
  resetLastDate: () => void
  parseEvents: Function
  parseEventsByDate: Function
  getSignedInStatus: () => boolean
  setAuthTokens: (token: string) => void
}

const CalendarService = () => {
  const DISCOVERY_DOCS = [
    "https://www.googleapis.com/discovery/v1/apis/calendar/v3/rest",
  ]
  const SCOPES = "https://www.googleapis.com/auth/calendar.readonly"
  const whoOptions = [
    "andrew",
    "cath",
    "james",
    "mary",
    "asher",
    "peter",
    "rob",
  ]

  let lastDate = ""
  let isSignedInStatus = false

  function handleSignIn() {
    gapi.auth2.getAuthInstance().signIn()
  }

  function setAuthTokens(token: string) {
    localStorage.setItem("google_auth_tokens", JSON.stringify(token))
    getServerEvents()
  }

  async function getServerEvents() {
    const token = JSON.parse(
      localStorage.getItem("google_auth_tokens") as string
    )
    return await fetch(
      `https://integrations-02rbowler.cloud.okteto.net/google/getEvents`,
      {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
        },
        body: JSON.stringify({ token }),
      }
    )
      .then(res => res.json())
      .then(parsedRes => {
        // console.log(parsedRes)
        return parsedRes
      })
  }

  function getTime(eventStart: EventTime) {
    if (!("dateTime" in eventStart)) {
      return <span></span>
    }
    let date = new Date(eventStart.dateTime)
    let hours = date.getHours()
    let mins = date.getMinutes()
    let type = hours >= 12 ? "pm" : "am"
    if (hours > 12) {
      hours -= 12
    }
    return (
      <span>
        <b>
          {hours}:{mins < 10 ? "0" + mins : mins}
        </b>
        {type}
      </span>
    )
  }

  function getSignedInStatus() {
    return isSignedInStatus
  }

  function mergeArray(arr1: any, arr2: any){
    let merged = [];
    let index1 = 0;
    let index2 = 0;
    let current = 0;

    while (current < (arr1.length + arr2.length)) {
      let unmerged1 = index1 < arr1.length ? arr1[index1].start.dateTime || arr1[index1].start.date : undefined;
      let unmerged2 = index2 < arr2.length ? arr2[index2].start.dateTime || arr2[index2].start.date : undefined;

      let isArr1Depleted = index1 >= arr1.length;
      let isArr2Depleted = index2 >= arr2.length;

      if (!isArr1Depleted && (isArr2Depleted || unmerged1 < unmerged2)) {
        merged[current] = arr1[index1];
        index1++;
      } else {
        merged[current] = arr2[index2];
        index2++;
      }

      current++;
    }

    return merged
  }

  function getEvents(callback: Function, emailId: string | string[], config: Config) {
    function updateSigninStatus(isSignedIn: boolean) {
      if (!isSignedIn) {
        // Sentry.captureException("Get events: not signed in - " + emailId)
        callback({ isSignedIn })
      } else {
        listUpcomingEvents()
      }
    }

    function listUpcomingEvents() {
      try {
        if(typeof emailId === "string") {
          gapi.client.calendar.events
            .list({
              calendarId: emailId,
              timeMin: new Date().toISOString(),
              showDeleted: false,
              singleEvents: true,
              maxResults: 50,
              orderBy: "startTime",
            })
            .then(
              (response: {
                status: number
                result: {
                  items: {
                    id: number
                    description?: string
                    summary: string
                    end: EventTime
                    start: EventTime
                    who?: string
                    attendees?: any
                  }[]
                }
              }) => {
                if (
                  response.status !== 200 ||
                  response.result.items.length === 0
                ) {
                  Sentry.captureException("Response not valid: " + response)
                }
                let events = response.result.items

                if (events.length === 0) {
                  Sentry.captureException("Events length is 0 - " + emailId)
                }

                events.forEach(
                  (event, i) => {
                    let who = event.description
                    if (event.summary.toLowerCase().indexOf("birthday") !== -1) {
                      who = "birthday"
                    } else if (
                      event.summary.toLowerCase().indexOf("cook ") === 0
                    ) {
                      who = "cooking"
                    }

                    if (!who || who === "") {
                      who = analyseTitle(event.summary.toLowerCase())
                    }

                    // remove rejected events
                    if(event.attendees && event.attendees[0].self === true && event.attendees[0].responseStatus === 'declined') {
                      events[i] = null as any
                    } else {
                      events[i] = {
                        id: event.id,
                        end: event.end,
                        start: event.start,
                        summary: event.summary,
                        who: who,
                      }
                    }
                  },
                  (error: Error) => {
                    Sentry.captureException("listupcomingevents: " + error)
                  }
                )

                events = events.filter(event => event !== null)

                callback({ isSignedIn: true, events })
              }
            )
        } else {
          let promiseArray: any[] = [];
          emailId.forEach(email => {
            promiseArray.push(gapi.client.calendar.events
              .list({
                calendarId: email,
                timeMin: new Date().toISOString(),
                showDeleted: false,
                singleEvents: true,
                maxResults: 50,
                orderBy: "startTime",
              })
              .then(
                (response: {
                  status: number
                  result: {
                    items: {
                      id: number
                      description?: string
                      summary: string
                      end: EventTime
                      start: EventTime
                      who?: string
                      attendees?: any
                    }[]
                  }
                }) => {
                  if (
                    response.status !== 200 ||
                    response.result.items.length === 0
                  ) {
                    Sentry.captureException("Response not valid: " + response)
                  }
                  let events = response.result.items
  
                  if (events.length === 0) {
                    Sentry.captureException("Events length is 0 - " + emailId)
                  }
  
                  events.forEach(
                    (event, i) => {
                      let who = event.description
                      if (event.summary.toLowerCase().indexOf("birthday") !== -1) {
                        who = "birthday"
                      } else if (
                        event.summary.toLowerCase().indexOf("cook ") === 0
                      ) {
                        who = "cooking"
                      }
  
                      if (!who || who === "") {
                        who = analyseTitle(event.summary.toLowerCase())
                      }
  
                      // remove rejected events
                      if(event.attendees && event.attendees[0].self === true && event.attendees[0].responseStatus === 'declined') {
                        events[i] = null as any
                      } else {
                        events[i] = {
                          id: event.id,
                          end: event.end,
                          start: event.start,
                          summary: event.summary,
                          who: who,
                        }
                      }
                    },
                    (error: Error) => {
                      Sentry.captureException("listupcomingevents: " + error)
                    }
                  )
  
                  events = events.filter(event => event !== null)
  
                  // callback({ isSignedIn: true, events })
                  return { isSignedIn: true, events }
                }
              )
            )
          });

          Promise.all(promiseArray)
            .then(values => {
              const mergedEvents = mergeArray(values[0].events, values[1].events)
              callback({ isSignedIn: values[0].isSignedIn, events: mergedEvents })
            })
              
          // })
        }
      } catch (e) {
        Sentry.captureException("listupcomingevents try catch: " + e)
      }
    }

    return gapi.load("client:auth2", () => {
      gapi.client
        .init({
          apiKey: config.API_KEY,
          clientId: config.CLIENT_ID,
          discoveryDocs: DISCOVERY_DOCS,
          scope: SCOPES,
        })
        .then(
          function () {
            // Listen for sign-in state changes.
            gapi.auth2
              .getAuthInstance()
              .isSignedIn.listen(updateSigninStatus)

            // Handle the initial sign-in state.
            updateSigninStatus(
              gapi.auth2.getAuthInstance().isSignedIn.get()
            )
          },
          (error: Error) => {
            Sentry.captureException("gapi load: " + JSON.stringify(error))
          }
        )
    })
  }

  function analyseTitle(title: string) {
    for (var i = 0; i < whoOptions.length; i++) {
      if (title.indexOf(whoOptions[i]) !== -1) {
        return whoOptions[i]
      }
    }

    return ""
  }

  function displayDate(
    eventStart: EventTime,
    timeService: TimeServiceType,
    inlineString: boolean,
    allowRepeatDates = false
  ) {
    var date =
      "dateTime" in eventStart
        ? new Date(eventStart.dateTime)
        : new Date(eventStart.date)
    const currentDate = new Date()
    const currentDateInTime = currentDate.getTime()

    if (date.toDateString() !== lastDate || allowRepeatDates) {
      const dateToUse = date.getTime() < currentDateInTime ? currentDate : date
      lastDate = dateToUse.toDateString()
      const dateToDisplay = timeService.parseDateValue(
        dateToUse,
        true,
        inlineString
      )
      return dateToDisplay
    }
    return null
  }

  function resetLastDate() {
    lastDate = ""
  }

  function getWhoClass(eventWho: string) {
    return eventWho.toLowerCase()
  }

  function parseEvents(
    response: {
      isSignedIn: boolean
      events: CalendarEvent[]
    },
    type: string
  ) {
    if (!response.isSignedIn || !type) {
      return {
        isSignedIn: response.isSignedIn,
        events: [response.events],
      }
    } else {
      const events = response.events
      const compareDate = new Date()

      let eventsToSet = []
      if (type.indexOf("today") !== -1) {
        let todayEvents = []
        for (let i = 0; i < events.length; i++) {
          const e = events[i].start
          const eventStart =
            "dateTime" in e ? new Date(e.dateTime) : new Date(e.date)

          if (eventStart.toDateString() === compareDate.toDateString()) {
            todayEvents.push(events[i])
          } else {
            break
          }
        }

        if (todayEvents.length !== 0) {
          eventsToSet.push(...todayEvents)
        }
      }

      if (type.indexOf("tomorrow") !== -1) {
        let tomorrowEvents = []
        compareDate.setDate(compareDate.getDate() + 1)
        for (let i = 0; i < events.length; i++) {
          const e = events[i].start
          const eventStart =
            "dateTime" in e ? new Date(e.dateTime) : new Date(e.date)
          if (eventStart.toDateString() === compareDate.toDateString()) {
            tomorrowEvents.push(events[i])
          }
          // TODO don't iterate through all
        }

        eventsToSet.push(...tomorrowEvents)
      }

      if (type.indexOf("birthday2weeks") !== -1) {
        let birthdayEvents = []
        compareDate.setDate(compareDate.getDate() + 30)
        for (let i = 0; i < events.length; i++) {
          const e = events[i].start
          const eventStart =
            "dateTime" in e ? new Date(e.dateTime) : new Date(e.date)
          if (
            events[i].summary.toLowerCase().indexOf("birthday") !== -1 &&
            (eventStart < compareDate || birthdayEvents.length < 6)
          ) {
            birthdayEvents.push(events[i])
          }

          if (eventStart > compareDate && birthdayEvents.length >= 5) {
            break
          }
        }
        eventsToSet.push(...birthdayEvents)
      }

      return { isSignedIn: response.isSignedIn, events: eventsToSet }
    }
  }

  function parseEventsByDate(
    events: CalendarEvent[],
    timeService: TimeServiceType
  ) {
    let returned: CalendarEvent[][] = []
    let lastStart = ""
    const currentDate = new Date()
    const currentDateInTime = currentDate.getTime()

    events.forEach(event => {
      if (event.start) {
        var date =
          "dateTime" in event.start
            ? new Date(event.start.dateTime)
            : new Date(event.start.date)
        const compareDate =
          date.getTime() < currentDateInTime
            ? currentDate.toLocaleDateString()
            : date.toLocaleDateString()
        if (compareDate !== lastStart) {
          returned.push([event])
        } else {
          returned[returned.length - 1].push(event)
        }

        lastStart = compareDate
      }
    })
    return returned.slice(0, 15)
  }

  return {
    setAuthTokens,
    getServerEvents,
    getEvents,
    handleSignIn,
    getTime,
    displayDate,
    getWhoClass,
    resetLastDate,
    parseEvents,
    parseEventsByDate,
    getSignedInStatus
  }
}

export default CalendarService
