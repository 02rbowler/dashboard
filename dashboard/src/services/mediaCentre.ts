export type WatchlistData = {
  user: number
  nextEpisode: string
  name: string
  poster_path: string
  type: "tv" | "movie"
  id: number
  isEpisodeAvailable?: boolean
  ts: number
}

export type MovieList = {
  id: number
  original_title: string
  overview: string
  poster_path: string
  release_date: string
  title: string
}

export type TVList = {
  first_air_date: string
  id: number
  name: string
  original_name: string
  overview: string
  poster_path: string
}

export const mediaCentreService = {
  parseFeatured: (
    movieList: MovieList[],
    tvList: TVList[],
    watchlist: WatchlistData[]
  ) => {
    const listToReturn: (MovieList | TVList)[] = []
    const watchlistTitle = mediaCentreService.getWatchlistTitles(watchlist)

    for (let i = 0; i < movieList.length; i++) {
      if (!watchlistTitle.includes(movieList[i].original_title))
        listToReturn.push(movieList[i])

      if (listToReturn.length === 2) {
        break
      }
    }
    for (let i = 0; i < tvList.length; i++) {
      if (!watchlistTitle.includes(tvList[i].name)) listToReturn.push(tvList[i])

      if (listToReturn.length === 4) {
        break
      }
    }

    return listToReturn
  },

  getWatchlistTitles: (watchlist: WatchlistData[]) => {
    const titleList = []
    for (let i = 0; i < watchlist.length; i++) {
      titleList.push(watchlist[i].name)
    }

    return titleList
  },
}
