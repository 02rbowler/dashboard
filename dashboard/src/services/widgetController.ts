import { SportsCentreQuery, TvQuery } from "../api";
import { CalendarData } from "../reducers/calendar";
import { NewsItem, TubeObj } from "../types";
import { getAllFixturesByType, hasAllEventsFinished, hasEventFinished } from "./sportsCentre";
import TubeService from "./tube";
import TVService from "./tv";

export type WidgetTransformer = {
  widget: string;
  data: any;
}

export const outputWidgetArray = (
  calendarData: CalendarData,
  schedule: any[], 
  tvSchedule: TvQuery | undefined, 
  newsItems: NewsItem[] | undefined, 
  scData: SportsCentreQuery | undefined,
  tubeData: { tubeData: TubeObj[], lastUpdated: string } | undefined,
  sportsGuide: any
) => {
  const outputArray: WidgetTransformer[] = [];

  schedule.map(scheduleItem => {
    switch(scheduleItem.widget) {
      case "tv": {
        const tvOutput = tvSchedule ? TVService.parseData(tvSchedule.TV) : null
        const extraSpace = tvOutput && tvOutput.length % 3 !== 0 && (3 - tvOutput.length % 3)
        tvOutput && outputArray.push(...tvOutput)
        extraSpace && outputArray.push(...Array(extraSpace))
        break;
      }
      case "news": {
        const newsOutput = newsItems ? newsItems.map(item => ({ widget: "news", data: { title: item.title, description: item.description }})) : null
        newsOutput && outputArray.push(...newsOutput.slice(0, 3))
        if (newsOutput && newsOutput.length < 3) { outputArray.push(...Array(3 - newsOutput.length))}
        break;
      }
      case "tube": {
        outputArray.push({
          widget: "tube",
          data: {
            liveStation: true
          }
        })
        if(tubeData) {
          const parsedData = TubeService.getDataToParse(tubeData.tubeData)
          if(parsedData.length > 0) {
            outputArray.push({
              widget: "tube",
              data: {
                lineStatus: parsedData,
                lastUpdated: tubeData.lastUpdated
              }
            })
            if(parsedData.length > 1) {
              outputArray.push({
                widget: "tube",
                data: {
                  lineStatus: null
                }
              })
              // if(parsedData.length > 2) {
              //   outputArray.push({
              //     widget: "tube",
              //     data: {
              //       lineStatus: null
              //     }
              //   })
              // } else {
              //   outputArray.push(...Array(1))
              // }
            } else {
              // outputArray.push(...Array(2))
              outputArray.push(...Array(1))
            }
          }
        } else {
          outputArray.push(...Array(2))
        }
        break;
      }
      case "birthdays": {
        if(calendarData.events && calendarData.events.length > 0) {
          const compareDate = new Date()
          let birthdayEvents = []
          compareDate.setDate(compareDate.getDate() + 30)
          for (let i = 0; i < calendarData.events.length; i++) {
            const e = calendarData.events[i].start
            const eventStart =
              "dateTime" in e ? new Date(e.dateTime) : new Date(e.date)
            if (
              calendarData.events[i].summary.toLowerCase().indexOf("birthday") !== -1 &&
              (eventStart < compareDate || birthdayEvents.length < 7)
            ) {
              birthdayEvents.push(calendarData.events[i])
            }

            if (eventStart > compareDate && birthdayEvents.length >= 6) {
              break
            }
          }

          outputArray.push(...[{
            widget: "birthdays",
            data: {
              events: birthdayEvents
            }
          }, {
            widget: "birthdays",
            data: {
              events: birthdayEvents,
              invisible: true
            }
          }, ...Array(1)])
        }
        break;
      }
      case "sportsCentre": {
        let f1Index: number | null = null;
        if(scData?.SportsCentre.schedule.length && scData.SportsCentre.schedule.length > 0 && hasAllEventsFinished(scData.SportsCentre.schedule).length !== 0) {
          const transformed = scData.SportsCentre.schedule.map((item, idx) => {
            if(item.competition.toLowerCase().indexOf('f1 ') !== -1) {
              if(f1Index === null) { 
                f1Index = idx;
                // @ts-ignore
                const events = getAllFixturesByType(scData.SportsCentre.schedule, "f1")
                if(events.length > 0) {
                  return {
                    widget: "sportsCentre",
                    data: {
                      f1Events: events
                    }
                  }
                } else {
                  return null
                }
              } else {
                return null
              }
            }
            if(!item.startTime || !hasEventFinished(item.startTime, item.liveLengthHours)){
              return {
                widget: "sportsCentre",
                data: item
              }
            }

            return null;
          }).filter(item => item !== null) as WidgetTransformer[]
          // if(f1Index !== null) {
          //   transformed.splice(f1Index + 1, 0, { widget: "sportsCentre", data: { invisible: true }})
          // }
          outputArray.push(...transformed)
          const extraSpace = (3 - scData.SportsCentre.schedule.length % 3)
          outputArray.push(...Array(extraSpace))
        }

        if(scData?.SportsCentre.nextF1 && scData?.SportsCentre.nextF1.date && f1Index === null) {
          let indexToUse = null;
          for(let i=0; i<outputArray.length && indexToUse === null; i++) {
            if(!outputArray[i]) {
              indexToUse = i;
              outputArray[i] = {
                widget: "nextF1",
                data: scData.SportsCentre.nextF1
              }
            }
          }
        }

        if(sportsGuide && sportsGuide.mainEvent.length > 0) {
          let indexToUse = null;
          for(let i=0; i<outputArray.length && indexToUse === null; i++) {
            if(!outputArray[i]) {
              indexToUse = i;
              outputArray[i] = {
                widget: "skySportsGuide",
                data: undefined
              }
            }
          }
        }
        // const newsOutput = newsItems ? newsItems.map(item => ({ widget: "news", data: { title: item.title, description: item.description }})) : null
        // console.log("SECOND", newsOutput)
        // newsOutput && outputArray.push(...newsOutput.slice(0, 3))
        // if (newsOutput && newsOutput.length < 3) { outputArray.push(...Array(3 - newsOutput.length))}
        break;
      }
    }
  })

  return outputArray
}