import * as Sentry from "@sentry/react"

const configFetchService = () => {
  setTimeout(() => {
    fetch(process.env.REACT_APP_HEROKU_BACKEND + "fetchprivatekeys", {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify({
        location: localStorage.getItem("userAccount"),
      }),
    }).catch(err => {
      Sentry.captureException("Failed to fetch private keys: " + err)

      setTimeout(() => configFetchService(), 3000000)
    })
  }, 2000)
}

export default configFetchService
