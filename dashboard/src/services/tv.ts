import { Episode, Maybe, Tv } from "../api"
import { TVProps } from "../components/widgets/TV"
import { ChannelType } from "../types"
import { WidgetTransformer } from "./widgetController"

interface TVWidgetTransformer extends WidgetTransformer {
  data: TVProps
}

const TVService = {
  convertTimeString(
    str: string | null,
    onlyEveningShows: boolean
  ): string | null {
    if (!str) {
      return null
    }

    const strArray = str.split("")
    strArray.splice(4, 0, "-")
    strArray.splice(7, 0, "-")
    strArray.splice(10, 0, " ")
    strArray.splice(13, 0, ":")
    strArray.splice(16, 0, ":")
    const date = new Date(strArray.join(""))
    const today = new Date()

    if ((today.getDay() === 0 || today.getDay() === 6) && onlyEveningShows) {
      if (date.getHours() < 18 || date.getHours() > 22) {
        return null
      }
    } else if (
      (date.getHours() < 20 || date.getHours() > 22) &&
      onlyEveningShows
    ) {
      return null
    }

    const hours = date.getHours() < 10 ? "0" + date.getHours() : date.getHours()
    const minutes =
      date.getMinutes() < 10 ? "0" + date.getMinutes() : date.getMinutes()
    return hours + ":" + minutes
  },

  hasAlreadyFinished(
    endTime: Maybe<string> | undefined,
    currentTime: Date
  ): boolean {
    if (!endTime) {
      return false
    }
    const showEndTime = endTime.split(":")
    if (
      parseInt(showEndTime[0]) < currentTime.getHours() ||
      (parseInt(showEndTime[0]) === currentTime.getHours() &&
        parseInt(showEndTime[1]) <= currentTime.getMinutes())
    ) {
      return true
    }
    return false
  },

  getChannelIds(channelNodes: HTMLCollectionOf<Element>) {
    let idArray: (string | null)[] = ["", "", "", ""]

    for (let i = 0; i < channelNodes.length; i++) {
      const channelName =
        channelNodes[i].getElementsByTagName("display-name")[0].innerHTML
      if (channelName.toLowerCase().indexOf("bbc one") === 0) {
        idArray[0] = channelNodes[i].getAttribute("id")
      } else if (channelName.toLowerCase().indexOf("bbc two") === 0) {
        idArray[1] = channelNodes[i].getAttribute("id")
      } else if (channelName.toLowerCase() === "itv") {
        idArray[2] = channelNodes[i].getAttribute("id")
      } else if (channelName.toLowerCase() === "channel 4") {
        idArray[3] = channelNodes[i].getAttribute("id")
      }
    }

    return idArray
  },

  parseShows(
    channelId: string | null,
    showNodes: HTMLCollectionOf<Element>,
    props: { onlyEveningShows: boolean },
    currentTime: Date
  ) {
    const showArray: Episode[] = []
    for (let i = 0; i < showNodes.length; i++) {
      if (showNodes[i].getAttribute("channel") === channelId) {
        const showName = showNodes[i].getElementsByTagName("title")[0].innerHTML
        const startTime = TVService.convertTimeString(
          showNodes[i].getAttribute("start"),
          props.onlyEveningShows
        )
        const endTime =
          TVService.convertTimeString(
            showNodes[i].getAttribute("stop"),
            props.onlyEveningShows
          ) || "23:59"
        if (startTime && !TVService.hasAlreadyFinished(endTime, currentTime)) {
          showArray.push({
            title: showName,
            startTime,
            endTime,
          })
        }
      }
    }
    return showArray
  },

  filterShowsByTime(
    shows: Episode[],
    onlyEveningShows: boolean,
    currentTime: Date
  ) {
    return shows.filter(show => {
      const startTimeSplit = show.startTime.split(":")
      const hours = parseInt(startTimeSplit[0])
      // const minutes = startTimeSplit[1];

      if (
        (currentTime.getDay() === 0 || currentTime.getDay() === 6) &&
        onlyEveningShows
      ) {
        if (hours < 18 || hours > 22) {
          return false
        }
      } else if ((hours < 20 || hours > 22) && onlyEveningShows) {
        return false
      }

      return true
    })
  },

  parseData(inputData: Tv): TVWidgetTransformer[] | null {
    const currentTime = new Date()
    if (!inputData) {
      return null
    }
    const bbc1Shows = TVService.filterShowsByTime(
      inputData.bbc1.shows,
      true,
      currentTime
    )
    const bbc1 = bbc1Shows.length > 0 ? {
      widget: "tv",
      data: {
        shows: TVService.filterShowsByTime(
          inputData.bbc1.shows,
          true,
          currentTime
        ),
        displayName: "BBC One",
        background: "#FF0000",
      }
    } : null

    const bbc2Shows = TVService.filterShowsByTime(
      inputData.bbc2.shows,
      true,
      currentTime
    )
    const bbc2 = bbc2Shows.length > 0 ? {
      widget: "tv",
      data: {
        shows: bbc2Shows,
        displayName: "BBC Two",
        background: "#00EEB5",
      }
    } : null

    const itvShows = TVService.filterShowsByTime(
      inputData.itv.shows,
      true,
      currentTime
    )
    const itv = itvShows.length > 0 ? {
      widget: "tv",
      data: {
        shows: itvShows,
        displayName: "ITV 1",
        background: "#525252",
      }
    } : null

    const c4Shows = TVService.filterShowsByTime(
      inputData.c4.shows,
      true,
      currentTime
    )
    const c4 = c4Shows.length > 0 ? {
      widget: "tv",
      data: {
        shows: c4Shows,
        displayName: "Channel 4",
        background: "#0E4980",
      }
    } : null

    return [bbc1, bbc2, itv, c4].filter(item => item !== null) as WidgetTransformer[]
  },

  reduceArray(input: Episode[]) {
    var currentTime = new Date()
    var startIndex = 0

    for (let i = 0; i < input.length; i++) {
      if (!TVService.hasAlreadyFinished(input[i].endTime, currentTime)) {
        startIndex = i
        break
      }
    }

    return input.slice(startIndex, startIndex + 4)
  },
}

export default TVService
