import { ReactNode } from "react"
import { Episode } from "./api"

export type Config = {
  [T: string]: any
}

export interface CalendarEvent {
  start: { dateTime: string } | { date: string }
  id: number
  who: string
  summary: string
}

export interface NewsItem {
  guid: string
  title: string
  description: string
  pubDate: string
}

export interface PodcastType {
  id: number
  episodeImage: string
  title: string
  episodeDate: string
  episodeName: string
  episodeDescription: string
}

export interface ReminderType {
  data: {
    contents: string
    user: number
  }
  ref: {
    "@ref": {
      id: string
    }
  }
}

export interface TubeObj {
  id: string
  name: string
  lineStatuses: {
    statusSeverityDescription: string
    reason: string
  }[]
}

export type ChannelType = {
  background: string
  displayName: string
  widget: "tv"
  shows: Episode[]
} | null

export interface ScheduleItem {
  displayName: string
  widget: string
  extra: any
  positioned: "window" | "stack"
}

interface ScheduleDay {
  morning: ScheduleItem[]
  afternoon: ScheduleItem[]
  evening: ScheduleItem[]
}

export interface ScheduleType {
  weekday: ScheduleDay
  weekend: ScheduleDay
}

export interface PusherMessage {
  spotify?: string[]
  command?: string
  widget?: string
  authCode?: string
  message?: {
    cctvUrl: string
  }
}

export interface Account {
  email: string
  schedule: ScheduleType
  userId: number
  displayName: string
  location: string
  strava: {
    access: string,
    refresh: string
  }
  ref?: number
}

export type PusherSubscribedStatus =
| "FAIL"
| "WAITING"
| "SUCCESS"

export interface ViewServiceType {
  getIsDarkMode: () => boolean
  getCurrentSchedule: (schedule: ScheduleType | undefined) => ScheduleItem[]
  getLayout: (
    schedule: ScheduleType,
    email: string,
    cctvMode: boolean | undefined,
    config: Config
  ) => StackLayout
  parsePusher: (
    pusherMessage: PusherMessage,
    refetchReminders: () => void,
    refetchExercise: () => void,
  ) => JSX.Element | null
  parseValues: (
    value: ScheduleItem[],
    config: Config,
    findForStack?: boolean
  ) => JSX.Element[]
}

export interface StackLayout {
  widgets: ReactNode[]
  stackWidgets: ReactNode[]
  default?: ReactNode
  showExtras?: boolean
  calendarElem?: ReactNode
}

export type SportsFixture = {
  channel: string
  competition: string
  eventText: string
  fixture: string
  liveLengthHours: number
  pageName: string
  startTime: string | null | undefined
}

export interface SportsCentreData {
  fixtures: SportsFixture[]
  nextRace: {
    location: string | null
    date: string | null
  }
}
