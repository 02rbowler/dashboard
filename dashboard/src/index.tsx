import React from "react"
import ReactDOM from "react-dom/client"
import "./index.css"
import App from "./App"
import * as Sentry from "@sentry/browser"
import * as serviceWorker from "./serviceWorker"
// import { createStore } from 'redux';
import store from "./reducers/store"
import { Provider } from "react-redux"
import { Auth0Provider } from "@auth0/auth0-react"
import {
  ApolloClient,
  ApolloProvider,
  createHttpLink,
  InMemoryCache,
} from "@apollo/client"
import { setContext } from "@apollo/client/link/context"

// const store = createStore(allReducers);

const { version } = require("./../package.json")

Sentry.init({
  release: "dashboard@" + version,
  dsn: "https://332e3bd1183642bb83d1cfa6f29e0f22@sentry.io/5185369",
  beforeSend(event) {
    if (window.location.hostname === "localhost") {
      return null
    }

    return event
  },
})

const httpLink = createHttpLink({
  uri: `${process.env.REACT_APP_NETLIFY_ENDPOINT}.netlify/functions/graphql`,
})

const authLink = setContext((_, { headers }) => {
  // get the authentication token from local storage if it exists
  const token = localStorage.getItem("userAccount")
  // return the headers to the context so httpLink can read them
  return {
    headers: {
      ...headers,
      authorization: token ? `${token}` : "",
    },
  }
})

const client = new ApolloClient({
  link: authLink.concat(httpLink),
  cache: new InMemoryCache(),
})

const root = ReactDOM.createRoot(document.getElementById("root") as HTMLElement)
root.render(
  <ApolloProvider client={client}>
    <Provider store={store}>
      <Auth0Provider
        domain="dev-093yrzg.eu.auth0.com"
        clientId="KDkcziTDMlaGlJolr2dygqzsErIaBI0c"
        redirectUri={window.location.origin}
      >
        <App />
      </Auth0Provider>
    </Provider>
  </ApolloProvider>
)

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister()
