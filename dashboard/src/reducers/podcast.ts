import * as Sentry from "@sentry/browser"
import { createSlice, createAsyncThunk } from "@reduxjs/toolkit"
import Axios from "axios"
import SpotifyService from "../services/spotify"
import { PodcastType } from "../types"

interface podcastShow {
  show: {
    id: number
    name: string
    images: {
      url: string
    }[]
  }
}

interface state {
  shows: podcastShow[]
  episodes: PodcastType[]
  status: string | number
  error: null
}

const initialState: state = {
  shows: [],
  episodes: [],
  status: "idle",
  error: null,
}

export const fetchPodcastList = createAsyncThunk(
  "podcast/fetchPodcastList",
  (accessToken: string, thunkAPI) => {
    return Axios.get("https://api.spotify.com/v1/me/shows", {
      headers: {
        Authorization: "Bearer " + accessToken,
      },
    })
      .then(res => {
        if (res.data) {
          return res.data.items
          // .show.id .show.name .show.images[1].url
        }

        Sentry.captureException("Podcast list fetch returned no data")
        return []
      })
      .catch(err => {
        if (err.response.data.error.message === "The access token expired") {
          const response = SpotifyService.requestRefreshToken()
        } else {
          Sentry.captureException(
            "Podcast list fetch failed: " + err.response.data.error.message
          )
        }
      })
  }
)

export const fetchLatestEpisodes = createAsyncThunk(
  "podcast/fetchLatestEpisodes",
  async ([shows, accessToken]: any[], thunkAPI) => {
    let showArray: PodcastType[] = []
    for (let i = 0; i < shows.length; i++) {
      await Axios.get(
        `https://api.spotify.com/v1/shows/${shows[i].show.id}/episodes?limit=1`,
        {
          headers: {
            Authorization: "Bearer " + accessToken,
          },
        }
      )
        .then(res => {
          if (res.data) {
            const episode = res.data.items[0]
            const episodeObj = {
              id: episode.id,
              title: shows[i].show.name,
              episodeDate: episode.release_date,
              episodeName: episode.name,
              episodeDescription: episode.description,
              episodeImage: episode.images[1].url,
            }
            showArray.push(episodeObj)
            return
          }

          Sentry.captureException("Podcast episode fetch returned no data")
          return
        })
        .catch(err => {
          Sentry.captureException("Podcast episode fetch failed: " + err)
        })
    }

    return SpotifyService.orderShows(showArray)
  }
)

export const podcastSlice = createSlice({
  name: "podcast",
  initialState,
  reducers: {},
  extraReducers: builder => {
    builder.addCase(fetchPodcastList.pending, (state, action) => {
      state.status = "loading"
    })
    builder.addCase(fetchPodcastList.fulfilled, (state, action) => {
      var date = new Date()
      state.status = date.getTime()
      state.shows = action.payload
    })
    builder.addCase(fetchPodcastList.rejected, state => {
      state.status = "failed"
    })
    builder.addCase(fetchLatestEpisodes.pending, (state, action) => {
      state.status = "loading"
    })
    builder.addCase(fetchLatestEpisodes.fulfilled, (state, action) => {
      var date = new Date()
      state.status = date.getTime()
      state.episodes = action.payload
    })
    builder.addCase(fetchLatestEpisodes.rejected, state => {
      state.status = "failed"
    })
  },
})

export default podcastSlice.reducer
