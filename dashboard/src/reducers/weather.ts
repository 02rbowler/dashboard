import { createSlice } from "@reduxjs/toolkit"

interface state {
  data: any
  status: string | number
  error: null
}

const initialState: state = {
  data: null,
  status: "idle",
  error: null,
}

export const weatherSlice = createSlice({
  name: "weather",
  initialState,
  reducers: {
    setForecast: (state, action: { payload: any }) => {
      if (action.payload) {
        state.data = action.payload
      }
    },
  },
})

export const { setForecast } = weatherSlice.actions
export default weatherSlice.reducer
