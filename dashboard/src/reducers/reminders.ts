import { createApi, fetchBaseQuery } from "@reduxjs/toolkit/query/react"
import * as Sentry from "@sentry/browser"
import { ReminderType } from "../types"

export const remindersApi = createApi({
  reducerPath: "remindersApi",
  baseQuery: fetchBaseQuery({
    baseUrl: process.env.REACT_APP_NETLIFY_ENDPOINT + ".netlify/functions/",
  }),
  endpoints: builder => ({
    getReminders: builder.query({
      query: (userId: number) => ({
        url: `getReminders`,
        method: "POST",
        body: JSON.stringify({ userId }),
      }),
    }),
  }),
})

// Export hooks for usage in functional components
export const { useGetRemindersQuery } = remindersApi
