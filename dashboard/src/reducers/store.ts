import { configureStore } from "@reduxjs/toolkit"

import { tubeApi } from "./tube"
import tvReducer from "./tv"
import { backgroundApi } from "./background"
import weatherReducer from "./weather"
import calendarReducer from "./calendar"
import { remindersApi } from "./reminders"
import { newsApi } from "./news"
import sportsScoresReducer from "./sportsScores"
import podcastReducer from "./podcast"
import keyStorageReducer from "./keyStorage"
import sportsCentreReducer, { sportsCentreApi } from "./sportsCentre"
import userConfigReducer from "./userConfig"
import stravaReducer from "./strava"
import { mediaApi, tmdbApi } from "./mediaCentre"
import { exerciseApi } from "./exercise"

const store = configureStore({
  reducer: {
    tvReducer,
    calendarReducer,
    sportsScoresReducer,
    podcastReducer,
    keyStorageReducer,
    sportsCentreReducer,
    userConfigReducer,
    stravaReducer,
    weatherReducer,
    [tubeApi.reducerPath]: tubeApi.reducer,
    [remindersApi.reducerPath]: remindersApi.reducer,
    [exerciseApi.reducerPath]: exerciseApi.reducer,
    [newsApi.reducerPath]: newsApi.reducer,
    [backgroundApi.reducerPath]: backgroundApi.reducer,
    [mediaApi.reducerPath]: mediaApi.reducer,
    [tmdbApi.reducerPath]: tmdbApi.reducer,
    [sportsCentreApi.reducerPath]: sportsCentreApi.reducer,
  },
  middleware: getDefaultMiddleware =>
    getDefaultMiddleware().concat(
      tubeApi.middleware,
      remindersApi.middleware,
      exerciseApi.middleware,
      newsApi.middleware,
      backgroundApi.middleware,
      mediaApi.middleware,
      tmdbApi.middleware,
      sportsCentreApi.middleware
    ),
})

export default store

export type RootState = ReturnType<typeof store.getState>
