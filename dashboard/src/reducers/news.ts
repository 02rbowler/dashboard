import * as Sentry from "@sentry/browser"
import { createApi, fetchBaseQuery } from "@reduxjs/toolkit/query/react"
import { NewsItem } from "../types"

export const newsApi = createApi({
  reducerPath: "newsApi",
  baseQuery: fetchBaseQuery({ baseUrl: "" }),
  endpoints: builder => ({
    getAllNews: builder.query({
      query: () =>
        `https://api.rss2json.com/v1/api.json?rss_url=http://feeds.bbci.co.uk/news/rss.xml`,
      transformResponse: (res: { items: NewsItem[] }, meta, arg) => {
        let numberToShow = 5

        return res.items.slice(0, numberToShow)
      },
    }),
  }),
})

// Export hooks for usage in functional components
export const { useGetAllNewsQuery } = newsApi
