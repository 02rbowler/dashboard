import { createApi, fetchBaseQuery } from "@reduxjs/toolkit/query/react"
import { TubeObj } from "../types"

export const tubeApi = createApi({
  reducerPath: "tubeApi",
  baseQuery: fetchBaseQuery({ baseUrl: "https://api.tfl.gov.uk/" }),
  endpoints: builder => ({
    getTubeData: builder.query({
      query: () => `line/mode/tube,dlr,overground,elizabeth-line/status`,
      transformResponse: (res: TubeObj[], meta, arg) => {
        const tubeLineExclusion = ["waterloo-city"]
        const tubeInfo = res.filter(
          (tubeItem: { id: string }) =>
            tubeLineExclusion.indexOf(tubeItem.id) === -1
        )
        const renamed = tubeInfo.map((tubeItem: any) => {
          if(tubeItem.id === "london-overground") {
            return {
              ...tubeItem,
              name: "Overground"
            }
          } else if (tubeItem.id === "elizabeth") {
            return {
              ...tubeItem,
              name: "Elizabeth"
            }
          }

          return tubeItem;
        })
        return {
          tubeData: renamed,
          lastUpdated: (new Date()).toLocaleTimeString()
        }
      },
    }),
  }),
})

// Export hooks for usage in functional components
export const { useGetTubeDataQuery } = tubeApi
