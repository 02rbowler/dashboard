import { createSlice, createAsyncThunk } from "@reduxjs/toolkit"
import * as Sentry from "@sentry/browser"
import Axios from "axios"

export const fetchCricketMatches = createAsyncThunk(
  "sportsScores/fetchCricketMatches",
  (cricApiKey: string) => {
    return Axios.get("https://cricapi.com/api/matches?apikey=" + cricApiKey)
      .then(res => {
        return res.data
      })
      .catch(err => {
        Sentry.captureException("Cricket scores fetch failed: " + err)
      })
  }
)

interface cricketScore {
  uniqueId: string
  cricApiKey: string
}

export const fetchCricketScore = createAsyncThunk(
  "sportsScores/fetchCricketScore",
  ({ uniqueId, cricApiKey }: cricketScore, thunkAPI) => {
    return Axios.get(
      "https://cricapi.com/api/cricketScore?unique_id=" +
        uniqueId +
        "&apikey=" +
        cricApiKey
    )
      .then(res => {
        return res.data
      })
      .catch(err => {
        Sentry.captureException("Cricket scores fetch failed: " + err)
      })
  }
)

interface state {
  data: any
  status: string | number
  error: null
}

const initialState: state = {
  data: {},
  status: "idle",
  error: null,
}

export const sportsScoresSlice = createSlice({
  name: "sportsScores",
  initialState,
  reducers: {},
  extraReducers: builder => {
    builder.addCase(fetchCricketScore.pending, (state, action) => {
      state.status = "loading"
    })
    builder.addCase(fetchCricketScore.fulfilled, (state, action) => {
      var date = new Date()
      // state.status = date.getTime();
      // state.data = action.payload;
      console.log(action.payload)
    })
    builder.addCase(fetchCricketScore.rejected, state => {
      state.status = "failed"
    })

    builder.addCase(fetchCricketMatches.pending, (state, action) => {
      state.status = "loading"
    })
    builder.addCase(fetchCricketMatches.fulfilled, (state, action) => {
      var date = new Date()
      state.status = date.getTime()
      state.data = action.payload
    })
    builder.addCase(fetchCricketMatches.rejected, state => {
      state.status = "failed"
    })
  },
})

export default sportsScoresSlice.reducer
