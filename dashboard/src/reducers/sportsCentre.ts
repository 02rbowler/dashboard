import { createSlice } from "@reduxjs/toolkit"
import { SportsCentreData } from "../types"
import { SportsCentreQuery } from "../api"
import { createApi, fetchBaseQuery } from "@reduxjs/toolkit/dist/query/react"

export const sportsCentreApi = createApi({
  reducerPath: "sportsCentreApi",
  baseQuery: fetchBaseQuery({
    baseUrl: process.env.REACT_APP_HEROKU_BACKEND,
  }),
  endpoints: builder => ({
    getSportsGuide: builder.query({
      query: () => `getSportsGuide`,
    }),
  }),
})

// Export hooks for usage in functional components
export const { useGetSportsGuideQuery } = sportsCentreApi

interface state {
  data: SportsCentreData
  status: string | number
  error: null
}

const initialState: state = {
  data: {
    fixtures: [],
    nextRace: {
      location: null,
      date: null,
    },
  },
  status: "idle",
  error: null,
}

export const sportsCentreSlice = createSlice({
  name: "sportsCentre",
  initialState,
  reducers: {
    updateSportsSchedule: (state, action: { payload: SportsCentreQuery }) => {
      if (action.payload) {
        state.data = {
          // @ts-ignore
          fixtures: action.payload.SportsCentre.schedule,
          nextRace: action.payload.SportsCentre.nextF1,
        }
      }
    },
  },
})

export const { updateSportsSchedule } = sportsCentreSlice.actions
export default sportsCentreSlice.reducer
