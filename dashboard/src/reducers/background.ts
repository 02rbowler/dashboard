import { createApi, fetchBaseQuery, retry } from "@reduxjs/toolkit/query/react"
import * as Sentry from "@sentry/browser"

const staggeredBaseQuery = retry(
  fetchBaseQuery({
    baseUrl: process.env.REACT_APP_NETLIFY_ENDPOINT + ".netlify/functions/",
  }),
  {
    maxRetries: 5,
  }
)

export const backgroundApi = createApi({
  reducerPath: "backgroundApi",
  baseQuery: staggeredBaseQuery,
  endpoints: builder => ({
    getAllBackgrounds: builder.query({
      query: () =>
        `fetchBackgroundImage?sender=${localStorage.getItem("userAccount")}`,
    }),
  }),
})

export const { useGetAllBackgroundsQuery } = backgroundApi
