import { createSlice } from "@reduxjs/toolkit"
import TVService from "../services/tv"
import { TvQuery } from "../api"
import { ChannelType } from "../types"

export type TVScheduleData = {
  bbc1: ChannelType
  bbc2: ChannelType
  itv: ChannelType
  c4: ChannelType
}

interface state {
  data: TVScheduleData | null
  status: string | number
  error: null
}

const initialState: state = {
  data: null,
  status: "idle",
  error: null,
}

export const tvSlice = createSlice({
  name: "tv",
  initialState,
  reducers: {
    resetStatus: state => {
      state.status = "idle"
    },
  },
})

export const { resetStatus } = tvSlice.actions

export default tvSlice.reducer
