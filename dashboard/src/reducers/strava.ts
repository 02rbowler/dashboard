import { createSlice } from "@reduxjs/toolkit"

interface state {
  data: number | null
}

const initialState: state = {
  data: null,
}

export const stravaSlice = createSlice({
  name: "strava",
  initialState,
  reducers: {
    setStrava: (state, action) => {
      if (action.payload) {
        state.data = action.payload
      }
    },
  },
})

export const { setStrava } = stravaSlice.actions

export default stravaSlice.reducer
