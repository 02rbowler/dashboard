import { createApi, fetchBaseQuery } from "@reduxjs/toolkit/query/react"

export const mediaApi = createApi({
  reducerPath: "mediaApi",
  baseQuery: fetchBaseQuery({ baseUrl: process.env.REACT_APP_OKTETO + "/" }),
  endpoints: builder => ({
    getWatchlist: builder.query({
      query: (userId: number) => `getMyWatchlist?userId=${userId}`,
    }),
  }),
})

export const tmdbApi = createApi({
  reducerPath: "tmdbApi",
  baseQuery: fetchBaseQuery({
    baseUrl: "https://api.themoviedb.org/3/discover/",
  }),
  endpoints: builder => ({
    getMovies: builder.query({
      query: () =>
        `movie?api_key=${process.env.REACT_APP_TMDB_API}&language=en-US&sort_by=popularity.desc&include_adult=false&include_video=false&page=1&vote_average.gte=6&with_watch_monetization_types=flatrate`,
    }),
    getTV: builder.query({
      query: () =>
        `tv?api_key=${process.env.REACT_APP_TMDB_API}&with_original_language=en&sort_by=popularity.desc&page=1&timezone=America%2FNew_York&vote_average.gte=6&with_watch_monetization_types=flatrate`,
    }),
  }),
})

// Export hooks for usage in functional components
export const { useGetWatchlistQuery } = mediaApi
export const { useGetMoviesQuery, useGetTVQuery } = tmdbApi
