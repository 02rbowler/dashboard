import { createApi, fetchBaseQuery } from "@reduxjs/toolkit/query/react"

export const exerciseApi = createApi({
  reducerPath: "exerciseApi",
  baseQuery: fetchBaseQuery({
    baseUrl: process.env.REACT_APP_NETLIFY_ENDPOINT + ".netlify/functions/",
  }),
  endpoints: builder => ({
    getExercise: builder.query({
      query: (userId: number) => ({
        url: `getExerciseData`,
        method: "POST",
        body: JSON.stringify({ userId }),
      }),
    }),
  }),
})

// Export hooks for usage in functional components
export const { useGetExerciseQuery } = exerciseApi