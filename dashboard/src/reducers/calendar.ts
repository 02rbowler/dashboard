import { createSlice } from "@reduxjs/toolkit"
import { CalendarEvent } from "../types"

interface fetchInput {
  emailId: string | number
  type: string
}

export type CalendarData = {
  isSignedIn: boolean
  events?: CalendarEvent[]
}

interface state {
  data: CalendarData
  status: string | number
  error: null
}

const initialState: state = {
  data: {
    isSignedIn: true,
    events: [],
  },
  status: "idle",
  error: null,
}

const calendarSlice = createSlice({
  name: "calendar",
  initialState,
  reducers: {
    addEvents(state, action) {
      var date = new Date()
      state.data = action.payload
      state.status = date.getTime()
    },
  },
})

export default calendarSlice.reducer

export const { addEvents } = calendarSlice.actions
