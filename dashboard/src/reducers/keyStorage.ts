import { createSlice } from "@reduxjs/toolkit"
import { Config } from "../types"

interface state {
  value: Config
}

const initialState: state = {
  value: {},
}

export const keyStorageSlice = createSlice({
  name: "keyStorage",
  initialState,
  reducers: {
    setKeys(state, action) {
      state.value = action.payload
    },
  },
})

export const { setKeys } = keyStorageSlice.actions
export default keyStorageSlice.reducer
