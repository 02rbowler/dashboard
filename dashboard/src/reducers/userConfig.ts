import { createSlice } from "@reduxjs/toolkit"
import { Account } from "../types"

interface state {
  data: Account | null
}

const initialState: state = {
  data: null,
}

export const userConfigSlice = createSlice({
  name: "userConfig",
  initialState,
  reducers: {
    setUser: (state, action) => {
      if (action.payload) {
        state.data = action.payload
      }
    },
  },
})

export const { setUser } = userConfigSlice.actions

export default userConfigSlice.reducer
