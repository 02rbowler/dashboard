import React from "react"
import WidgetHeader from "../components/shared/WidgetHeader"
import { render } from "@testing-library/react"

describe("WidgetHeader component", () => {
  it("Should match the snapshot", () => {
    const widgetHeader = render(
      <WidgetHeader>This is the header text</WidgetHeader>
    )
    expect(widgetHeader).toMatchSnapshot()
  })
})
