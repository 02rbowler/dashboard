import React from "react"
import { render } from "@testing-library/react"
import WidgetRow from "../components/shared/WidgetRow"

describe("WidgetRow component", () => {
  it("Should match the snapshot", () => {
    const component = render(<WidgetRow>Row text</WidgetRow>)
    expect(component).toMatchSnapshot()
  })
})
