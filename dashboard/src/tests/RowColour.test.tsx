import React from "react"
import { render } from "@testing-library/react"
import RowColour from "../components/shared/RowColour"

describe("RowColour component", () => {
  it("Should match the snapshot", () => {
    const component = render(<RowColour customClass="central" />)
    expect(component).toMatchSnapshot()
  })
})
