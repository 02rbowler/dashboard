import "typeface-roboto"

export const fontFamily = `font-family: 'roboto'`
export const fontSize = {
  xs: "20px",
  s: "25px",
  m: "30px",
  l: "38px",
  xl: "50px",
} as const
