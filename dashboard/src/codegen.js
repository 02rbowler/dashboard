module.exports = {
  schema: [
    {
      [`${process.env.REACT_APP_NETLIFY_ENDPOINT}.netlify/functions/graphql`]: {
        headers: {
          authorization: process.env.REACT_APP_DEFAULT_GRAPH_TOKEN,
        },
      },
    },
  ],
  documents: ["./src/api/*.graphql"],
  overwrite: true,
  generates: {
    "./src/api/index.ts": {
      plugins: [
        "typescript",
        "typescript-operations",
        "typescript-react-apollo",
      ],
      config: {
        skipTypename: false,
        withHooks: true,
        withHOC: false,
        withComponent: false,
        apolloReactHooksImportFrom: "@apollo/client",
      },
    },
  },
}
