import React, { SetStateAction, useEffect, useState } from "react"
import Background from "./components/Background"
import ApiService from "./services/api"
import DashboardWrapper from "./components/Dashboardwrapper"
import { getUserAccount } from "./data"
import CalendarService from "./services/calendar"

import styled from "styled-components"
import { Account, PusherMessage, PusherSubscribedStatus, ViewServiceType } from "./types"
import Login from "./components/Login"
import { useDispatch, useSelector } from "react-redux"
import { RootState } from "./reducers/store"
import ViewService from "./services/view"
import * as Sentry from "@sentry/react"
import { setUser } from "./reducers/userConfig"
import { setKeys } from "./reducers/keyStorage"
import Pusher from "pusher-js"
import configFetchService from "./services/configFetch"
import { useMouseMove } from "./hooks/useMouseMove"
import { useGetRemindersQuery } from "./reducers/reminders"
import { useGetExerciseQuery } from "./reducers/exercise"

const StyledApp = styled.div`
  height: 100vh;
  background-color: black;

  &.hideCursor {
    cursor: none;
  }
`

const pusherConfig = {
  key: "fcbc398aa7617818014a",
  cluster: "eu",
}

export const ThemeContext = React.createContext<{value: 'light' | 'dark', setValue: React.Dispatch<React.SetStateAction<"light" | "dark">>}>({
  value: 'light',
  setValue: (prevState: SetStateAction<'light' | 'dark'>) => prevState
});

function App() {
  const calendarService = CalendarService()
  const [viewService] = useState(ViewService(calendarService))
  const [pusherSubscribed, setPusherSubscribed] = useState<PusherSubscribedStatus>("WAITING");
  const [theme, setTheme] = useState<'light' | 'dark'>('light')

  const location = localStorage.getItem("userAccount")
  const [chosenUser, setChosenUser] = useState<Account | null>(null)
  const mouseMove = useMouseMove()
  const configKeys = useSelector(
    (state: RootState) => state.keyStorageReducer.value
  )
  const userAccount = useSelector(
    (state: RootState) => state.userConfigReducer.data
  )
  const userId = userAccount ? userAccount.userId : -1
  const { refetch: refetchReminders } = useGetRemindersQuery(userId)
  const { refetch: refetchExercise } = useGetExerciseQuery(userId)
  const dispatch = useDispatch()

  Sentry.setUser({ location })

  useEffect(() => {
    const getAccountInfo = async (location: string) => {
      const userAccount = await getUserAccount(location)
      setChosenUser(userAccount)
      dispatch(setUser(userAccount))
    }

    let interval: NodeJS.Timeout
    if (location && location !== null) {
      if (Object.keys(configKeys).length !== 0) {
        ApiService.sendHealthCheck()
        getAccountInfo(location)
        // localStorage.setItem('userAccount', location);

        interval = setInterval(() => {
          ApiService.sendHealthCheck()
        }, 600000)
      }
    }

    return () => {
      if (interval) clearInterval(interval)
    }
  }, [configKeys, location, dispatch])

  useEffect(() => {
    const pusher = new Pusher(pusherConfig.key, {
      cluster: pusherConfig.cluster,
      authEndpoint:
        process.env.REACT_APP_NETLIFY_ENDPOINT +
        ".netlify/functions/pusherAuth",
      // encrypted: true
    })

    if (localStorage.getItem("userAccount")) {
      const channel = pusher.subscribe("private-dashboard")

      channel.bind(
        "pusher:subscription_error",
        (status: { error: string }) => {
          Sentry.captureException(status.error)
          setPusherSubscribed("FAIL")
        }
      )

      channel.bind(
        "pusher:subscription_succeeded",
        () => {
          setPusherSubscribed("SUCCESS")
        }
      )

      // configFetchService()
      channel.bind(
        localStorage.getItem("userAccount") || "",
        (data: PusherMessage) => {
          if (data.spotify) {
            // SpotifyService.parseTokens(data.spotify)
            return
          } else if (data.message) {
            dispatch(setKeys(data.message))
            return
          } else if (data.authCode) {
            calendarService.setAuthTokens(data.authCode)
          }
          // const displayComp = viewService.parsePusher(data, refetchReminders)

          // setTimeoutForWindow()
          // setWindowContent(displayComp)
        }
      )

      channel.bind(
        "client-" + localStorage.getItem("userAccount") || "",
        (data: PusherMessage) => {
          const displayComp = viewService.parsePusher(data, refetchReminders, refetchExercise)
          // setTimeoutForWindow()
          // setWindowContent(displayComp)
        }
      )
    }

    return () => {
      pusher && pusher.unsubscribe("private-dashboard")
    }
  }, [dispatch, viewService])

  useEffect(() => {
    if(pusherSubscribed) {
      configFetchService()
    }
  }, [pusherSubscribed])

  if (!location && (!chosenUser || !chosenUser.schedule)) {
    return <Login />
  }

  const themeContext = {
    value: theme,
    setValue: setTheme
  }

  return (
    <Sentry.ErrorBoundary>
      <ThemeContext.Provider value={themeContext}>
        <StyledApp className={`${!mouseMove && "hideCursor"}`}>
          <Background mouseMove={mouseMove} />
          <DashboardWrapper 
            calendarService={calendarService}
            email={chosenUser?.email}
            // cctvMode={(location as string).indexOf("manorcroft_") === 0}
            schedule={chosenUser?.schedule}
            viewService={viewService as ViewServiceType}
            pusherSubscribed={pusherSubscribed}
          />
        </StyledApp>
      </ThemeContext.Provider>
    </Sentry.ErrorBoundary>
  )
}

export default App
