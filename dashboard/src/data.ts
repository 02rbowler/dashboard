import { Account } from "./types"
import * as Sentry from "@sentry/browser"

export const getUserAccount = async (
  location: string
): Promise<Account | null> => {
  return fetch(
    process.env.REACT_APP_NETLIFY_ENDPOINT +
      ".netlify/functions/getDashboardConfig"
  )
    .then(res => {
      if (res.status !== 200) {
        Sentry.captureException("Bad status from getUserAccount: " + res.status)
        return null
      }
      return res.json()
    })
    .then(res => {
      for (let i = 0; i < res.length; i++) {
        if (res[i].data.location === location) {
          return {
            ...res[i].data,
            ref: res[i].ref['@ref'].id
          }
        }
      }

      Sentry.captureException("Cannot find location for " + location)
      return null
    })
    .catch(e => {
      Sentry.captureException(e)
      return null
    })
}
