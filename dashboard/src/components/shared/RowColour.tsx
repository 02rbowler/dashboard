import React from "react"
import styled from "styled-components"

const StyledRowColour = styled.div`
  height: calc(100% - 2px);
  position: absolute;
  margin-left: -15px;
  width: 5px;
  background: orange;
  border-radius: 5px;

  &.rob {
    background: red;
  }
  &.andrew {
    background: green;
  }
  &.james {
    background: yellow;
  }
  &.cath {
    background: #fb94a6;
  }
  &.mary {
    background: #a6ff00;
  }
  &.asher {
    background: #0099ff;
  }
  &.peter {
    background: #ff6600;
  }
  &.birthday {
    background: #d801d8b5;
  }
  &.cooking {
    background: #f37600d4;
  }

  &.f1 {
    background-image: linear-gradient(45deg, black, #565656);
    width: calc(100% + 10px);
  }
`

const RowColour = ({ customClass }: { customClass: string }): JSX.Element => (
  <StyledRowColour className={customClass} />
)

export default RowColour
