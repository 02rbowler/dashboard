import React, { ReactNode } from "react"
import styled from "styled-components"

const StyledWidgetHeader = styled.div`
  padding-bottom: 2px;
  padding-left: 10px;
  font-weight: bold;
  font-size: 0.8em;
  display: initial;
  letter-spacing: 1px;
`

const WidgetHeader = ({ children }: { children: ReactNode }): JSX.Element => (
  <StyledWidgetHeader>{children}</StyledWidgetHeader>
)

export default WidgetHeader
