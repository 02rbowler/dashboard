import React, { ReactNode } from "react"
import styled from "styled-components"
import RowColour from "./RowColour"

const StyledWidgetRow = styled.div<{ colour?: string }>`
  ${props => props.colour && `color: ${props.colour};`}

  &:last-of-type {
    border-bottom: 0;
  }

  .row-container {
    position: relative;
    display: flex;
    flex: 1 1 auto;
    width: 100%;
    padding-bottom: 2px;
  }

  span:first-of-type {
    margin: 0px 5px;
    display: inline-block;
    flex-grow: 1;
    z-index: 2;
    margin-bottom: 1px;
    margin-top: 1px;
    white-space: initial;
  }

  span:last-of-type {
    margin: 0px 5px;
    margin-bottom: 1px;
    margin-top: 1px;
    float: right;
    z-index: 2;
  }

  span:nth-of-type(3) {
    margin-left: 5px;
  }
`

interface WidgetRowProps {
  rowColour?: string
  colour?: string
  children: ReactNode
}

const WidgetRow = ({
  rowColour,
  colour,
  children,
}: WidgetRowProps): JSX.Element => (
  <StyledWidgetRow colour={colour}>
    <div className="row-container">
      {rowColour && <RowColour customClass={rowColour} />}
      {children}
    </div>
  </StyledWidgetRow>
)

export default WidgetRow
