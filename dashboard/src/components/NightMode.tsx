import React from "react"
import styled from "styled-components"

const DashboardNight = styled.div`
  position: absolute;
  width: 100vw;
  height: 100vh;
  font-size: 30px;
  background-color: black;
  color: #bfbfbf;

  .dashboard-night-content {
    background-color: #3e3e3e;
    padding: 150px;
    width: fit-content;
    position: absolute;
    opacity: 0.7;
  }
`

const NightMode = () => {
  const positionNightMode = () => {
    const screenWidth = window.innerWidth - 150
    const screenHeight = window.innerHeight - 130
    const positionLeft = Math.floor(Math.random() * screenWidth)
    const positionTop = Math.floor(Math.random() * screenHeight)
    return { top: positionTop + "px", left: positionLeft + "px" }
  }

  return (
    <DashboardNight>
      <div className="dashboard-night-content" style={positionNightMode()}>
        Good Night
      </div>
    </DashboardNight>
  )
}

export default NightMode
