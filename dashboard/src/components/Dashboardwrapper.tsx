import React, { memo, useContext } from "react"
import Header from "./Header"
import Window from "./Window"
import * as Sentry from "@sentry/browser"
import { GoogleOAuthProvider } from '@react-oauth/google';

import styled from "styled-components"
import { ViewServiceType, ScheduleType, PusherSubscribedStatus } from "../types"
import { useDispatch, useSelector } from "react-redux"
import { RootState } from "../reducers/store"
import { useScreenAdjust } from "../hooks/useScreenAdjust"
import NightMode from "./NightMode"
import WindowViewController from "./WindowViewController"
import Reminders from "./widgets/Reminders"
import { useShowReminders } from "../hooks/useShowReminders"
import { fontSize } from "../styles/themes"
import { useCompleteDataFetcher } from "../hooks/useCompleteDataFetcher"
import CCTV from "./widgets/CCTV"
import { CalendarServiceType } from "../services/calendar"
import Calendar from "./widgets/Calendar"
import { ThemeContext } from "../App";
import { WidgetController } from "./WidgetController";

const DashboardContainer = styled.div<{darkMode: boolean}>`
  height: 100vh;
  overflow-y: hidden;
  box-sizing: border-box;
  display: flex;
`

const StyledDashboardWrapper = styled.div`
  width: 100%;
  height: 100vh;
  position: absolute;
  display: flex;
  transform: none;
`

const LeftSide = styled.div<{darkMode: boolean}>`
  display: flex;
  flex-direction: column;
  height: calc(100% - 40px);
  overflow-y: hidden;
  max-width: 35vw;
  width: 509px;
  background: ${props => props.darkMode ? `
    linear-gradient( 129deg, rgb(61 67 93 / 90%) 10%, rgb(13 14 43 / 75%) 82.84% );
  ` :
    `rgba(255, 255, 255, 0.7);`}
  backdrop-filter: blur(20px);
  padding: 20px 20px 20px 30px;

  ${props => props.darkMode ? `
    color: white;
  ` : ``}
`

const LoadingState = styled.div`
  font-size: ${fontSize.m};
`

const StackWrapper = styled.div`
  margin: 0 -20px 0 -40px;
  flex: none;
  overflow-y: hidden;
  transition: max-height 2s;
  max-height: 0;

  &.animate-height {
    max-height: 100%;
  }
`

type Props = {
  viewService: ViewServiceType
  schedule?: ScheduleType
  calendarService: CalendarServiceType
  email: string | string[] | undefined
  pusherSubscribed: PusherSubscribedStatus
}

const DashboardWrapper = memo(
  ({viewService, schedule, calendarService, email, pusherSubscribed}: Props): JSX.Element => {
    const screenAdjust = useScreenAdjust()
    const dispatch = useDispatch()
    const userAccount = useSelector(
      (state: RootState) => state.userConfigReducer.data
    )
    const themeContext = useContext(ThemeContext)
    const [windowWidgets, stackWidgets] = useCompleteDataFetcher(schedule, viewService, dispatch, userAccount, themeContext.setValue)

    const config = useSelector(
      (state: RootState) => state.keyStorageReducer.value
    )

    const showReminders = useShowReminders()
    const comps = viewService.parseValues(stackWidgets, config, true)

    return (
      <GoogleOAuthProvider clientId={config.CLIENT_ID}>
        <StyledDashboardWrapper>
          <DashboardContainer
            className={windowWidgets.length > 0 ? "withWindow" : ""}
            darkMode={themeContext.value === 'dark'}
          >
            <LeftSide darkMode={themeContext.value === 'dark'}>
              <Header screenAdjust={screenAdjust} />
              {email ? (
                <>
                  {comps.length > 0 && (
                    <StackWrapper
                      className={!showReminders ? "animate-height" : ""}
                    >
                      {comps.map(node => node)}
                    </StackWrapper>
                  )}
                  <Reminders show={showReminders} />
                  <Calendar
                    key={"mainCal"}
                    emailId={email}
                    calendarService={calendarService}
                    config={config}
                  />
                </>
              ) : (
                <LoadingState>
                  Loading...
                  <ul>
                    <li>Pusher connected: {pusherSubscribed === "SUCCESS" ? "Done" : pusherSubscribed === "WAITING" ? "Waiting" : "Failure"}</li>
                    <li>Config: {pusherSubscribed !== "SUCCESS" ? "Waiting" : Object.keys(config).length === 0 ? "Loading" : "Done"}</li>
                    <li>Widget schedule: {schedule ? "Done" : "Loading"}</li>
                    {!window.navigator.onLine && <li>Device not connected to internet</li>}

                    {/* TODO BE MORE SPECIFIC OVER HAS LOADING ACTUALLY STARTED */}
                  </ul>  
                </LoadingState>
              )}
            </LeftSide>
            {/* {layout && ( */}
            <Window showWindow={windowWidgets.length > 0} darkMode={themeContext.value === 'dark'}>
              <WidgetController widgetSchedule={windowWidgets} />
              {/* <WindowViewController
                widgets={windowWidgets}
                viewService={viewService}
              /> */}
            </Window>
            {/* )} */}
          </DashboardContainer>
        </StyledDashboardWrapper>
      </GoogleOAuthProvider>
    )
  }
)

export default DashboardWrapper
