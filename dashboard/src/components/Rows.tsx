import React, { ReactNode } from "react"
import styled from "styled-components"
import { fontSize } from "../styles/themes"

export const BaseRowBasicStyles = `
  background: rgba(255, 255, 255, 0.5);
  border-radius: 10px;
  margin-bottom: 4px;
  box-sizing: border-box;
`

export const StyledBaseRow = styled.div<{darkMode?: boolean}>`
  ${BaseRowBasicStyles}
  color: black;
  ${props => props.darkMode ? `
    background: rgb(41 43 80);
    color: white;
  ` : ``}
  height: calc(${fontSize.m} + 20px);
  font-size: ${fontSize.m};
  display: flex;
  align-items: center;
  padding: 0 20px;
`

export const WhiteRow = styled(StyledBaseRow)`
  background: white;
`

export const Separator = styled.div`
  height: 8px;
`

export const ContentRow = styled(StyledBaseRow)`
  height: auto;
  padding-top: 10px;
  padding-bottom: 10px;
`

export interface ColouredBackground {
  first: string
  second: string
  last: string
}

const StyledColouredRow = styled(StyledBaseRow)<{
  background: ColouredBackground,
  darkMode?: boolean
}>`
  border: 3px solid ${props => props.darkMode ? 'black' : '#ffffff'};
  background: ${props =>
    `linear-gradient(90.4deg, ${props.background.first} 0%, ${props.background.last} 100%)`};
  font-size: ${fontSize.s};
  color: white;
`

export const BaseRow = styled(StyledBaseRow)``;

export const ColouredBlock = styled.div<{background: string, whiteText?: boolean}>`
  font-size: ${fontSize.m};
  ${props => `background: ${props.background};`}
  border-radius: 5px;
  padding: 5px 10px;
  width: fit-content;
  ${props => props.whiteText ? 'color: white;' : ''}
`

export const ColouredRow = ({
  background,
  darkMode,
  children,
}: {
  background: ColouredBackground
  darkMode?: boolean
  children: ReactNode
}) => <StyledColouredRow darkMode={darkMode} background={background}>{children}</StyledColouredRow>
