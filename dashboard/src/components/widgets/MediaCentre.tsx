import React, { memo, FC, useContext } from "react"
import * as Sentry from "@sentry/react"
import { useSelector } from "react-redux"
import styled from "styled-components"

import { RootState } from "../../reducers/store"
import { fontSize } from "../../styles/themes"
import {
  useGetMoviesQuery,
  useGetTVQuery,
  useGetWatchlistQuery,
} from "../../reducers/mediaCentre"
import { mediaCentreService, WatchlistData } from "../../services/mediaCentre"
import { ThemeContext } from "../../App"

const Header = styled.div<{darkMode: boolean}>`
  font-size: ${fontSize.s};
  margin-bottom: 10px;
  ${props => props.darkMode ? 'color: white;' : ''}
`

const ImageRow = styled.div`
  margin-bottom: 20px;
  display: flex;
  width: calc(100% + 10px);

  img {
    width: calc(25% - 10px);
    margin-right: 10px;
    border-radius: 10px;

    &:last-of-type {
      margin-right: 0;
    }
  }
`

const WatchlistItem = styled.span`
  flex: 0.25;

  img {
    width: calc(100% - 10px);
  }

  div {
    margin-right: 10px;
    font-size: ${fontSize.s};
    background-color: #ffffffad;
    border-radius: 10px;
    text-align: center;
  }
`

const MediaCentre: FC = () => {
  const userAccount = useSelector(
    (state: RootState) => state.userConfigReducer.data
  )

  if (!userAccount) {
    Sentry.captureException("No user account found in Media Centre")
  }
  const userId = userAccount ? userAccount.userId : -1

  const {
    data: watchlist,
    error,
    isLoading,
    isFetching,
    refetch,
  } = useGetWatchlistQuery(userId)
  const themeContext = useContext(ThemeContext)
  const { data: movieList } = useGetMoviesQuery(undefined, {
    refetchOnMountOrArgChange: 3600
  })

  const { data: tvList } = useGetTVQuery(undefined, {
    refetchOnMountOrArgChange: 3600
  })

  if (!watchlist) {
    return null
  }

  const parseEpisode = (details: string) => {
    const [season, episode] = details.split(".")
    return `S${season} E${episode}`
  }

  const featuredList =
    movieList &&
    tvList &&
    mediaCentreService.parseFeatured(
      movieList.results,
      tvList.results,
      watchlist
    )

  return (
    <>
      <Header darkMode={themeContext.value === 'dark'}>Watchlist</Header>
      <ImageRow>
        {watchlist &&
          watchlist
            .filter((item: WatchlistData) => {
              return item.type === "movie" || item.isEpisodeAvailable
            })
            .sort((a: WatchlistData, b: WatchlistData) => a.ts < b.ts ? 1 : -1)
            .slice(0, 4).map((item: WatchlistData) => (
              <WatchlistItem key={item.id}>
                <img
                  src={`https://image.tmdb.org/t/p/w500${item.poster_path}`}
                  alt="poster"
                />
                {item.type === "tv" && (
                  <div>{parseEpisode(item.nextEpisode)}</div>
                )}
              </WatchlistItem>
          ))}
      </ImageRow>
      <Header darkMode={themeContext.value === 'dark'}>Featured</Header>
      <ImageRow>
        {featuredList &&
          featuredList.map((item: WatchlistData) => (
            <img
              key={item.id}
              src={`https://image.tmdb.org/t/p/w500${item.poster_path}`}
              alt="featured poster"
            />
          ))}
      </ImageRow>
    </>
  )
}

export default memo(MediaCentre)
