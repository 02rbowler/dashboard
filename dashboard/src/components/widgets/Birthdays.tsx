import React, { memo, useContext } from "react"
import TimeService from "../../services/time"
import { useSelector } from "react-redux"

import { RootState } from "../../reducers/store"
import { BaseRow, ColouredBlock, ColouredRow } from "../Rows"
import { CalendarData } from "../../reducers/calendar"
import { CalendarServiceType } from "../../services/calendar"
import { CalendarEvent, Config } from "../../types"
import { ThemeContext } from "../../App"
import styled from "styled-components"
import { WidgetOutline } from "../Widget"
import { fontSize } from "../../styles/themes"

const WidthOutline = styled(WidgetOutline)<{largeWidth?: boolean}>`
  ${props => props.largeWidth ? 'width: calc(100% / 3);' : ''}
`

const Row = styled.div<{darkMode: boolean}>`
  font-size: ${fontSize.m};
  display: flex;
  margin-bottom: 2px;
  ${props => props.darkMode ? 'color: white;' : ''}
`

const ColumnWrapper = styled.div`
  display: flex;
  gap: 50px;
  
  div {
    flex: 1;
  }
`

interface CalendarProps {
  events: CalendarEvent[]
  invisible: boolean
}

const Birthdays = ({
  events,
  invisible,
}: CalendarProps): JSX.Element | null => {
  const removeSuffix = (input: string) => {
    const lastIndex = input.lastIndexOf("s ")
    return input.substring(0, lastIndex - 1)
  }

  const displayDate = (
    eventStart: any,
    inlineString: boolean,
  ) => {
    var date =
      "dateTime" in eventStart
        ? new Date(eventStart.dateTime)
        : new Date(eventStart.date)
    const currentDate = new Date()
    const currentDateInTime = currentDate.getTime()

    const dateToUse = date.getTime() < currentDateInTime ? currentDate : date
    const dateToDisplay = timeService.parseDateValue(
      dateToUse,
      true,
      inlineString
    )
    return dateToDisplay
  }

  const timeService = TimeService()
  const themeContext = useContext(ThemeContext)

  if(invisible && events.length > 3) {
    return null
  } else if(invisible) {
    <WidthOutline darkMode={themeContext.value === 'dark'}>
      {events.slice(3).map(event => (
        <Row key={event.id} darkMode={themeContext.value === 'dark'}>
          <span style={{ flex: 1 }}>{removeSuffix(event.summary)}</span>
          <span>
            {displayDate(event.start, true)}
          </span>
        </Row>
      ))}
    </WidthOutline>
  }

  return (
    <WidthOutline largeWidth={events.length > 3} darkMode={themeContext.value === 'dark'}>
      <ColouredBlock background={'#8f00ff'} whiteText={false} style={{fontSize: fontSize.s, fontWeight: 'bold', marginBottom: '10px'}}>
        Upcoming birthdays
      </ColouredBlock>
      {events.length > 3 ? <>
        <ColumnWrapper>
          <div>
            { events.slice(0, 3).map(event => (
              <Row key={event.id} darkMode={themeContext.value === 'dark'}>
                <span style={{ flex: 1 }}><b>{removeSuffix(event.summary)}</b></span>
                <span>
                  {displayDate(event.start, true)}
                </span>
              </Row>
            ))}
          </div>
          <div>
            { events.slice(3).map(event => (
              <Row key={event.id} darkMode={themeContext.value === 'dark'}>
                <span style={{ flex: 1 }}><b>{removeSuffix(event.summary)}</b></span>
                <span>
                  {displayDate(event.start, true)}
                </span>
              </Row>
            ))}
          </div>
        </ColumnWrapper>
      </> : events.map(event => (
          <Row key={event.id} darkMode={themeContext.value === 'dark'}>
            <span style={{ flex: 1 }}><b>{removeSuffix(event.summary)}</b></span>
            <span>
              {displayDate(event.start, true)}
            </span>
          </Row>
        ))
      }
    </WidthOutline>
  )
}

export default memo(Birthdays)
