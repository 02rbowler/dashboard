import React, { useContext, useEffect } from "react"

import WeatherService, { ParsedMetOfficeWeather } from "../../services/weather"
import { WindowWidgetWrapper } from "../WidgetWrapper"

import styled from "styled-components"
import store from "../../reducers/store"
import { memo } from "react"
import {
  setForecast,
  // useGetForecastQuery,
} from "../../reducers/weather"
import { ThemeContext } from "../../App"
import { fontSize } from "../../styles/themes"
import { useDispatch } from "react-redux"

const ForecastItem = styled.div`
  display: inline-block;
  width: 33%;
  position: relative;

  .text-wrapper {
    text-align: center;

    .temp {
      padding-right: 10px;
      margin-right: 10px;
      border-right: 1px solid white;
      font-weight: bold;
    }
  }
`

const IconWrapper = styled.div`
  text-align: center;
  position: absolute;
  top: -100px;
  left: 0;
  right: 0;
  color: white;
`

const SpaceDivider = styled.div`
  height: 140px;
`

const Temperature = styled.div`
  font-size: 100px;
  margin-right: 130px;
`

const TemperatureWrapper = styled.div`
  display: flex;
  color: white;
  justify-content: center;
  align-items: center;
`

const TemperatureVariation = styled.div`
  font-size: 60px;
`

const TempText = styled.div`
  font-size: 40px;
`

const WeatherBackground = styled.div<{darkMode: boolean}>`
  background: ${props => props.darkMode ? 
    `linear-gradient(96.95deg, #00e0ff61 0%, #0a20e261 50%, #00e0ff61 100%);` :
    `linear-gradient(96.95deg, #00e0ff 0%, #0a20e2 50%, #00e0ff 100%);`
  }
  height: 100%;
  margin: -30px -40px;
  padding: 30px 20px 20px;
`

const CustomHeightRow = styled.div`
  padding-top: 80px;
`

const Content = styled.div`
  font-size: ${fontSize.m};
`

const Weather = ({
  location,
  asWindow,
}: {
  location: string
  asWindow: boolean
}): JSX.Element => {
  const state = store.getState()
  const weatherApiId = state.keyStorageReducer.value.weatherID
  const dispatch = useDispatch()
  const data = state.weatherReducer.data
  const forecastState = data ? WeatherService.parseForecast(data) : null

  useEffect(() => {
    let timeout: NodeJS.Timeout;
    if(!forecastState) {
      fetch(`${process.env.REACT_APP_NETLIFY_ENDPOINT}.netlify/functions/getWeather`, {
        method: 'POST',
        body: JSON.stringify({location, weatherAPI: weatherApiId})
      }).then(res => res.json())
      .then(res => {
        dispatch(setForecast((JSON.parse(res)).SiteRep.DV.Location.Period))
      })
      timeout = setInterval(() => {
        fetch(`${process.env.REACT_APP_NETLIFY_ENDPOINT}.netlify/functions/getWeather`, {
          method: 'POST',
          body: JSON.stringify({location, weatherAPI: weatherApiId})
        }).then(res => res.json())
        .then(res => {
          dispatch(setForecast((JSON.parse(res)).SiteRep.DV.Location.Period))
        })
      }, 3600000);
    }

    return () => clearInterval(timeout)
  }, [forecastState])

  const themeContext = useContext(ThemeContext)

  if(!forecastState || forecastState.length === 0) {
    return <WindowWidgetWrapper />
  }

  // if (!asWindow) {
    return (
      <WindowWidgetWrapper background={themeContext.value === 'dark' ? '#00e0ff61' : '#006fff'}>
        <CustomHeightRow>
          <Content>
            {forecastState.slice(0, 3).map((weather: ParsedMetOfficeWeather, idx: number) => (
              <ForecastItem key={idx}>
                <IconWrapper>
                  {WeatherService.getIcon(weather.icon)}
                </IconWrapper>
                <div className="text-wrapper">
                  <span className="temp">
                    {Math.floor(parseInt(weather.temperature))}
                    &#176;
                  </span>
                  <span>{WeatherService.getTime(weather.dateTime)}</span>
                </div>
              </ForecastItem>
            ))}
          </Content>
        </CustomHeightRow>
        {/* </WeatherListBackground> */}
      </WindowWidgetWrapper>
    )
  // }

  // return (
  //   <WeatherBackground darkMode={themeContext.value === 'dark'}>
  //     <div style={{ display: "flex", justifyContent: "center" }}>
  //       <div>
  //         {/* <div
  //           style={{
  //             display: "flex",
  //             alignItems: "end",
  //             flexDirection: "row",
  //             color: "white",
  //           }}
  //         >
  //           <div style={{ marginBottom: "-25px" }}>
  //             {WeatherService.getIcon(weatherState.weatherIcon, true)}
  //           </div>
  //           <TempText>{weatherState.weatherIcon}</TempText>
  //         </div>
  //         <TemperatureWrapper>
  //           <Temperature>{weatherState.temperature.current}&#176;</Temperature>
  //           <div>
  //             <TemperatureVariation>
  //               {weatherState.temperature.max}&#176;
  //             </TemperatureVariation>
  //             <TemperatureVariation>
  //               {weatherState.temperature.min}&#176;
  //             </TemperatureVariation>
  //           </div>
  //         </TemperatureWrapper> */}
  //       </div>
  //     </div>
  //     <SpaceDivider />
  //     <BaseRow>
  //       {forecastState.forecast &&
  //         forecastState.forecast.slice(0, 3).map((weather: ParsedMetOfficeWeather, index) => (
  //           <ForecastItem key={index}>
  //             <IconWrapper>
  //               {WeatherService.getIcon(weather.icon)}
  //             </IconWrapper>
  //             <div className="text-wrapper">
  //               <span className="temp">
  //                 {Math.floor(parseInt(weather.temperature))}&#176;
  //               </span>
  //               <span>{WeatherService.getTime(weather.dateTime)}</span>
  //             </div>
  //           </ForecastItem>
  //         ))}
  //     </BaseRow>
  //     <SpaceDivider />
  //     <BaseRow>
  //       {forecastState.forecast &&
  //         forecastState.forecast[1].map((weather: WeatherType) => (
  //           <ForecastItem key={weather.dt}>
  //             <IconWrapper>
  //               {WeatherService.getIcon(weather.weather[0].main)}
  //             </IconWrapper>
  //             <div className="text-wrapper">
  //               <span className="temp">
  //                 {Math.floor(weather.main.temp)}&#176;
  //               </span>
  //               <span>{WeatherService.getTime(weather.dt_txt)}</span>
  //             </div>
  //           </ForecastItem>
  //         ))}
  //     </BaseRow>
  //   </WeatherBackground>
  // )
}

export default memo(Weather)
