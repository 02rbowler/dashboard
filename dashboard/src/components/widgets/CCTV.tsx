import React, { useEffect, useRef } from "react"
import * as Sentry from "@sentry/browser"

import styled from "styled-components"
import { loadPlayer } from "rtsp-relay/browser"

const VideoContainer = styled.div`
  margin-left: -40px;
  width: calc(100% + 80px);
  height: 100%;
  display: flex;
  flex-direction: column;
  justify-content: center;
`

interface propTypes {
  permanentWindow?: boolean
}

function CCTV(props: propTypes) {
  // const config = useSelector((state: RootState) => state.keyStorageReducer.value);
  // const canvas = useRef<HTMLCanvasElement>(null)
  // const videoJsOptions = {
  //     autoplay: true,
  //     controls: false,
  //     sources: [{
  //         src: '',
  //         type: 'video/mp4'
  //     }]
  // }

  // useEffect(() => {
  //   if (!canvas.current) throw new Error("Ref is null")

  //   loadPlayer({
  //     //   url: 'ws://manorcroft.webredirect.org/node/cctv/api/stream/2',
  //     // url: 'ws://192.168.5.66:3002/api/streamFrontDoor',
  //     url: "ws://192.168.5.66:3002/api/stream",
  //     canvas: canvas.current,
  //     audio: false,
  //     onDisconnect: () => {
  //       Sentry.captureException("CCTV disconnected")
  //     },
  //     disconnectThreshold: 5000,
  //   })
  // }, [])

  return (
    <VideoContainer>
      {/* <canvas
        ref={canvas}
        style={{ width: "100%", transform: "rotate(0deg)" }}
      /> */}
      <iframe src="https://manorcroftapi.com/cctvapi/frontdoor"></iframe>
    </VideoContainer>
  )
}

export default CCTV
