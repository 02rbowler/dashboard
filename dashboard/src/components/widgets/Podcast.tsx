import React, { memo, useEffect, useState } from "react"
import WidgetWrapper from "../WidgetWrapper"
import { useDispatch, useSelector } from "react-redux"
import { GradientWidget } from "../Widget"
import WidgetRow from "../shared/WidgetRow"
import SpotifyService from "./../../services/spotify"
import { fetchLatestEpisodes, fetchPodcastList } from "../../reducers/podcast"
import styled from "styled-components"
import { RootState } from "../../reducers/store"

const StyledPoster = styled.span`
  img {
    width: 100%;
    margin-top: 7px;
  }
`

const Podcast = ({ window }: { window?: boolean }): JSX.Element | null => {
  const [tokens, setTokens] = useState<(string | null)[]>([null, null])
  const podcastShows = useSelector(
    (state: RootState) => state.podcastReducer.shows
  )
  const podcastEpisodes = useSelector(
    (state: RootState) => state.podcastReducer.episodes
  )
  const dispatch = useDispatch()

  useEffect(() => {
    setTokens(SpotifyService.getTokens())
  }, [])

  useEffect(() => {
    if (tokens[0]) {
      if (!podcastShows || podcastShows.length === 0) {
        // dispatch(fetchPodcastList(tokens[0] || ""));
      } else if (!podcastEpisodes || podcastEpisodes.length === 0) {
        // dispatch(fetchLatestEpisodes([podcastShows, tokens[0]]));
      }
    }
  }, [tokens, dispatch, podcastShows, podcastEpisodes])

  if (!tokens[0]) {
    return (
      <WidgetWrapper allowAnimate={true}>
        <WidgetRow>
          <span>Need to login to Spotify</span>
        </WidgetRow>
      </WidgetWrapper>
    )
  }

  if (podcastEpisodes.length === 0) {
    return null
  }

  const backgroundVals = {
    first: "#ac32e4e3",
    second: "#7918f2e3",
    last: "#4801ffe3",
  }

  return (
    <WidgetWrapper window={window} allowAnimate={true}>
      <GradientWidget text="white" background={backgroundVals}>
        <WidgetRow>
          {podcastEpisodes.map(podcast => (
            <StyledPoster key={podcast.id}>
              <img alt="episode poster" src={podcast.episodeImage} />
            </StyledPoster>
          ))}
        </WidgetRow>
      </GradientWidget>
    </WidgetWrapper>
  )
}

export default memo(Podcast)
