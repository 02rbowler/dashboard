import React, { useEffect, useState } from "react"
import { useDispatch, useSelector } from "react-redux"
import {
  fetchCricketMatches,
  fetchCricketScore,
} from "../../reducers/sportsScores"
import { RootState } from "../../reducers/store"
import WidgetRow from "../shared/WidgetRow"
import Widget from "../Widget"
import WidgetWrapper from "../WidgetWrapper"
import SportsScoresService from "./../../services/sportsScores"

interface displayDataType {
  matchId: number
  team1: string | null
  team2: string | null
  status: string | null
}

function SportsScores() {
  const [displayData, setDisplayData] = useState<displayDataType>({
    matchId: 0,
    team1: null,
    team2: null,
    status: null,
  })

  const dispatch = useDispatch()

  const sportsScores = useSelector(
    (state: RootState) => state.sportsScoresReducer.data
  )
  const sportsScoresStatus = useSelector(
    (state: RootState) => state.sportsScoresReducer.status
  )
  const config = useSelector(
    (state: RootState) => state.keyStorageReducer.value
  )
  const sportsScoresService = SportsScoresService()

  useEffect(() => {
    const parsedMatches = sportsScoresService.parseCricketData(sportsScores)
    if (parsedMatches) {
      setDisplayData({
        matchId: parsedMatches["unique_id"],
        team1: parsedMatches["team-1"],
        team2: parsedMatches["team-2"],
        status: "Match yet to start",
      })
      // if(!parsedMatches.matchStarted && displayData.matchId !== parsedMatches['unique_id']) {
      //     setDisplayData({
      //         matchId: parsedMatches['unique_id'],
      //         team1: parsedMatches['team-1'],
      //         team2: parsedMatches['team-2'],
      //         status: "Match yet to start"
      //     });
      // } else if(parsedMatches.matchStarted) {
      //     console.log("MATCH STARTED");
      //     dispatch(fetchCricketScore(parsedMatches['unique_id']));
      // }
    }
  }, [sportsScores])

  // useFetchData(sportsScoresStatus, 172800000, () => fetchCricketMatches(config.cricApiKey));

  if (displayData.matchId === 0) {
    return <WidgetWrapper />
  }

  return (
    <WidgetWrapper allowAnimate={true}>
      <Widget header="Cricket Scores">
        <WidgetRow>
          <span>{`${displayData.team1} vs ${displayData.team2}`}</span>
        </WidgetRow>
        <WidgetRow>
          <span>{displayData.status}</span>
        </WidgetRow>
      </Widget>
    </WidgetWrapper>
  )
}

export default React.memo(SportsScores)
