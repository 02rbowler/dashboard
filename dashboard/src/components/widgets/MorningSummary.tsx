import React, { FC, Fragment, memo } from "react"
import { BaseRow, ColouredRow, ContentRow, Separator } from "../Rows"
import styled from "styled-components"
import { useGetAllNewsQuery } from "../../reducers/news"
import { useGetTubeDataQuery } from "../../reducers/tube"
import { useSportsCentreQuery } from "../../api"
import TubeService from "../../services/tube"
import { fontSize } from "../../styles/themes"

const OverflowedDiv = styled.div`
  white-space: nowrap;
  text-overflow: ellipsis;
  width: 100%;
  overflow: hidden;
`

const TubeMark = styled.div<{background: string}>`
  background-color: ${props => props.background};
  margin-left: 4px;
  font-size: ${fontSize.s};
  color: white;
  box-sizing: border-box;
  width: calc((100% / 3) - 4px);
  white-space: nowrap;
  text-overflow: ellipsis;
  overflow: hidden;
  padding: 5px 10px;
  text-transform: capitalize;
  margin-bottom: 4px;
  border-radius: 5px;
`

const MorningSummary: FC = () => {
  const {
    data: sportsData
  } = useSportsCentreQuery()

  const {
    data: lineStatus,
  } = useGetTubeDataQuery(undefined)
  
  const {
    data: newsItems,
  } = useGetAllNewsQuery(undefined, { refetchOnMountOrArgChange: 1200 })

  const backgroundVals = {
    first: "#F020209c",
    second: "#F020209c",
    last: "#7303039c",
  }

  // console.log(lineStatus)
  const parsedData = lineStatus && TubeService.getDataToParse(lineStatus.tubeData)

  return (
    <div>
      {parsedData && <BaseRow darkMode={true}>
        <span>Tube Status</span>
      </BaseRow>}

      <div style={{
        display: 'flex',
        flex: 1,
        flexWrap: 'wrap',
        width: "calc(100% + 4px)",
        marginLeft: "-4px",
        marginBottom: "8px"
      }}>
        { parsedData && parsedData.map(tube => (<>
          <TubeMark background={TubeService.getBackgroundByLineType(tube.id)}>
            {tube.id}
          </TubeMark>
        </>))}
      </div>
      {newsItems && newsItems.map((newsItem, index) => (
        <Fragment key={index}>
          <ColouredRow background={backgroundVals} darkMode={true}>
            <OverflowedDiv>{newsItem.title}</OverflowedDiv>
          </ColouredRow>
          <ContentRow darkMode={true}>{newsItem.description}</ContentRow>
          <Separator />
        </Fragment>
      ))}
    </div>
  )
}

export default memo(MorningSummary)
