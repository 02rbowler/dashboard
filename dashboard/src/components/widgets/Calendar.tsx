import React, { useContext, useEffect } from "react"
import * as Sentry from "@sentry/browser"
import TimeService from "../../services/time"
import { PlainWidget } from "../Widget"
import { useDispatch, useSelector } from "react-redux"
import { addEvents, CalendarData } from "../../reducers/calendar"
import WidgetWrapper from "../WidgetWrapper"
import { ReactComponent as BirthdayCake } from "./../../assets/calendarIcons/birthdayCake.svg"
import { GoogleLogin } from '@react-oauth/google';

import styled from "styled-components"
import WidgetRow from "../shared/WidgetRow"
import { RootState } from "../../reducers/store"
import { CalendarEvent, Config } from "../../types"
import { memo } from "react"
import { CalendarServiceType } from "../../services/calendar"
import { ThemeContext } from "../../App"
import { fontSize } from "../../styles/themes"

const BirthdayIcon = styled.span<{ highlightMode: boolean }>`
  svg {
    width: 30px;
  }

  ${props =>
    props.highlightMode &&
    `svg {
		path {
			fill: white;
		}
	}`}
`

const DayGroup = styled.div`
  border-bottom: 1px solid #797979;
  padding-bottom: 7px;
  font-size: ${fontSize.m};
  color: black;
`

const TodayStyle = styled.div<{styleDiv: boolean, darkMode: boolean}>`
  padding: 0px 20px 0px 40px;
  padding-top: 7px;
  ${props => props.styleDiv ? `
    background-color: #4d4d4d;
    div {
      color: white;
    }
  ` : ''}
  ${props => props.darkMode ? 'div { color: white !important; }' : ''}
`

const RowContent = styled.div`
  display:flex;
  width: 100%;

  span:first-of-type { flex: 1; margin-left: 0; }
`

const Calendar = ({
  calendarService,
  emailId,
  type,
  config,
}: {
  calendarService: CalendarServiceType
  emailId: string | string[]
  type?: string
  config: Config
}): JSX.Element => {
  const dispatch = useDispatch()
  const calendarData = useSelector(
    (state: RootState) => state.calendarReducer.data
  )
  const calendarStatus = useSelector(
    (state: RootState) => state.calendarReducer.status
  )
  const timeService = TimeService()
  let eventsToDisplay = calendarData

  const themeContext = useContext(ThemeContext)

  useEffect(() => {
    let interval: NodeJS.Timeout

    if (calendarStatus === "idle") {
      fetchEvents()
    }

    interval = setInterval(() => {
      fetchEvents()
    }, 15000)

    return function cleanup() {
      clearInterval(interval)
    }
  }, [])

  const fetchEvents = async () => {
    calendarService.getEvents(
      (response: CalendarData) => {
        if (
          !response ||
          !response.isSignedIn ||
          !response.events ||
          response.events.length === 0
        ) {
          Sentry.captureException("fetchEvents: " + JSON.stringify(response))
        }
        dispatch(addEvents(response))
      },
      emailId,
      config
    )
  }

  calendarService.resetLastDate()

  // NEED TO FIND SOLUTION FOR SCENARIO:
  //  || (calendarData.isSignedIn && calendarData.events?.length === 0)

  if (!calendarData || !calendarData.isSignedIn) {
    Sentry.captureException(
      "Showing sign in to google: " + JSON.stringify(calendarData)
    )
    return (
      <div>
        <GoogleLogin
          onSuccess={credentialResponse => {
            fetchEvents();
          }}
          onError={() => {
            Sentry.captureException(
              "Failed to login to google"
            )
          }}
          auto_select
        />
      </div>
    )
  }

  if (type) {
    eventsToDisplay = calendarService.parseEvents(calendarData, type)
  }

  if (!eventsToDisplay?.events || eventsToDisplay.events.length === 0) {
    return <div>No events loaded from calendar</div>
  }

  const backgroundVals = {
    first: "#e0e0e0de",
    second: "#ffffffd1",
    last: "#ffffffe6",
    titleColumn: "#ffffffd1",
  }

  return (
    <div style={{
      width: 'calc(100% + 60px)',
      marginLeft: '-40px',
      marginRight: '-20px',
      maxHeight: '90vh',
      overflow: 'hidden'
    }}>
      <WidgetWrapper>
        {calendarService
          .parseEventsByDate(eventsToDisplay.events, timeService)
          .map((eventsByDay: CalendarEvent[], i: number) => {
            const displayDate = calendarService.displayDate(
              eventsByDay[0].start,
              timeService,
              true
            )

            return (
              <TodayStyle key={`eventSection` + i} darkMode={themeContext.value === 'dark'} styleDiv={displayDate === "Today"}>
                <DayGroup>
                  {displayDate !== 'Today' && <b>{displayDate}</b>}
                  {eventsByDay.map(event => (
                    <WidgetRow
                      key={event.id}
                      {...(event.who && {
                        rowColour: calendarService.getWhoClass(event.who),
                      })}
                    >
                      <RowContent>
                        <span>{event.summary}</span>
                        {event.who === "birthday" ? (
                          <BirthdayIcon highlightMode={displayDate === "Today"}>
                            <BirthdayCake />
                          </BirthdayIcon>
                        ) : (
                          calendarService.getTime(event.start)
                        )}
                      </RowContent>
                    </WidgetRow>
                  ))}
                  {/* <PlainWidget background={backgroundVals} header={displayDate}>
                    {eventsByDay.map(event => (
                      <WidgetRow
                        key={event.id}
                        {...(event.who && {
                          rowColour: calendarService.getWhoClass(event.who),
                        })}
                      >
                        <span>{event.summary}</span>
                        {event.who === "birthday" ? (
                          <BirthdayIcon highlightMode={displayDate === "Today"}>
                            <BirthdayCake />
                          </BirthdayIcon>
                        ) : (
                          calendarService.getTime(event.start)
                        )}
                      </WidgetRow>
                    ))}
                  </PlainWidget> */}
                </DayGroup>
              </TodayStyle>
            )
          })}
      </WidgetWrapper>
    </div>
  )
}

export default memo(Calendar)
