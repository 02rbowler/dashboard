import React, { useContext, useEffect, useState } from "react"

import { WindowWidgetWrapper } from "../WidgetWrapper"

import styled from "styled-components"
import { RootState } from "../../reducers/store"
import { memo } from "react"
import { ThemeContext } from "../../App"
import { fontSize } from "../../styles/themes"
import { useSelector } from "react-redux"
import { setUser } from "../../reducers/userConfig"
import { Account } from "../../types"
import { useGetExerciseQuery } from "../../reducers/exercise"

const StravaBackground = styled.div<{darkMode: boolean}>`
  background: ${props => props.darkMode ? 
    `linear-gradient(96.95deg,#fc52005e 0%,#fc5200a1 50%,#ff692161 100%);` :
    `linear-gradient(96.95deg,#ff8a52 0%,#fc5201 50%,#ff7b3c 100%);`
  }
  height: 100%;
  margin: -30px -40px 14px;
  padding: 30px 20px 20px;
  border-top: 1px solid white;
  border-bottom: 1px solid white;
  margin: 0;
  box-sizing: border-box;
  height: fit-content;
`

const Bar = styled.div`
  position: relative;
  width: calc(100% - 4px);
  overflow: hidden;
  background-color: #752500;
  border-radius: 10px;
  border: 2px solid #752500;
`

const ProgressAmount = styled.div<{percentageWidth: number}>`
  width: ${props => props.percentageWidth}%;
  background-color: #fc5200;
  height: 30px;
`

const TargetMarker = styled.div<{percentageWidth: number}>`
  position: absolute;
  top: 0;
  left: ${props => props.percentageWidth}%;
  background-color: white;
  width: 6px;
  height: 30px;
`

const TargetText = styled.div`
  font-size: ${fontSize.m};
  text-align: center;
  margin-top: 5px;
  color: white;
`

const Strava = ({
  asWindow,
}: {
  asWindow: boolean
}): JSX.Element | null => {
  const targetDistance = 500;
  const now = new Date();
  const start = new Date(now.getFullYear(), 0, 0);
  const diff = now as any - (start as any);
  const oneDay = 1000 * 60 * 60 * 24;
  const day = Math.floor(diff / oneDay);
  const percentageThroughYear = day / 365;

  const startDate = new Date('2024-02-28')
  const daysDuration = 57;
  const exerciseDiff = now as any - (startDate as any);
  const percentageThroughTime = Math.floor(exerciseDiff / oneDay) / daysDuration;

  const expectedDistanceRun = targetDistance * percentageThroughYear;
  const distanceRun = useSelector(
    (state: RootState) => state.stravaReducer.data
  )
  const userAccount = useSelector(
    (state: RootState) => state.userConfigReducer.data
  )
  const userId = userAccount ? userAccount.userId : -1

  const {
    data: exerciseData,
    error,
    isLoading,
    isFetching,
    refetch,
  } = useGetExerciseQuery(userId, { refetchOnMountOrArgChange: 3600 })
  
  const exercisePercentageData = exerciseData ? exerciseData[0].data.data : null
  const themeContext = useContext(ThemeContext)

  if (
    !distanceRun
  ) {
    return <WindowWidgetWrapper />
  }

  if (!asWindow && distanceRun !== 0) {
    return (
      <WindowWidgetWrapper background={themeContext.value === 'dark' ? '#b33a00' : '#fc7501'}>
        <Bar>
          <ProgressAmount percentageWidth={distanceRun / targetDistance * 100} />
          <TargetMarker percentageWidth={percentageThroughYear * 100} />
        </Bar>
        <TargetText>
          {targetDistance - distanceRun < 50 ?
            targetDistance > distanceRun ?
              <span><b>{Math.floor(targetDistance - distanceRun)}km</b> left</span>
            : <span><b>{Math.floor(distanceRun)}km</b> run</span>
          : distanceRun < expectedDistanceRun ?
            <span><b>{Math.floor(expectedDistanceRun - distanceRun)}km</b> behind</span> :
            <span><b>{Math.floor(distanceRun - expectedDistanceRun)}km</b> ahead</span>
          }
        </TargetText>
        {exerciseData && exercisePercentageData && exercisePercentageData.length > 0 && exercisePercentageData[exercisePercentageData.length - 1] !== 0 && (
          <>
            <br />
            <Bar>
              <ProgressAmount percentageWidth={exercisePercentageData[exercisePercentageData.length - 1]} />
              <TargetMarker percentageWidth={percentageThroughTime * 100} />
            </Bar>
            <TargetText>
              <span>Progress to goal: <b>{exercisePercentageData[exercisePercentageData.length - 1]}%</b></span>
            </TargetText>
          </>
        )}
        {/* {exerciseData && exercisePercentageData && exercisePercentageData.length > 0 && exercisePercentageData[exercisePercentageData.length - 1] !== 0 && (
          <>
            <br />
            <Bar>
              <ProgressAmount percentageWidth={exercisePercentageData[exercisePercentageData.length - 1]} />
            </Bar>
            <TargetText>
              <span>Progress to goal: <b>{exercisePercentageData[exercisePercentageData.length - 1]}%</b></span>
            </TargetText>
          </>
        )} */}
      </WindowWidgetWrapper>
    )
  }

  return null
}

export default memo(Strava)
