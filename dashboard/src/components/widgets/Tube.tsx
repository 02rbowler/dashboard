import React, { memo, useContext, useEffect, useState } from "react"
import styled from "styled-components"
import { ColouredBlock } from "../Rows"
import TubeService from "../../services/tube"
import { ThemeContext } from "../../App"
import { TubeObj } from "../../types"
import { WidgetOutline } from "../Widget"
import { fontSize } from "../../styles/themes"

const TubeText = styled.span`
  text-transform: capitalize;
  flex: 1;
  font-weight: bold;
`

const Wrapper = styled(WidgetOutline)<{numberOfStatuses: number}>`
  width: calc(100% / ${props => props.numberOfStatuses === 1 ? 6 : 3}); // Because it's at 200%
`

const TopRow = styled.div`
  display: flex;
  flex-wrap: wrap;
`

const Destination = styled.span`
  flex: 1;
  white-space: nowrap;
  overflow: hidden;
  text-overflow: ellipsis;
`

const Station = styled.div`
  font-size: ${fontSize.s};
  font-weight: bold;
  margin: 5px 0;
  border-radius: 5px;
  width: fit-content;
`

const TrainRow = styled.div<{darkMode: boolean}>`
  font-size: ${fontSize.m};
  display: flex;
  margin-bottom: 4px;
  ${props => props.darkMode ? 'color: white;' : ''}
`

const Tube = ({liveStation, lineStatus}: {lineStatus?: TubeObj[], liveStation?: boolean}) => {
  const themeContext = useContext(ThemeContext)
  const [stationData, setStationData] = useState([])

  useEffect(() => {
    if(liveStation) {
      fetch("https://api.tfl.gov.uk/Line/metropolitan/Arrivals/940GZZLUHOH")
        .then(res => res.json())
        .then(res => {
          const transformed = res.map((item: any) => {
            if(item.platformName.toLowerCase().indexOf('southbound') !== -1 && item.timeToStation > 180) {
              const time = Math.round(item.timeToStation / 60)
              const timeSuffix = time === 1 ? 'min' : 'mins'
              return {
                expectedArrival: item.expectedArrival,
                platformName: item.platformName,
                timeToStation: time,
                timeSuffix,
                towards: item.towards,
                destinationName: item.destinationName,
                currentLocation: item.currentLocation
              }
            }

            return null;
          }).filter((item: any) => item).sort((item1: any, item2: any) => item1.timeToStation < item2.timeToStation ? -1 : 1)
          const reduced = transformed.map((item: any, idx: number) => {
            if(idx === 0) {
              return item
            } else {
              if(item.timeToStation === transformed[idx - 1].timeToStation) {
                return null
              } else {
                return item
              }
            }
          }).filter((item: any) => item).slice(0,4)
          setStationData(reduced)
        })
    }
  }, [liveStation])

  if ((!lineStatus || lineStatus.length === 0) && !liveStation) {
    return null
  }

  return (
    <Wrapper numberOfStatuses={lineStatus?.length || 1} darkMode={themeContext.value === 'dark'}>
      {liveStation && stationData.length > 0 && (
        <>
          <Station>Southbound</Station>
          {
            stationData.map((dataItem: any, idx) => <TrainRow key={idx} darkMode={themeContext.value === 'dark'}>
              <Destination>{dataItem.towards}</Destination>
              <span><b>{dataItem.timeToStation} {dataItem.timeSuffix}</b></span>
            </TrainRow>)
          }
        </>
      )}
      {lineStatus && lineStatus.length > 0 && (
        <TopRow>
          {lineStatus.map(tube => <ColouredBlock key={tube.id} background={TubeService.getBackgroundByLineType(tube.id)} whiteText={TubeService.isWhiteText(tube.id)} style={{marginRight: "10px", marginBottom: "10px"}}>
            <TubeText>{tube.name}</TubeText>
            {lineStatus.length <= 3 && <span>: {tube.lineStatuses[0].statusSeverityDescription}</span>}
          </ColouredBlock>)}
        </TopRow>
      )}
    </Wrapper>
  )
}

export default memo(Tube)
