import React, { FC, memo, useContext } from "react"
import styled from "styled-components"
import { ThemeContext } from "../../App"
import { WidgetOutline } from "../Widget"
import { fontSize } from "../../styles/themes"

const Title = styled.div<{darkMode: boolean}>`
  font-size: ${fontSize.m};
  ${props => props.darkMode ? 'color: white;' : ''}
  margin-bottom: 10px;
  font-weight: bold;
  color: white;
`

const Description = styled.div<{darkMode: boolean}>`
  font-size: ${fontSize.s};
  ${props => props.darkMode ? 'color: white;' : ''}
  color: white;
`

const News: FC<{title: string, description: string}> = ({title, description}) => {
  const themeContext = useContext(ThemeContext)

  return (
    <WidgetOutline background={"rgba(226, 8, 8, 0.7)"}>
      <Title darkMode={themeContext.value === 'dark'}>{title}</Title>
      <Description darkMode={themeContext.value === 'dark'}>{description}</Description>
    </WidgetOutline>
  )
}

export default memo(News)
