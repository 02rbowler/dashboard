import React, { useContext } from "react"
import { useSelector } from "react-redux"
import WidgetWrapper from "../WidgetWrapper"
import TVService from "../../services/tv"
import { RootState } from "../../reducers/store"
import { BaseRow, Separator } from "../Rows"
import { memo } from "react"
import styled from "styled-components"
import { TVScheduleData } from "../../reducers/tv"
import { fontSize } from "../../styles/themes"
import { useTvQuery } from "../../api"
import { WidgetOutline } from "../Widget"
import { ThemeContext } from "../../App"

const ShowTitle = styled.span`
  flex: 1;
  white-space: nowrap;
  overflow: hidden;
  text-overflow: ellipsis;
`

const ColouredBackground = styled.div<{
  background: { first: string; second: string; last: string }
}>`
  background: ${props =>
    `linear-gradient(101.14deg, 
        ${props.background.first} 0%, 
        ${props.background.second} 38.54%, 
        ${props.background.second} 65.1%, 
        ${props.background.last} 100%);
    `}
  margin: 0 -40px;
  padding: 10px 40px;
  border-bottom: 2px solid white;

  :first-of-type {
    margin-top: -30px;
  }
`

const ChannelName = styled.div<{colour?: string, background: string}>`
  font-size: ${fontSize.s};
  font-weight: bold;
  color: ${props => props.colour || 'white'};
  margin: 5px 0;
  ${props => `background: ${props.background};`}
  border-radius: 5px;
  padding: 5px 10px;
  width: fit-content;
`

const ShowRow = styled.div<{darkMode: boolean}>`
  font-size: ${fontSize.m};
  display: flex;
  margin-bottom: 4px;
  ${props => props.darkMode ? 'color: white;' : ''}
`

export type TVProps = {
  displayName: string;
  background: string;
  shows: any[]
}

const TV = ({displayName, background, shows}: TVProps) => {
  let lastShow = ""
  let lastTime = "0"

  const themeContext = useContext(ThemeContext);

  const checkRepeatedShow = (showName: string, startTime: string): boolean => {
    if (lastShow === showName || lastTime === startTime) {
      return true
    }

    lastShow = showName
    lastTime = startTime
    return false
  }

  return (
    <WidgetOutline darkMode={themeContext.value === 'dark'}>
      <ChannelName background={background} colour={displayName === "BBC Two" ? "black" : undefined}>{displayName}</ChannelName>
      {TVService.reduceArray(shows).map(
        show =>
          !checkRepeatedShow(show.title, show.startTime) && (
            <ShowRow key={show.title + show.startTime} darkMode={themeContext.value === 'dark'}>
              <ShowTitle>{show.title}</ShowTitle>
              <span><b>{show.startTime}</b></span>
            </ShowRow>
          )
      )}
    </WidgetOutline>
  )
}

export default memo(TV)
