import React from "react"
import styled from "styled-components"
import GeneralBackground from "../../../assets/sportsImages/cricket-general.jpg"
import TestBackground from "../../../assets/sportsImages/cricket-test.jpg"
import WhiteBallBackground from "../../../assets/sportsImages/cricket-white.jpg"
import HundredBackground from "../../../assets/sportsImages/hundred.jpg"
import TheHundredLogo from "../../../assets/sportsComps/the_hundred.svg"
import {
  getCricketHundredTeamData,
  getCricketTeamData,
  getTestMatchDay,
  parseChannel,
} from "../../../services/sportsCentre"
import { fontSize } from "../../../styles/themes"
import { SportsFixture } from "../../../types"

const Background = styled.div<{ img: string }>`
  background-image: url(${props => props.img});
  background-position: center;
  background-repeat: no-repeat;
  background-size: cover;
  margin: 0 -40px;
  padding: 10px 40px;
  border-bottom: 2px solid white;
  color: #fc6641;
  height: 185px;
  max-height: 185px;
  overflow: hidden;
`

const Content = styled.div`
  font-size: ${fontSize.m};
  width: 350px;
  margin-left: auto;

  div:first-of-type {
    margin: 20px 0;
    color: #fd2101;
  }

  div:last-of-type {
    margin-top: 5px;
  }
`

const RightAlignedContent = styled.div`
  font-size: ${fontSize.m};
  text-align: right;
  color: white;
  align-self: flex-end;
`

const Logos = styled.div`
  display: flex;
`

const LogoCircle = styled.div<{ visible: boolean }>`
  width: 150px;
  height: 150px;
  border-radius: 50%;
  display: flex;
  justify-content: center;
  ${props =>
    props.visible &&
    `
    background: #F4F4F4;
    box-shadow: inset 0px 0px 10px 4px rgba(0, 0, 0, 0.4);
  `}
  text-align: center;

  img {
    margin-top: 15px;
    height: calc(100% - 30px);
  }
`

const CircleOutline = styled.div<{ colour: string; visible: boolean }>`
  ${props => props.visible && `border: 2px solid ${props.colour};`}
  border-radius: 50%;
  padding: 6px;
`

const BackgroundLogo = styled.div`
  position: absolute;
  z-index: -1;
  top: -40px;
  height: calc(100% + 80px);
  left: -80px;
  opacity: 0.5;
  display: flex;

  img {
    height: 100%;
  }
`

const StrapContents = styled.div``

const LogoStrap = styled.div<{ colour: string; border: string }>`
  background: ${props => props.colour};
  ${props =>
    `border: ${props.border === "white" ? 1 : 2}px solid ${props.border};`}
  transform: rotate(-18deg);
  padding: 5px;
  height: 300px;
  margin-top: -60px;
  padding-top: 65px;
  box-sizing: border-box;
  overflow: hidden;

  :first-of-type {
    margin-right: 15px;
    margin-left: -10px;
  }

  ${StrapContents} {
    transform: rotate(18deg);
  }
`

const TeamName = styled.div`
  font-size: 50px;
  font-weight: bold;
  align-self: center;
  text-transform: uppercase;
`

const ChannelLogo = ({channel, whiteVersion}: {channel: string, whiteVersion?: boolean}) => {
  const isPaddedLogo = true;

  return <img
    height={isPaddedLogo ? "30px" : "40px"}
    alt="channel logo"
    src={parseChannel(channel, whiteVersion)}
  />
}

type FixtureData = {
  data: SportsFixture
}

export const NoLogoCricket = ({
  data,
}: FixtureData): JSX.Element => {
  return (
    <Background img={GeneralBackground}>
      <Content>
        <div>
          <b>{data.fixture.trim()}</b>
        </div>
        <div>
          {data.competition.trim()} | {data.startTime}
        </div>
        <div>
          <ChannelLogo channel={data.channel} />
        </div>
      </Content>
    </Background>
  )
}

const LogoComponent = ({
  img: imgUrl,
  whiteBallImg,
  teamName,
  primary,
  secondary,
  mode,
}: {
  img?: string
  whiteBallImg?: string
  teamName?: string
  primary: string
  secondary: string
  mode: "red" | "white"
}): JSX.Element => (
  <LogoStrap colour={primary} border={mode === "red" ? "white" : secondary}>
    <StrapContents>
      {mode === "white" && (
        <BackgroundLogo>
          {imgUrl ? (
            <img src={imgUrl} alt="logo" />
          ) : (
            <TeamName
              style={{
                minWidth: "250px",
                textAlign: "center",
                marginBottom: "80px",
                fontSize: "80px",
              }}
            >
              {teamName}
            </TeamName>
          )}
        </BackgroundLogo>
      )}
      <CircleOutline colour={secondary} visible={mode === "red"}>
        <LogoCircle visible={mode === "red"}>
          {imgUrl ? (
            <img src={whiteBallImg || imgUrl} alt="team logo" />
          ) : (
            <TeamName>{teamName}</TeamName>
          )}
        </LogoCircle>
      </CircleOutline>
    </StrapContents>
  </LogoStrap>
)

export const TestMatch = ({
  data,
}: FixtureData): JSX.Element => {
  const teamData = getCricketTeamData(data.fixture)

  return (
    <Background img={TestBackground}>
      <div
        style={{
          display: "flex",
          justifyContent: "space-between",
          height: "100%",
        }}
      >
        <Logos>
          <LogoComponent
            img={teamData.home.img}
            primary={teamData.home.primaryColour}
            secondary={teamData.home.secondaryColour as string}
            mode="red"
          />
          <LogoComponent
            img={teamData.away.img}
            primary={teamData.away.primaryColour}
            secondary={teamData.away.secondaryColour as string}
            mode="red"
          />
        </Logos>
        <RightAlignedContent>
          <div>
            <b>{getTestMatchDay(data.competition)}</b>
          </div>
          <div style={{ marginBottom: "30px" }}>{data.startTime}</div>
          <div>
            <ChannelLogo channel={data.channel} whiteVersion={true} />
          </div>
        </RightAlignedContent>
      </div>
    </Background>
  )
}

export const WhiteBallLogoMatch = ({
  data,
}: FixtureData): JSX.Element => {
  const teamData = getCricketTeamData(data.fixture)
  const competition = data.competition.trim()
  const renderedCompetition = competition === "Twenty20 International" ? "T20I" :
    competition === "One Day International" ? "ODI" : competition

  return (
    <Background img={WhiteBallBackground}>
      <div
        style={{
          display: "flex",
          justifyContent: "space-between",
          height: "100%",
        }}
      >
        <Logos>
          <LogoComponent
            img={teamData.home.img}
            whiteBallImg={teamData.home.whiteBallImg}
            teamName={teamData.home.name.trim().slice(0, 3)}
            primary={teamData.home.whiteBallColour as string}
            secondary={teamData.home.secondaryColour as string}
            mode="white"
          />
          <LogoComponent
            img={teamData.away.img}
            whiteBallImg={teamData.away.whiteBallImg}
            teamName={teamData.away.name.trim().slice(0, 3)}
            primary={teamData.away.whiteBallColour as string}
            secondary={teamData.away.secondaryColour as string}
            mode="white"
          />
        </Logos>
        <RightAlignedContent>
          <div>
            <b>{renderedCompetition}</b>
          </div>
          <div style={{ marginBottom: "30px" }}>{data.startTime}</div>
          <div>
            <ChannelLogo channel={data.channel} whiteVersion={true} />
          </div>
        </RightAlignedContent>
      </div>
    </Background>
  )
}

export const TheHundred = ({
  data,
}: FixtureData): JSX.Element => {
  const teamData = getCricketHundredTeamData(data.fixture);

  return (
    <Background img={HundredBackground}>
      <div
        style={{
          display: "flex",
          justifyContent: "space-between",
          height: 207,
          marginTop: "-10px",
          marginBottom: "-10px",
        }}
      >
        {teamData.home && teamData.away && <Logos>
          <img src={teamData.home} alt="home team logo" />
          <img src={teamData.away} alt="away team logo" />
        </Logos>}
        <RightAlignedContent>
          <div>
            <img src={TheHundredLogo} alt="the hundred logo" width={200} />
          </div>
          <div style={{ marginBottom: "30px" }}>{data.startTime}</div>
          <div style={{marginBottom: 10}}>
            <ChannelLogo channel={data.channel} />
          </div>
        </RightAlignedContent>
      </div>
    </Background>
  )
}
