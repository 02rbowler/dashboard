import React, { memo, FC, useContext } from "react"
import { useSelector } from "react-redux"
import styled from "styled-components"
import TourDeFrance from "../../../assets/sportsComps/le-tour-de-france.svg"
import SixNations from "../../../assets/sportsComps/six_nations.svg"
import RyderCup from "../../../assets/sportsComps/RyderCupLogo.svg"
import Lions from "../../../assets/sportsComps/lions.svg"
import Ashes from "../../../assets/sportsComps/ashes.svg"
import CyclingBackground from "../../../assets/sportsImages/cycling.jpg"
import { SportsCentreDashboard } from "@02rbowler/sports-centre-content"

import { RootState } from "../../../reducers/store"
import {
  getAllFixturesByType,
  hasEventFinished,
  isChannelPadded,
  parseChannel,
  parseCompetition,
  parseMainText,
} from "../../../services/sportsCentre"
import { BaseRow, Separator, WhiteRow } from "../../Rows"
import { SkySportsGuide } from "./SkySportsGuide"
import { F1Event, F1NextRace } from "./F1"
import {
  NoLogoCricket,
  TestMatch,
  TheHundred,
  WhiteBallLogoMatch,
} from "./Cricket"
import { Rugby } from "./Rugby"
import { GenericSport } from "./Generic"
import { useSportsCentreQuery } from "../../../api"
import { Football } from "./Football"
import { WidgetOutline } from "../../Widget"
import { ThemeContext } from "../../../App"
import { fontSize } from "../../../styles/themes"
import { SportsFixture } from "../../../types"

const Fixture = styled.div`
  font-size: ${fontSize.m};
  font-weight: bold;
  margin: 60px 20px 0;
`

const Competition = styled.div`
  font-size: ${fontSize.m};
  margin: 0 20px;
`

const LogoContainer = styled.span<{ isPadded: boolean }>`
  display: flex;
`

const FullHeight = styled.div`
  margin: -30px 0;
  min-height: calc(100% + 60px);
  display: flex;
  flex-direction: column;
`

const TimeBanner = styled.div`
  position: absolute;
  top: 10px;
  left: 10px;
  background-color: white;
  color: black;
  font-size: ${fontSize.s};
  font-weight: bold;
  padding: 5px 10px;
  border-radius: 5px;
  display: flex;
  align-items: center;

  span {
    border-right: 1px solid #909090;
    padding-right: 10px;
    margin-right: 10px;
  }
`

const Outline = styled(WidgetOutline)<{doubleWidth?: boolean}>`
  padding: 0;
  background: transparent;
  ${props => props.doubleWidth ? 'width: calc(100% / 3);' : ''}
`

const SportsCentre: FC<{
  startTime: string | null | undefined,
  liveLengthHours: number,
  competition: string,
  channel: string,
  fixture: string,
  eventText: string,
  f1Events?: SportsFixture[],
  invisible: false,
} | {invisible: true}> = (props) => {
  const channelLogo = (channel: string) => {
    const isPaddedLogo = isChannelPadded(channel)
    return (
      <LogoContainer isPadded={isPaddedLogo}>
        <img
          height={isPaddedLogo ? "30px" : "40px"}
          alt="channel logo"
          src={parseChannel(channel)}
        />
      </LogoContainer>
    )
  }

  let hasMergedF1Event = false
  let hasMergedRugbyEvent = false
  let hasMergedFootballEvent = false

  if(props.invisible) {
    return null;
  }

  if(props.f1Events) {
    if(props.f1Events.length > 0) {
      return <Outline>
        <SportsCentreDashboard {...props} />
      </Outline>
    } else {
      return null
    }
  }

  return (
    <Outline>
      <SportsCentreDashboard {...props} />
    </Outline>
    // <WidgetOutline colour="white" background="rgba(1, 78, 194, 0.7)">
    //   <TimeBanner>
    //     <span>{startTime}</span>
    //     <img
    //       height={"30px"}
    //       alt="channel logo"
    //       src={parseChannel(channel, false)}
    //     />
    //   </TimeBanner>
    //   <Fixture>{fixture}</Fixture>
    //   <Competition>{competition}</Competition>
    // </WidgetOutline>
  )

  // return (
  //   <FullHeight>
  //     {sportsData.SportsCentre.schedule.map((data, idx) => {
  //       if(hasEventFinished(data.startTime, data.liveLengthHours)) {
  //         return null
  //       }

  //       if (data.competition.indexOf("F1 season") === 0 || data.competition.indexOf("W Series") === 0) {
  //         if (hasMergedF1Event) {
  //           return null
  //         }
  //         hasMergedF1Event = true

  //         const events = getAllFixturesByType(sportsData.SportsCentre.schedule, "f1")

  //         return <F1Event events={events} key={idx} />
  //       }

  //       if (
  //         data.competition.indexOf("Rugby Union International") !== -1 ||
  //         data.competition.indexOf("Six Nations") !== -1
  //       ) {
  //         if (hasMergedRugbyEvent) {
  //           return null
  //         }
  //         hasMergedRugbyEvent = true

  //         const events = getAllFixturesByType(sportsData.SportsCentre.schedule, "rugby")

  //         return <Rugby key={idx} data={events} />
  //       }

  //       if (data.pageName === "football") {
  //         if (hasMergedFootballEvent) {
  //           return null
  //         }
  //         hasMergedFootballEvent = true

  //         const events = getAllFixturesByType(sportsData.SportsCentre.schedule, "football")

  //         return <Football key={idx} data={events} />
  //       }

  //       return data.competition.indexOf("Vitality Blast") !== -1 ?
  //         <NoLogoCricket key={idx} data={data} />
  //       : data.competition.indexOf("Test Match") !== -1 ?
  //         <TestMatch key={idx} data={data} />
  //       : (
  //         data.competition.indexOf("One Day International") !== -1 ||
  //         data.competition.indexOf("Twenty20 International") !== -1
  //       ) ?
  //         <WhiteBallLogoMatch key={idx} data={data} />
  //       : data.competition.indexOf("The Hundred") !== -1 ?
  //         <TheHundred data={data} key={idx} />
  //       : (data.fixture.indexOf("Tour de France") !== -1) ?
  //         (
  //           <GenericSport
  //             key={idx}
  //             logo={TourDeFrance}
  //             background={CyclingBackground}
  //             backgroundColour="#FFAD12"
  //             overlayText={
  //               <>
  //                 {data.startTime} |
  //                 <img
  //                   height={"30px"}
  //                   alt="channel logo"
  //                   src={parseChannel(data.channel, true)}
  //                 />
  //               </>
  //             }
  //           />
  //         )
  //       : (
  //         <div key={idx} style={{ marginTop: 10 }}>
  //           <WhiteRow>
  //             <span style={{ flex: 1 }}>
  //               {parseCompetition(data.competition, data.fixture)}
  //             </span>
  //             <span>{data.startTime}</span>
  //           </WhiteRow>
  //           <BaseRow>
  //             <span style={{ flex: 1 }}>{parseMainText(data)}</span>
  //             {channelLogo(data.channel)}
  //           </BaseRow>
  //           <Separator />
  //         </div>
  //       )
  //     })}

  //     {sportsData.SportsCentre.nextF1 &&
  //       sportsData.SportsCentre.nextF1.location &&
  //       !hasMergedF1Event && <F1NextRace data={sportsData.SportsCentre.nextF1} />}
  //     <SkySportsGuide />
  //   </FullHeight>
  // )
}

export default memo(SportsCentre)
