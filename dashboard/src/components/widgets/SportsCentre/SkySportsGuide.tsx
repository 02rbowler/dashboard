import React from "react"
import styled from "styled-components"
import { useGetSportsGuideQuery } from "../../../reducers/sportsCentre"
import { parseCurrentAndFutureSportsGuide } from "../../../services/sportsCentre"
import { fontSize } from "../../../styles/themes"
import { WidgetOutline } from "../../Widget"
import SkySports from "../../../assets/channels/SkySports.svg"

const ProgramItem = styled.div`
  font-size: ${fontSize.m};
  white-space: nowrap;
  overflow: hidden;
  text-overflow: ellipsis;
  margin-bottom: 4px;
`

type Programme = {
  minutesLength: string
  startTime: string
  title: string
}

export type SportsGuideApiReturn = {
  action: Programme[]
  arena: Programme[]
  cricket: Programme[]
  f1: Programme[]
  football: Programme[]
  golf: Programme[]
  mainEvent: Programme[]
  premierLeague: Programme[]
  racing: Programme[]
}

export const SkySportsGuide = () => {
  const { data, error, isLoading, isFetching, refetch } =
    useGetSportsGuideQuery(undefined)

  const sportsGuide: SportsGuideApiReturn | undefined =
    parseCurrentAndFutureSportsGuide(data)

  if (!sportsGuide) {
    return null
  }

  return (
    <WidgetOutline>
      <div style={{height: '39.5px', display: 'flex', alignItems: 'center', marginTop: 5, marginBottom: 5}}><img src={SkySports} height={30} /></div>
      {
        Object.keys(sportsGuide).map((channel, idx) => {
          if(['racing', 'football', 'premierLeague', 'golf', 'action'].includes(channel)) {
            return null
          }
          if(sportsGuide[channel as keyof typeof sportsGuide].length == 0) {
            return null
          }
          return <ProgramItem key={idx}>{sportsGuide[channel as keyof typeof sportsGuide][0].title}</ProgramItem>
        })
      }
    </WidgetOutline>
  )
}
