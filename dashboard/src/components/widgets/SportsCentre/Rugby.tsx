import React from "react"
import styled from "styled-components"
import TopRight from "../../../assets/sportsImages/rugby-top-right.png"
import BottomLeft from "../../../assets/sportsImages/rugby-bottom-left.png"
import { getRugbyTeamData, parseChannel } from "../../../services/sportsCentre"
import { fontSize } from "../../../styles/themes"

const Background = styled.div`
  background: linear-gradient(180deg, #3a3f5b 0%, #010101 100%);
  margin: 0 -40px;
  padding: 10px 40px;
  border-bottom: 2px solid white;
  min-height: 207px;
  box-sizing: border-box;
  position: relative;
  color: white;
  font-size: ${fontSize.m};
`

const StartTime = styled.div`
  color: #fc8f69;
  flex: 1;
  text-align: center;
  font-size: ${fontSize.s};
  margin: 0 2px;
`

const TopRightGraphic = styled.img`
  position: absolute;
  width: 254px;
  top: 0;
  right: 0;
`

const BottomLeftGraphic = styled.img`
  position: absolute;
  width: 254px;
  bottom: 0;
  left: 0;
`

const Row = styled.div`
  display: flex;
  margin-bottom: 8px;
  align-items: center;
`

const MatchRow = styled(Row)`
  background-color: #434362;
  margin-bottom: 0;
`

const LogoWrapper = styled.div<{ type: "home" | "away" }>`
  flex: none;
  width: 70px;
  height: 52px;
  overflow: hidden;
  margin: 0 5px;
  z-index: 1;

  ${props =>
    props.type === "home" ? `
      margin-right: 15px;
    ` : `
    text-align: right;
    margin-left: 15px;
  `}

  img {
    height: 100%;
  }
`

const TeamName = styled.div`
  display: flex;
  align-items: center;
  width: 300px;
  position: relative;

  .team-text {
    white-space: nowrap;
    text-overflow: ellipsis;
    overflow: hidden;
  }
`

const BorderLine = styled.div<{ colour: string; }>`
  background: ${props => props.colour};
  height: 3px;
  width: 100%;
`

const Competition = styled.div`
  color: #da3a9f;
  font-weight: bold;
  text-shadow: 0px 4px 4px rgba(0, 0, 0, 0.25), 0px 4px 4px rgba(0, 0, 0, 0.25);
  flex: 1;
`

export const Rugby = ({
  data,
}: {
  data: {
    channel: string
    competition: string
    eventText: string
    fixture: string
    liveLengthHours: number
    pageName: string
    startTime: string
  }[]
}): JSX.Element => {
  const isPaddedLogo = true
  const firstChannel = parseChannel(data[0].channel, true)
  const allMatchesSameChannel = data.every(
    item => parseChannel(item.channel, true) === firstChannel
  )

  return (
    <Background>
      <TopRightGraphic src={TopRight} />
      <BottomLeftGraphic src={BottomLeft} />
      <div style={{ position: "relative" }}>
        <Row style={{ marginBottom: "20px" }}>
          <Competition>{data[0].competition.replace("Rugby Union International", "")}</Competition>
          {allMatchesSameChannel && (
            <div style={{ display: "flex" }}>
              <img
                height={isPaddedLogo ? "30px" : "40px"}
                alt="channel logo"
                src={firstChannel}
              />
            </div>
          )}
        </Row>
        {data.map((item, idx) => {
          const teamData = getRugbyTeamData(item.fixture)

          return (
            <>
              <MatchRow key={idx}>
                <TeamName>
                  <LogoWrapper type="home">
                    <img src={teamData.home.img} alt="" />
                  </LogoWrapper>
                  <div className="team-text">
                    <i>{teamData.home.name}</i>
                  </div>
                </TeamName>
                <StartTime>{item.startTime}</StartTime>
                <TeamName>
                  <div
                    className="team-text"
                    style={{
                      flex: 1,
                      textAlign: "right",
                    }}
                  >
                    <i>{teamData.away.name}</i>
                  </div>
                  <LogoWrapper type="away">
                    <img src={teamData.away.img} alt="" />
                  </LogoWrapper>
                </TeamName>
                {!allMatchesSameChannel && (
                  <div style={{ display: "flex", marginLeft: "15px" }}>
                    <img
                      height={isPaddedLogo ? "30px" : "40px"}
                      width={"120px"}
                      alt="channel logo"
                      src={parseChannel(item.channel, true)}
                    />
                  </div>
                )}
              </MatchRow>
              <MatchRow style={{marginBottom: "8px"}}>
                <BorderLine
                  colour={teamData.home.primaryColour}
                />
                <BorderLine
                  colour={teamData.away.primaryColour}
                />
              </MatchRow>
            </>
          )
        })}
      </div>
    </Background>
  )
}
