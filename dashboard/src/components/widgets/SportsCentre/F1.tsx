import React from "react"
import styled from "styled-components"
import { Schedule } from "../../../api"
import F1Logo from "../../../assets/sportsComps/f1.svg"
import BottomImg from "../../../assets/sportsImages/f1.jpg"
import { parseChannel, parseMainText } from "../../../services/sportsCentre"
import { fontSize } from "../../../styles/themes"
import { WidgetOutline } from "../../Widget"

const Background = styled.div`
  background-color: black;
  color: #d4d4d4;
  margin: 0 -40px;
  padding: 10px 40px;
  border-bottom: 2px solid white;
  position: relative;
  min-height: 207px;
  box-sizing: border-box;
  display: flex;
  flex-direction: column;
  overflow: hidden;
`

const BottomImgWrapper = styled.div<{ rightOffset: boolean }>`
  margin: 0 -40px -10px;
  width: 100%;
  position: absolute;
  bottom: 6px;
  ${props => props.rightOffset && `right: -60px;`}
  z-index: 0;

  img {
    width: 100%;
  }
`

const SideGradient = styled.div`
  position: absolute;
  z-index: 0;
  top: 0;
  background: linear-gradient(
    91.8deg,
    #000000 10%,
    #1e1e1e 65%,
    rgba(0, 0, 0, 0) 100%
  );
  width: 280px;
  height: 100%;
  margin: 0 -40px;
`

const Content = styled.div`
  text-align: center;
  font-size: ${fontSize.m};
  margin-top: 20px;
`

const HorizontalContent = styled.div`
  background: linear-gradient(
    180deg,
    #000000 0%,
    #1e1e1e 60%,
    rgba(0, 0, 0, 0) 100%
  );
  margin: -10px -40px 130px;
  padding: 20px 40px 30px;
  position: inherit;
  display: flex;
  font-size: ${fontSize.m};
  justify-content: space-between;
  align-items: center;
`

const ChannelWrapper = styled.div`
  position: absolute;
  bottom: 20px;
`

export const F1NextRace = ({
  location,
  date
}: {
  location: string | null
    date: string | null
}): JSX.Element => (
  <WidgetOutline colour="white" background="rgba(0,0,0, 0.55)">
    <Content>
      <div>
        <img src={F1Logo} height="50px" alt="F1 logo" style={{marginTop: '10px'}} />
      </div>
      <div>
        <div>
          <b>{location?.toUpperCase()}</b>
        </div>
        <div>{date}</div>
      </div>
    </Content>
  </WidgetOutline>
)

const transformGrandPrix = (input: string) => {
  return input.replace("Grand Prix", "gp").toUpperCase()
}

export const F1Event = ({ events }: { events: Schedule[] }): JSX.Element => (
  <Background>
    <BottomImgWrapper rightOffset={false}>
      <img src={BottomImg} alt="background" />
    </BottomImgWrapper>
    <HorizontalContent>
      <div style={{ maxWidth: "225px" }}>
        <img src={F1Logo} height="30px" alt="F1 logo" />
        <div>
          <b>{transformGrandPrix(events[0].fixture)}</b>
        </div>
      </div>
      <div style={{ flex: 1, maxWidth: "320px", color: "white" }}>
        {events.map((event, i) => (
          <div
            style={{
              display: "flex",
              justifyContent: "space-between",
            }}
            key={i}
          >
            <i>{parseMainText(event)}</i>
            <div>{event.startTime}</div>
          </div>
        ))}
      </div>
    </HorizontalContent>
    <ChannelWrapper>
      <img
        height={"30px"}
        alt="channel logo"
        src={parseChannel(events[0].channel, true)}
      />
    </ChannelWrapper>
  </Background>
)
