import React from "react"
import styled from "styled-components"
import Shapes from "../../../assets/sportsImages/football-shapes.png"
import { parseChannel } from "../../../services/sportsCentre"
import { fontSize } from "../../../styles/themes"

const Background = styled.div`
  background: linear-gradient(86.17deg, #240000 0%, #881437 100%);;
  margin: 0 -40px;
  padding: 10px 40px;
  border-bottom: 2px solid white;
  min-height: 207px;
  box-sizing: border-box;
  position: relative;
  color: white;
  font-size: ${fontSize.m};
  overflow: hidden;
`

const StartTime = styled.div`
  color: #ff8964;
  font-weight: bold;
  flex: 1;
  text-align: center;
  font-size: ${fontSize.s};
  margin: 0 2px;
`

const ShapesImg = styled.img`
  position: absolute;
  z-index: 0;
  width: 100%;
  top: 0;
  left: 0;
`

const Row = styled.div`
  display: flex;
  margin-bottom: 8px;
  align-items: center;
`

const MatchRow = styled(Row)`
  background-color: rgba(106, 45, 47, 0.8);
  border-radius: 10px;
  padding: 3px 10px;
`

const TeamName = styled.div`
  width: 300px;
  position: relative;
  text-align: center;
`

const Competition = styled.div`
  color: white;
  font-weight: bold;
  text-shadow: 0px 4px 4px rgba(0, 0, 0, 0.25), 0px 4px 4px rgba(0, 0, 0, 0.25);
  flex: 1;
`

export const Football = ({
  data,
}: {
  data: {
    channel: string
    competition: string
    eventText: string
    fixture: string
    liveLengthHours: number
    pageName: string
    startTime: string
  }[]
}): JSX.Element => {
  const isPaddedLogo = true
  const firstChannel = parseChannel(data[0].channel, true)
  const allMatchesSameChannel = data.every(
    item => parseChannel(item.channel, true) === firstChannel
  )

  return (
    <Background>
      <ShapesImg src={Shapes} />
      <div style={{ position: "relative" }}>
        <Row style={{ marginBottom: "20px", paddingBottom: "10px", borderBottom: "2px solid #FE440B" }}>
          <Competition>{data[0].competition.split("Group")[0]}</Competition>
          {allMatchesSameChannel && (
            <div style={{ display: "flex" }}>
              <img
                height={isPaddedLogo ? "30px" : "40px"}
                alt="channel logo"
                src={firstChannel}
              />
            </div>
          )}
        </Row>
        {data.map((item, idx) => {
          const teamData = item.fixture.split(" v ")

          return (
            <MatchRow key={idx}>
              <TeamName>
                  {teamData[0].trim()}
              </TeamName>
              <StartTime>{item.startTime}</StartTime>
              <TeamName>
                  {teamData[1].trim()}
              </TeamName>
              {!allMatchesSameChannel && (
                <div style={{ display: "flex", marginLeft: "15px" }}>
                  <img
                    height={isPaddedLogo ? "30px" : "40px"}
                    width={"120px"}
                    alt="channel logo"
                    src={parseChannel(item.channel, true)}
                  />
                </div>
              )}
            </MatchRow>
          )
        })}
      </div>
    </Background>
  )
}
