import React, { ReactNode } from "react"
import styled from "styled-components"
import { fontSize } from "../../../styles/themes"

const Background = styled.div`
  background-color: black;
  min-height: 207px;
  box-sizing: border-box;
  margin: 0 -40px;
  position: relative;
  border-bottom: 2px solid white;
  overflow: hidden;

  img.background {
    height: 100%;
    position: absolute;
    top: 0;
    right: 0;
  }
`

const Overlay = styled.div<{ background: string }>`
  background-color: ${props => props.background};
  min-width: 260px;
  height: 100%;
  border-right: 5px solid #ffffff;
  position: absolute;
  box-shadow: 3px 0px 11px 2px #353535;
  padding-left: 40px;
  padding-right: 20px;

  img.logo {
    margin-top: 10px;
    height: 150px;
  }
`

const OverlayText = styled.div`
  font-size: ${fontSize.m};
  display: flex;
  align-items: center;

  img {
    margin-left: 10px;
  }
`

export const GenericSport = ({
  logo,
  background,
  backgroundColour,
  overlayText,
}: {
  logo: string
  background: string
  backgroundColour: string
  overlayText: ReactNode
}): JSX.Element => (
  <Background>
    <img className="background" src={background} alt="background" />
    <Overlay background={backgroundColour}>
      <img className="logo" src={logo} alt="logo" />
      <OverlayText>{overlayText}</OverlayText>
    </Overlay>
  </Background>
)
