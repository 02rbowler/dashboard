import React, { useContext } from "react"
import WidgetWrapper from "../WidgetWrapper"
import { useSelector } from "react-redux"
import { useGetRemindersQuery } from "../../reducers/reminders"
import { RootState } from "../../reducers/store"
import { ReminderType } from "../../types"
import * as Sentry from "@sentry/react"
import styled from "styled-components"
import { fontSize } from "../../styles/themes"
import { memo } from "react"
import { ThemeContext } from "../../App"

const RemindersContainer = styled.div<{darkMode: boolean}>`
  background: ${props => props.darkMode ? `rgba(255, 56, 56, 0.5)` : `rgba(255, 56, 56, 0.8)`};
  color: white;
  font-size: ${fontSize.m};
`

const AnimateWrapper = styled.div`
  overflow-y: hidden;
  flex: none;
  transition: max-height 2s;
  max-height: 0;
  margin: 0 -20px 0 -40px;

  &.animate-height {
    max-height: 100%;
  }
`

const PaddedDiv = styled.ul`
  padding: 15px 20px 10px 60px;
  margin: 0;

  li {
    margin-bottom: 5px;
  }
`

const Reminders = ({ show }: { show: boolean }): JSX.Element => {
  const userAccount = useSelector(
    (state: RootState) => state.userConfigReducer.data
  )

  if (!userAccount) {
    Sentry.captureException("No user account found in Reminders")
  }
  const userId = userAccount ? userAccount.userId : -1

  const {
    data: reminders,
    error,
    isLoading,
    isFetching,
    refetch,
  } = useGetRemindersQuery(userId, { refetchOnMountOrArgChange: 600 })

  const themeContext = useContext(ThemeContext)

  if (!reminders || reminders.length === 0) {
    return <WidgetWrapper />
  }

  return (
    <AnimateWrapper className={show ? "animate-height" : ""}>
      <RemindersContainer darkMode={themeContext.value === 'dark'}>
        <PaddedDiv>
          {reminders.map((rem: ReminderType) => (
            <li key={rem.ref["@ref"].id}>{rem.data.contents}</li>
          ))}
        </PaddedDiv>
      </RemindersContainer>
    </AnimateWrapper>
  )
}

export default memo(Reminders)
