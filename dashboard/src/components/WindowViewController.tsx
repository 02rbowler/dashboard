import React, { useEffect, useState } from "react"
import { useSelector } from "react-redux"
import styled from "styled-components"
import { RootState } from "../reducers/store"
import { ViewServiceType } from "../types"

const WindowContainer = styled.div`
  position: relative;
  display: flex;
  width: 200%;
  height: 100%;
`

const WindowView = styled.div`
  height: 100%;
  box-sizing: border-box;
  padding: 30px 40px;
  width: 50%;
`

const WindowViewController = ({ widgets, viewService }: { widgets: any[], viewService: ViewServiceType }) => {
  const [animate, setAnimate] = useState(false)
  const [arrayIndex, setArrayIndex] = useState(0)
  const [widgetComponents, setWidgetComponents] = useState<any[]>([])
  const config = useSelector(
    (state: RootState) => state
  )

  const stringedWidgets = JSON.stringify(widgets);

  useEffect(() => {
    let timeout: NodeJS.Timeout
    let secondTimeout: NodeJS.Timeout

    if (widgets.length > 1) {
      timeout = setInterval(() => {
        setAnimate(true)

        secondTimeout = setTimeout(() => {
          setArrayIndex(arrayIndex < widgets.length - 1 ? arrayIndex + 1 : 0)
          setAnimate(false)
        }, 3000)
      }, 20000)
    } else {
      setArrayIndex(0)
    }

    return () => {
      clearInterval(timeout)
      clearTimeout(secondTimeout)
    }
  }, [stringedWidgets, arrayIndex])

  useEffect(() => {
    const comps = viewService.parseValues(widgets, config)
    setWidgetComponents(comps)
  }, [stringedWidgets])

  return (
    <WindowContainer className={animate ? "slide-in" : ""}>
      <WindowView>{widgetComponents[arrayIndex]}</WindowView>
      <WindowView>
        {widgetComponents[arrayIndex < widgetComponents.length - 1 ? arrayIndex + 1 : 0]}
      </WindowView>
    </WindowContainer>
  )
}

export default WindowViewController
