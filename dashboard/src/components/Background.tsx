import React, { useState, useEffect, memo, useContext } from "react"
import background1 from "./../assets/backgroundImages/8ywSvj.jpg"
import { useSelector, useDispatch } from "react-redux"
import BackgroundService from "../services/background"
import styled from "styled-components"
import { RootState } from "../reducers/store"
import { useGetAllBackgroundsQuery } from "../reducers/background"
import { ThemeContext } from "../App"

const { version } = require("./../../package.json")

const BackgroundImage = styled.img`
  min-width: 100vw;
  height: 105vh;
  position: absolute;
  z-index: 0;
`

const VersionNumber = styled.div`
  position: absolute;
  bottom: 20px;
  right: 20px;
  font-size: 1.8em;
  color: white;
  font-weight: bold;
  opacity: 0.7;
`

const DarkModeScreen = styled.div`
  min-width: 100vw;
  height: 105vh;
  position: absolute;
  background-color: #0000009c;
  z-index: 0;
`

const Background = ({
  mouseMove,
}: {
  mouseMove: boolean
}): JSX.Element | null => {
  const [imageNumber, setImageNumber] = useState<string>(background1)
  const [nightMode, setNightMode] = useState<boolean>(false)
  const [fetched, setFetched] = useState<boolean>(false)
  const dispatch = useDispatch()
  const config = useSelector(
    (state: RootState) => state.keyStorageReducer.value
  )

  let noInternetBackgroundToggle: boolean = true

  const {
    data: backgroundImages,
    error,
    isLoading,
    isFetching,
    refetch,
  } = useGetAllBackgroundsQuery(undefined)

  useEffect(() => {
    let interval: NodeJS.Timeout
    if (Object.keys(config).length !== 0) {
      if (!fetched) {
        BackgroundService.getBackgroundImage(
          backgroundImages,
          noInternetBackgroundToggle,
          setNightMode,
          setImageNumber
        )
        setFetched(true)
      }

      interval = setInterval(() => {
        BackgroundService.getBackgroundImage(
          backgroundImages,
          noInternetBackgroundToggle,
          setNightMode,
          setImageNumber
        )
      }, 300000)
    }

    return () => {
      clearInterval(interval)
    }
  }, [config, backgroundImages, dispatch, fetched, noInternetBackgroundToggle])

  const themeContext = useContext(ThemeContext)

  if (nightMode) {
    return null
  }

  return (
    <>
      <BackgroundImage
        alt="background"
        className={`${!mouseMove && "hideCursor"}`}
        style={{
          background: !noInternetBackgroundToggle
            ? "linear-gradient(295deg, #07ff03, #2400ec)"
            : "linear-gradient(45deg, #489ff3, red)",
        }}
        src={imageNumber}
        onError={() => {
          setImageNumber(background1)
        }}
      />
      {themeContext.value === 'dark' && <DarkModeScreen />}
      {mouseMove && <VersionNumber>{version}</VersionNumber>}
    </>
  )
}

export default memo(Background)
