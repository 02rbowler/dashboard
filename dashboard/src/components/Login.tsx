import React, { useEffect, useState } from "react"
import styled from "styled-components"
import Pusher from "pusher-js"

const LoginContainer = styled.div`
  margin-top: 20vh;
  text-align: center;

  h1 {
    font-size: 6em;
    letter-spacing: 10px;
  }
`

function Login() {
  const publicDashboardConnect = {
    key: "1d7b65b02a05480a02b9",
    cluster: "eu",
  }

  const [uid] = useState((Math.floor(Math.random() * 90000) + 10000).toString())

  let viewInterval: NodeJS.Timeout | undefined
  let adjustInterval: NodeJS.Timeout | undefined
  let windowTimeout: NodeJS.Timeout | undefined

  useEffect(() => {
    const pusher = new Pusher(publicDashboardConnect.key, {
      cluster: publicDashboardConnect.cluster,
      // encrypted: true
    })

    var channel = pusher.subscribe("dashboard")
    channel.bind(uid, (data: { location: string }) => {
      if (data.location && data.location.length > 0) {
        localStorage.setItem("userAccount", data.location)
        window.location.reload()
      } else {
        // need to redo
      }
    })

    return function cleanup() {
      viewInterval && clearInterval(viewInterval)
      adjustInterval && clearInterval(adjustInterval)
      windowTimeout && clearTimeout(windowTimeout)
      pusher.unsubscribe(uid)
    }
  }, [])

  return (
    <LoginContainer>
      <h2>Your dashboard code is:</h2>
      <h1>{uid}</h1>
      <h2>In the app go to settings and click "setup new dashboard"</h2>
      <h2>Fill in the details with the code above to setup</h2>
    </LoginContainer>
  )
}

export default Login
