import React, { useEffect, useState } from "react"
import { useSelector } from "react-redux"
import { useSportsCentreQuery, useTvQuery } from "../api"
import { useGetAllNewsQuery } from "../reducers/news"
import { useGetSportsGuideQuery } from "../reducers/sportsCentre"
import { RootState } from "../reducers/store"
import { useGetTubeDataQuery } from "../reducers/tube"
import { outputWidgetArray, WidgetTransformer } from "../services/widgetController"
import { WidgetOutline } from "./Widget"
import Birthdays from "./widgets/Birthdays"
import News from "./widgets/News"
import SportsCentre from "./widgets/SportsCentre"
import { F1NextRace } from "./widgets/SportsCentre/F1"
import { SkySportsGuide } from "./widgets/SportsCentre/SkySportsGuide"
import Tube from "./widgets/Tube"
import TV from "./widgets/TV"

export const WidgetController = ({widgetSchedule}: {widgetSchedule: any[]}) => {
  const [animate, setAnimate] = useState(false)
  const [arrayIndex, setArrayIndex] = useState(0)
  
  const {
    data: tvSchedule
  } = useTvQuery({pollInterval: 3600000})

  const calendarData = useSelector(
    (state: RootState) => state.calendarReducer.data
  )
  
  const {
    data: newsItems
  } = useGetAllNewsQuery(undefined, { pollingInterval: 600000 })

  const {
    data: scData
  } = useSportsCentreQuery({pollInterval: 3600000})

  const {
    data: lineStatus
  } = useGetTubeDataQuery(undefined, {pollingInterval: 300000})

  const { data: sportsGuide } =
    useGetSportsGuideQuery(undefined, {pollingInterval: 3600000})
  
  const [renderArray, setRenderArray] = useState(outputWidgetArray(calendarData, widgetSchedule, tvSchedule, newsItems, scData, lineStatus, sportsGuide))

  useEffect(() => {
    setRenderArray(outputWidgetArray(calendarData, widgetSchedule, tvSchedule, newsItems, scData, lineStatus, sportsGuide))
  }, [calendarData, widgetSchedule, tvSchedule, newsItems, scData, lineStatus?.toString(), sportsGuide])

  useEffect(() => {
    let timeout: NodeJS.Timeout
    let secondTimeout: NodeJS.Timeout

    if (renderArray.length > 3) {
      timeout = setInterval(() => {
        setAnimate(true)

        secondTimeout = setTimeout(() => {
          setArrayIndex(arrayIndex < renderArray.length - 3 ? arrayIndex + 3 : 0)
          setAnimate(false)
        }, 3000)
      }, 20000)
    } else {
      setArrayIndex(0)
    }

    return () => {
      clearInterval(timeout)
      clearTimeout(secondTimeout)
    }
  }, [JSON.stringify(renderArray), arrayIndex])

  const getWidget = (item: WidgetTransformer) => {
    if(!item) {
      return <WidgetOutline invisible />
    }
    switch(item.widget) {
      case "tv": return <TV {...item.data} />
      case "news": return <News {...item.data} />
      case "sportsCentre": return <SportsCentre {...item.data} />
      case "nextF1": return <F1NextRace {...item.data} />
      case "skySportsGuide": return <SkySportsGuide />
      case "tube": return <Tube {...item.data} />
      case "birthdays": return <Birthdays {...item.data} />
      default: return null
    }
  }

  if(renderArray.length === 0) {
    return null
  }

  return <div style={{display: 'flex', height: '100%', width: '200%'}} className={animate ? "slide-in" : ""}>
    { getWidget(renderArray[arrayIndex]) }
    { getWidget(renderArray[arrayIndex + 1]) }
    { getWidget(renderArray[arrayIndex + 2]) }

    { 
      renderArray.length > 3 && arrayIndex + 3 < renderArray.length ?
        <>
          { getWidget(renderArray[arrayIndex + 3]) }
          { getWidget(renderArray[arrayIndex + 4]) }
          { getWidget(renderArray[arrayIndex + 5]) }
        </>
      : renderArray.length > 3 ?
        <>
          { getWidget(renderArray[0]) }
          { getWidget(renderArray[1]) }
          { getWidget(renderArray[2]) }
        </>
      : null
    }
  </div>
}