import React, { useEffect, useState } from "react"
import TimeService from "../services/time"
import styled from "styled-components"
import { fontFamily, fontSize } from "../styles/themes"

const StyledHeader = styled.div<{screenAdjust: boolean}>`
  ${fontFamily};
  line-height: 1.2;
  margin-bottom: 10px;
  display: flex;
  align-items: baseline;
  white-space: nowrap;

  &.opacity {
    opacity: 0.8;
  }

  ${props => props.screenAdjust ? `
    transform: translate(-10px, -10px);
  ` : ''}
`

const Time = styled.div`
  font-size: ${fontSize.xl};
  margin-bottom: -10px;
`

const Date = styled.div`
  padding-left: 20px;
  font-size: ${fontSize.l};
  flex: 1;
  overflow: hidden;
  text-overflow: ellipsis;
`

const TimeSymbol = styled.span`
  text-transform: uppercase;
  font-size: ${fontSize.l};
`

const Header = ({screenAdjust}: {screenAdjust: boolean}) => {
  const timeService = TimeService()
  const [time, setTime] = useState(timeService.getTime())
  const [timeSymbol, setTimeSymbol] = useState(timeService.getTimeSymbol())
  const [dayOfWeek, setDayOfWeek] = useState(timeService.getDayOfWeek())
  const [date, setDate] = useState(timeService.getDate())
  const [showDaysLeft, setShowDaysLeft] = useState(false)
  const [isConnected, setIsConnected] = useState(true)

  useEffect(() => {
    let internalTimeout!: NodeJS.Timeout
    let timeInterval = setInterval(() => {
      setTime(timeService.getTime())
      setTimeSymbol(timeService.getTimeSymbol())
      setDayOfWeek(timeService.getDayOfWeek())
      setDate(timeService.getDate())
    }, 30000)

    let viewInterval = setInterval(() => {
      setShowDaysLeft(false)
      setIsConnected(window.navigator.onLine)

      internalTimeout = setTimeout(() => {
        setShowDaysLeft(true)
      }, 10000)
    }, 15000)

    return () => {
      clearTimeout(internalTimeout)
      clearInterval(timeInterval)
      clearInterval(viewInterval)
    }
  }, [])

  const createMarkup = () => {
    if (!isConnected) {
      return "-- No internet connection --"
    }
    return `${dayOfWeek} ${date}`
  }

  return (
    <StyledHeader screenAdjust={screenAdjust} className={showDaysLeft ? "opacity" : ""}>
      <Time>
        {time}
        <TimeSymbol>{timeSymbol}</TimeSymbol>
      </Time>
      <Date>{createMarkup()}</Date>
    </StyledHeader>
  )
}

export default Header
