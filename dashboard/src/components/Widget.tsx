import React, { ReactNode } from "react"
import styled from "styled-components"
import { fontFamily, fontSize } from "../styles/themes"
import WidgetHeader from "./shared/WidgetHeader"

const BaseWidget = styled.div`
  font-size: 1.8em;
  border-radius: 10px;
  border: 1px solid #ffffffd9;
  ${fontFamily};
  overflow-y: hidden;
  margin: 5px;
  margin-bottom: 7px;
  box-shadow: 0px 0px 4px 3px rgba(0, 0, 0, 0.25);
  backdrop-filter: blur(4px);
`

const StyledWidget = styled(BaseWidget)<{ text?: string; background?: string }>`
  background-color: ${props => props.background || "#ebebebed"};
  color: ${props => props.text || "#272727"};
`

type widgetProps = {
  extraClass?: string
  text?: string
  background?: {
    first: string
    second: string
    last: string
  }
  header?: string | ReactNode
  children: ReactNode
}

type columnWidgetProps = widgetProps & {
  background: {
    titleColumn: string
    titleColour?: string
  }
}

const StyledGradientWidget = styled(BaseWidget)<widgetProps>`
	background: #ebebebed;
	background: ${props =>
    props.background
      ? `linear-gradient(140deg, ${props.background.first} 0%, 
		${props.background.second} 48%, 
		${props.background.last} 100%);`
      : "#ebebebed;"}

	color: ${props => props.text || "#272727"};
`

const PlainFlexWidget = styled.div`
  display: flex;
  flex-direction: row;
  color: black;
  font-size: ${fontSize.m};
  align-items: baseline;
`

const WidgetList = styled.div`
  flex: 1;
  display: flex;
  flex-direction: column;
`

const Widget = ({
  text,
  background,
  header,
  extraClass,
  children,
}: {
  extraClass?: string
  text?: string
  background?: string
  header?: string
  children: ReactNode
}): JSX.Element => (
  <StyledWidget text={text} background={background} className={extraClass}>
    {header && <WidgetHeader>{header}</WidgetHeader>}
    {children}
  </StyledWidget>
)

export const GradientWidget = ({
  text,
  background,
  header,
  extraClass,
  children,
}: widgetProps): JSX.Element => (
  <StyledGradientWidget
    text={text}
    background={background}
    className={extraClass}
  >
    {header && <WidgetHeader>{header}</WidgetHeader>}
    {children}
  </StyledGradientWidget>
)

const DayColumn = styled.div`
  width: 130px;
  font-weight: bold;
`

const StyledRowBackground = styled.div<{ highlightMode?: boolean, darkMode?: boolean }>`
  background-color: ${props =>
    props.highlightMode ? `#00000094` : props.darkMode ? '#09091fad' : `#ffffffad`};
  ${props => (props.highlightMode || props.darkMode) && `div { color: white; }`}
  border-radius: 10px;
  margin-bottom: 4px;
  padding: 5px 15px;
  box-sizing: border-box;
`

export const RowBackground = ({
  highlightMode,
  darkMode,
  children,
}: {
  highlightMode?: boolean
  darkMode?: boolean
  children: ReactNode
}) => (
  <StyledRowBackground highlightMode={highlightMode} darkMode={darkMode}>
    {children}
  </StyledRowBackground>
)

export const WidgetOutline = styled.div<{invisible?: boolean, darkMode?: boolean, colour?: string, background?: string}>`
  width: calc(100% / 6); // Because it's at 200%
  flex: none;
  overflow: hidden;
  height: 100%;
  padding: 10px 20px 30px;
  box-sizing: border-box;
  position: relative;
  ${props => props.darkMode ? 'color: white;' : props.colour ? `color: ${props.colour};` : ''}
  background: ${props => props.background ? `${props.background};` : props.darkMode ?  'rgb(13 14 43 / 75%);' : 'rgba(255,255,255,0.7);'}
  backdrop-filter: blur(20px);
  ${props => props.invisible ? 'visibility: hidden;' : ''}
`

export const PlainWidget = ({
  text,
  background,
  header,
  extraClass,
  children,
}: columnWidgetProps) => (
  <PlainFlexWidget>
    <DayColumn>{header}</DayColumn>
    <WidgetList>{children}</WidgetList>
  </PlainFlexWidget>
)

export default Widget
