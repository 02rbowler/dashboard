import React, { ReactNode } from "react"

import styled from "styled-components"
import useAnimate from "../hooks/useAnimate"

interface propTypes {
  allowAnimate?: boolean
  children?: ReactNode
  window?: boolean
  background?: string
}

const StyledWidgetWrapper = styled.div`
  max-height: 90vh;
  transition: max-height 2s;
  color: white;
  overflow-y: hidden;
  white-space: nowrap;
  width: 100%;

  &.empty {
    max-height: 0px;
    transition: max-height 1.4s ease-in-out;
  }
`

const StyledWindowWidgetWrapper = styled(StyledWidgetWrapper)<{background?: string}>`
  border-radius: 0;
  padding: 20px 20px 20px 40px;
  box-sizing: border-box;
  ${props => props.background ? `background: ${props.background};` : ''}
`

export const WindowWidgetWrapper = (props: propTypes) => {
  return (
    <StyledWindowWidgetWrapper background={props.background}
      // className={`${(!props.children || !animate || animate === 2) && "empty"}`}
    >
      {props.children}
    </StyledWindowWidgetWrapper>
  )
}

function WidgetWrapper(props: propTypes) {
  const animate = useAnimate(props.allowAnimate || false, props.window || false)

  return (
    <StyledWidgetWrapper
      className={`${(!props.children || !animate || animate === 2) && "empty"}`}
    >
      {props.children}
    </StyledWidgetWrapper>
  )
}

export default WidgetWrapper
