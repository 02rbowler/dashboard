import React, { ReactNode } from "react"
import styled from "styled-components"

const StyledWindow = styled.div<{ showWindow: boolean, darkMode: boolean }>`
  height: 240px;
  width: ${props => (props.showWindow ? "calc(100vw - 559px)" : "0")};
  // background: ${props => props.darkMode ? `rgb(12 14 35 / 85%)` : `rgba(255,255,255,0.7)`};
  // background: rgba(248, 248, 248, 0.6);
  // backdrop-filter: blur(20px);
  overflow: hidden;
  margin-top: auto;
`

function Window(props: { showWindow: boolean; darkMode: boolean; children?: ReactNode }) {
  return (
    <StyledWindow showWindow={props.showWindow} darkMode={props.darkMode}>{props.children}</StyledWindow>
  )
}

export default Window
